<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_get_demo_array($dir_url, $dir_path){

    $demo_items = array(
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-01.jpg?zoom=2&ssl=1&resize=1020,972&quality=91',
            'link'      => 'https://zephys.la-studioweb.com/home-01/',
            'title'     => 'Demo Architecture 01',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i1.wp.com/la-studioweb.com/demo-data/zephys/home-02.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-02/',
            'title'     => 'Demo Architecture 02',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i1.wp.com/la-studioweb.com/demo-data/zephys/home-03.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-03/',
            'title'     => 'Demo Architecture 03',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-04.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-04/',
            'title'     => 'Demo Architecture 04',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i1.wp.com/la-studioweb.com/demo-data/zephys/home-05.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-05/',
            'title'     => 'Demo Architecture 05',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i2.wp.com/la-studioweb.com/demo-data/zephys/home-06.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-06/',
            'title'     => 'Demo Architecture 06',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-07.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-07/',
            'title'     => 'Demo Architecture 07',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i1.wp.com/la-studioweb.com/demo-data/zephys/home-08.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-08/',
            'title'     => 'Demo Architecture 08',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i2.wp.com/la-studioweb.com/demo-data/zephys/home-09.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-09/',
            'title'     => 'Demo Architecture 09',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        ),
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-10.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/interior/demo-01/',
            'title'     => 'Demo Interior 01',
            'category'  => array(
                'Demo',
                'Interior'
            )
        ),
        array(
            'image'     => 'https://i1.wp.com/la-studioweb.com/demo-data/zephys/home-11.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/interior/demo-02/',
            'title'     => 'Demo Interior 02',
            'category'  => array(
                'Demo',
                'Interior'
            )
        ),
        array(
            'image'     => 'https://i2.wp.com/la-studioweb.com/demo-data/zephys/home-12.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/interior/demo-03/',
            'title'     => 'Demo Interior 03',
            'category'  => array(
                'Demo',
                'Interior'
            )
        ),
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-13.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/interior/demo-04/',
            'title'     => 'Demo Interior 04',
            'category'  => array(
                'Demo',
                'Interior'
            )
        ),
        array(
            'image'     => 'https://i1.wp.com/la-studioweb.com/demo-data/zephys/home-14.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/real-estate/demo-01/',
            'title'     => 'Demo Real Estate 01',
            'category'  => array(
                'Demo',
                'Real Estate',
                'Shop'
            )
        ),
        array(
            'image'     => 'https://i2.wp.com/la-studioweb.com/demo-data/zephys/home-15.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/real-estate/demo-02/',
            'title'     => 'Demo Real Estate 02',
            'category'  => array(
                'Demo',
                'Real Estate'
            )
        ),
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-16.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/real-estate/demo-03/',
            'title'     => 'Demo Real Estate 03',
            'category'  => array(
                'Demo',
                'Real Estate'
            )
        ),
        array(
            'image'     => 'https://i0.wp.com/la-studioweb.com/demo-data/zephys/home-17.jpg?zoom=2&ssl=1&resize=1020,972&quality=90',
            'link'      => 'https://zephys.la-studioweb.com/home-17/',
            'title'     => 'Demo Architecture 10',
            'category'  => array(
                'Demo',
                'Architecture'
            )
        )
    );

    $default_image_setting = array(
        'woocommerce_single_image_width' => 850,
        'woocommerce_thumbnail_image_width' => 850,
        'woocommerce_thumbnail_cropping' => 'custom',
        'woocommerce_thumbnail_cropping_custom_width' => 425,
        'woocommerce_thumbnail_cropping_custom_height' => 450,
        'thumbnail_size_w' => 0,
        'thumbnail_size_h' => 0,
        'medium_size_w' => 0,
        'medium_size_h' => 0,
        'medium_large_size_w' => 0,
        'medium_large_size_h' => 0,
        'large_size_w' => 0,
        'large_size_h' => 0
    );

    $default_menu = array(
        'main-nav'              => 'Main Navigation'
    );

    $default_page = array(
        'page_for_posts' 	            => 'Our Blog',
        'woocommerce_shop_page_id'      => 'Shop Page',
        'woocommerce_cart_page_id'      => 'Cart',
        'woocommerce_checkout_page_id'  => 'Checkout',
        'woocommerce_myaccount_page_id' => 'My Account'
    );

    $slider = $dir_path . 'Slider/';
    $content = $dir_path . 'Content/';
    $widget = $dir_path . 'Widget/';
    $setting = $dir_path . 'Setting/';
    $preview = $dir_url;


    $data_return = array();

    for( $i = 1; $i <= 17; $i ++ ){
        $tmp_i = $i;
        $id = $tmp_i;
        if( $tmp_i < 10 ) {
            $id = '0'. $tmp_i;
        }
        $demo_item_name = !empty($demo_items[$tmp_i - 1]['title']) ? $demo_items[$tmp_i - 1]['title'] : 'Demo ' . $id;

        $value = array();
        $value['title']             = $demo_item_name;
        $value['category']          = !empty($demo_items[$tmp_i - 1]['category']) ? $demo_items[$tmp_i - 1]['category'] : array('Demo');
        $value['demo_preset']       = 'home-' . $id;
        $value['demo_url']          = !empty($demo_items[$tmp_i - 1]['link']) ? $demo_items[$tmp_i - 1]['link'] : 'https://zephys.la-studioweb.com/home-' . $id . '/';
        $value['preview']           = !empty($demo_items[$tmp_i - 1]['image']) ? $demo_items[$tmp_i - 1]['image'] : $preview  .   'home-' . $id . '.jpg';
        $value['option']            = $setting  .   'home-' . $id . '.json';
        $value['content']           = $content  .   'data-sample.xml';
        $value['widget']            = $widget   .   'widget.json';

        $value['pages']             = array_merge(
            $default_page,
            array(
                'page_on_front' => 'Home ' . $id
            )
        );

        $value['menu-locations']    = array_merge(
            $default_menu,
            array(

            )
        );
        $value['other_setting']    = array_merge(
            $default_image_setting,
            array(

            )
        );

        if( !in_array( $tmp_i, array(6,7,14,15,16,17)) ){
            $value['slider']  = $slider   .   'home-'. $id .'.zip';
        }

        $data_return['home-'. $id] = $value;
    }

    $tmp_data_return = array();

    foreach ($data_return as $k => $v){
        if($k == 'home-17'){
            continue;
        }
        $tmp_data_return[$k] = $v;
        if($k == 'home-09'){
            $tmp_data_return['home-17'] = $data_return['home-17'];
        }
    }

    $data_return = $tmp_data_return;


    if(class_exists('LAHB_Helper')){
        $header_presets = LAHB_Helper::getHeaderDefaultData();

        $header_01 = json_decode($header_presets['header-01']['data'], true);
        $header_02 = json_decode($header_presets['header-02']['data'], true);
        $header_03 = json_decode($header_presets['header-03']['data'], true);
        $header_04 = json_decode($header_presets['header-04']['data'], true);
        $header_05 = json_decode($header_presets['header-05']['data'], true);
        $header_06 = json_decode($header_presets['header-06']['data'], true);
        $header_07 = json_decode($header_presets['header-07']['data'], true);
        $header_08 = json_decode($header_presets['header-08']['data'], true);
        $header_09 = json_decode($header_presets['header-09']['data'], true);


        $data_return['home-01']['other_setting'] = $header_09;
        $data_return['home-02']['other_setting'] = $header_03;
        $data_return['home-03']['other_setting'] = $header_01;
        $data_return['home-04']['other_setting'] = $header_02;
        $data_return['home-05']['other_setting'] = $header_03;
        $data_return['home-06']['other_setting'] = $header_04;
        $data_return['home-07']['other_setting'] = $header_04;
        $data_return['home-08']['other_setting'] = $header_06;
        $data_return['home-09']['other_setting'] = $header_05;
        $data_return['home-10']['other_setting'] = $header_01;
        $data_return['home-11']['other_setting'] = $header_03;
        $data_return['home-12']['other_setting'] = $header_05;
        $data_return['home-13']['other_setting'] = $header_07;
        $data_return['home-14']['other_setting'] = $header_08;
        $data_return['home-15']['other_setting'] = $header_08;
        $data_return['home-16']['other_setting'] = $header_08;
        $data_return['home-17']['other_setting'] = $header_02;
    }

    return $data_return;
}