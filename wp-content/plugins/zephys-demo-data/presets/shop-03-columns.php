<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_shop_03_columns()
{
    return [
        [
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ],
        [
            'key' => 'header_transparency',
            'value' => 'yes'
        ],
        [
            'key' => 'main_full_width_archive_product',
            'value' => 'no'
        ],
        [
            'key' => 'shop_item_space',
            'value' => [
                'laptop' => [
                    'left' => '30',
                    'right' => '30',
                    'bottom' => '50'
                ],
                'tablet' => [
                    'left' => '20',
                    'right' => '20',
                    'bottom' => '40'
                ],
                'mobile' => [
                    'left' => '10',
                    'right' => '10',
                    'bottom' => '30'
                ]
            ]
        ],
        [
            'key' => 'woocommerce_shop_page_columns',
            'value' => [
                'desktop' => 3,
                'laptop' => 3,
                'tablet' => 3,
                'mobile_landscape' => 2,
                'mobile' => 1
            ]
        ],
        [
            'key' => 'page_title_bar_space',
            'value' => [
                'desktop' => [
                    'top' => 260,
                    'bottom' => 140,
                ],
                'laptop' => [
                    'top' => 180,
                    'bottom' => 100,
                ],
                'mobile' => [
                    'top' => 140,
                    'bottom' => 50,
                ],
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 80,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 60,
                    'unit' => 'px'
                ],
                'mobile' => [
                    'font-size' => 50,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_breadcrumb_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 18,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 16,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_family',
            'value' => [
                'font-weight' => 700,
                'line-height' => 1,
            ]
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_id',
            'value'         => 160
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_transparency_id',
            'value'         => 160
        ],
        [
            'filter_name'       => 'body_class',
            'filter_func'       => function( $classes ) {
                $classes[] = 'header-black';
                return $classes;
            },
            'filter_priority'   => 10,
            'filter_args'       => 1
        ]
    ];
}