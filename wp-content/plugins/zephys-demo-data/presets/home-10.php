<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_home_10()
{
    return [
        [
            'filter_name'   => 'LaStudio_Builder/logo_id',
            'value'         => 1458
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_transparency_id',
            'value'         => 1459
        ],
        [
            'key'           => 'primary_color',
            'value'         => '#EA622B'
        ]
    ];
}