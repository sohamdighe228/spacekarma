<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_left_sidebar()
{
    return [
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'Left Sidebar'
        ]
    ];
}