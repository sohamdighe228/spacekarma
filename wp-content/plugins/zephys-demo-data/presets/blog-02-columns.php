<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_02_columns()
{
    return [
        [
            'key'           => 'layout_blog',
            'value'         => 'col-1c'
        ],
        [
            'key'           => 'main_full_width',
            'value'         => 'no'
        ],
        [
            'key'           => 'blog_design',
            'value'         => 'grid-4'
        ],
        [
            'key'           => 'blog_thumbnail_height_custom',
            'value'         => '60%'
        ],
        [
            'key'           => 'blog_post_column',
            'value'         => [
                'desktop'           => 2,
                'laptop'            => 2,
                'tablet'            => 2,
                'mobile_landscape'  => 2,
                'mobile'            => 1,
            ]
        ],
        [
            'key'           => 'blog_item_space',
            'value'         => [
                'mobile'    => [
                    'top'       => '0',
                    'right'     => '0',
                    'bottom'    => '30',
                    'left'      => '0'
                ],
                'mobile_landscape'    => [
                    'top'       => '0',
                    'right'     => '15',
                    'bottom'    => '40',
                    'left'      => '15'
                ],
                'laptop'    => [
                    'top'       => '0',
                    'right'     => '15',
                    'bottom'    => '50',
                    'left'      => '15'
                ],
                'desktop'    => [
                    'top'       => '0',
                    'right'     => '15',
                    'bottom'    => '60',
                    'left'      => '15'
                ]
            ]
        ],
        [
            'key'           => 'blog_entry_title_font_size',
            'value'         => [
                'laptop'    => [
                    'font-size'       => '26',
                    'line-height'     => '30',
                    'unit'            => 'px',
                ],
                'desktop'    => [
                    'font-size'       => '32',
                    'line-height'     => '36',
                    'unit'            => 'px',
                ]
            ]
        ],
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'Blog 02 Columns'
        ]
    ];
}