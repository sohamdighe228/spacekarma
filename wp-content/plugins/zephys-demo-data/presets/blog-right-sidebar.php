<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_right_sidebar()
{
    return [
        [
            'key'           => 'layout_blog',
            'value'         => 'col-2cr'
        ],
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'Right Sidebar'
        ]
    ];
}