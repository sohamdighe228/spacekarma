<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_custom_02()
{
    return [
        [
            'key'           => 'layout_blog',
            'value'         => 'col-1c'
        ],
        [
            'key'           => 'main_full_width',
            'value'         => 'yes'
        ],
        [
            'key'           => 'blog_design',
            'value'         => 'grid-6'
        ],
        [
            'key'           => 'blog_thumbnail_height_custom',
            'value'         => '95%'
        ],
        [
            'key'           => 'blog_post_column',
            'value'         => [
                'desktop'           => 3,
                'laptop'            => 3,
                'tablet'            => 3,
                'mobile_landscape'  => 2,
                'mobile'            => 1,
            ]
        ],
        [
            'key'           => 'blog_item_space',
            'value'         => [
                'mobile'    => [
                    'top'       => '10',
                    'right'     => '10',
                    'bottom'    => '10',
                    'left'      => '10'
                ]
            ]
        ],
        [
            'key'           => 'blog_entry_title_font_size',
            'value'         => [
                'mobile_landscape'    => [
                    'font-size'       => '20',
                    'line-height'     => '30',
                    'unit'            => 'px',
                ],
                'laptop'    => [
                    'font-size'       => '24',
                    'line-height'     => '30',
                    'unit'            => 'px',
                ]
            ]
        ],
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'Blog Custom 02'
        ],
        [
            'filter_name'       => 'zephys/filter/get_option',
            'filter_func'       => function( $value, $key ) {
                if( $key == 'la_custom_css'){
                    $value .= '#main #content-wrap{ max-width: 100%; padding: 10px 20px}';
                }
                return $value;
            },
            'filter_priority'   => 10,
            'filter_args'       => 2
        ]
    ];
}