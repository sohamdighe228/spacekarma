<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_shop_masonry()
{
    return [
        [
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ],
        [
            'key' => 'header_transparency',
            'value' => 'yes'
        ],
        [
            'key' => 'main_full_width_archive_product',
            'value' => 'no'
        ],
        [
            'key' => 'woocommerce_toggle_grid_list',
            'value' => 'off'
        ],
        [
            'key' => 'active_shop_masonry',
            'value' => 'on'
        ],
        [
            'key' => 'shop_masonry_column_type',
            'value' => 'custom'
        ],
        [
            'key' => 'product_masonry_container_width',
            'value' => 1250
        ],
        [
            'key' => 'product_masonry_item_width',
            'value' => 370
        ],
        [
            'key' => 'product_masonry_item_height',
            'value' => 350
        ],
        [
            'key' => 'woocommerce_shop_masonry_custom_columns',
            'value' => [
                'mobile' => 1,
                'mobile_landscape' => 2,
                'tablet' => 3,
                'laptop' => 3
            ]
        ],
        [
            'key' => 'enable_shop_masonry_custom_setting',
            'value' => 'on'
        ],
        [
            'key' => 'shop_masonry_item_setting',
            'value' => [
                0 => [
                    'size_name' => '1w x 2h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                1 => [
                    'size_name' => '1w x 1h',
                    'w'         => 1,
                    'h'         => 1.3,
                ],
                2 => [
                    'size_name' => '1w x 1h',
                    'w'         => 1,
                    'h'         => 1,
                ]
            ]
        ],
        [
            'key' => 'shop_item_space',
            'value' => [
                'laptop' => [
                    'left' => '30',
                    'right' => '30',
                    'bottom' => '50'
                ],
                'mobile' => [
                    'left' => '10',
                    'right' => '10',
                    'bottom' => '30'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_space',
            'value' => [
                'desktop' => [
                    'top' => 260,
                    'bottom' => 140,
                ],
                'laptop' => [
                    'top' => 180,
                    'bottom' => 100,
                ],
                'mobile' => [
                    'top' => 140,
                    'bottom' => 50,
                ],
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 80,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 60,
                    'unit' => 'px'
                ],
                'mobile' => [
                    'font-size' => 50,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_breadcrumb_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 18,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 16,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_family',
            'value' => [
                'font-weight' => 700,
                'line-height' => 1,
            ]
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_id',
            'value'         => 160
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_transparency_id',
            'value'         => 160
        ],
        [
            'filter_name'       => 'body_class',
            'filter_func'       => function( $classes ) {
                $classes[] = 'header-black';
                return $classes;
            },
            'filter_priority'   => 10,
            'filter_args'       => 1
        ]
    ];
}