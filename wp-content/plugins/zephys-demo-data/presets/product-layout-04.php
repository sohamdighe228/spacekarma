<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_product_layout_04()
{
    return [
        [
            'key' => 'woocommerce_product_page_design',
            'value' => '4'
        ],
        [
            'key' => 'move_woo_tabs_to_bottom',
            'value' => 'no'
        ],
        [
            'filter_name'       => 'zephys/filter/get_option',
            'filter_func'       => function( $value, $key ) {
                if( $key == 'move_woo_tabs_to_bottom' ) {
                    $value = 'no';
                }
                return $value;
            },
            'filter_priority'   => 10,
            'filter_args'       => 2
        ]
    ];
}