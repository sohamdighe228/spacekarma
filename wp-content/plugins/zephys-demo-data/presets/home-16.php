<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_home_16()
{
    return [
        [
            'filter_name'   => 'LaStudio_Builder/logo_id',
            'value'         => 1486
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_transparency_id',
            'value'         => 1487
        ],
        [
            'key'           => 'primary_color',
            'value'         => '#75ad1c'
        ]
    ];
}