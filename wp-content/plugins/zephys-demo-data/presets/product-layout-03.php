<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_product_layout_03()
{
    return [
        [
            'key' => 'layout_single_product',
            'value' => 'col-2cl'
        ],
        [
            'key' => 'woocommerce_product_page_design',
            'value' => '1'
        ],
        [
            'key' => 'main_full_width_single_product',
            'value' => 'yes'
        ],
        [
            'filter_name'       => 'zephys/filter/get_option',
            'filter_func'       => function( $value, $key ) {
                if( $key == 'la_custom_css'){
                    $value .= '@media(min-width: 1400px){ .enable-main-fullwidth .section-page-header > .container, #main #content-wrap { width: 1560px; max-width: 96%;} }';
                }
                return $value;
            },
            'filter_priority'   => 10,
            'filter_args'       => 2
        ]
    ];
}