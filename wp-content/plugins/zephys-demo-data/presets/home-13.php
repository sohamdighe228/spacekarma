<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_home_13()
{
    return [
        [
            'filter_name'   => 'LaStudio_Builder/logo_id',
            'value'         => 1458
        ],
        [
            'filter_name'   => 'LaStudio_Builder/logo_transparency_id',
            'value'         => 1459
        ],
        [
            'key'       => 'primary_color',
            'value'     => '#EA622B'
        ],
        [
            'key'       => 'body_font_family',
            'value'     => [
                'font-family'   => 'Roboto Condensed',
                'font-weight'   => 'normal',
                'font-size'     => 16,
                'unit'          => 'px',
                'extra-styles' => [
                    'italic',
                    '700',
                    '700italic'
                ]
            ]
        ],
        [
            'filter_name'       => 'zephys/filter/get_option',
            'filter_func'       => function( $value, $key ) {
                if( $key == 'la_custom_css'){
                    $value .= '@import url(https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&display=swap);';
                }
                return $value;
            },
            'filter_priority'   => 10,
            'filter_args'       => 2
        ]
    ];
}