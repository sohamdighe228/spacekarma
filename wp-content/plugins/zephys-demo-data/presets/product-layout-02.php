<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_product_layout_02()
{
    return [
        [
            'key' => 'woocommerce_product_page_design',
            'value' => '3'
        ]
    ];
}