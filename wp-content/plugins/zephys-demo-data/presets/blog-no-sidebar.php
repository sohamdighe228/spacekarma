<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_no_sidebar()
{
    return [
        [
            'key'           => 'layout_blog',
            'value'         => 'col-1c'
        ],
        [
            'key'           => 'blog_small_layout',
            'value'         => 'on'
        ],[
            'key'           => 'blog_thumbnail_height_custom',
            'value'         => '56.25%'
        ],
        [
            'key'           => 'blog_item_space',
            'value'         => [
                'mobile'    => [
                    'top'       => '0',
                    'right'     => '0',
                    'bottom'    => '30',
                    'left'      => '0'
                ],
                'mobile_landscape'    => [
                    'top'       => '0',
                    'right'     => '0',
                    'bottom'    => '50',
                    'left'      => '0'
                ],
                'laptop'    => [
                    'top'       => '0',
                    'right'     => '0',
                    'bottom'    => '50',
                    'left'      => '0'
                ],
                'desktop'    => [
                    'top'       => '0',
                    'right'     => '0',
                    'bottom'    => '70',
                    'left'      => '0'
                ]
            ]
        ],
        [
            'key'           => 'blog_design',
            'value'         => 'list-2'
        ],
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'No Sidebar'
        ],
        [
            'filter_name'       => 'zephys/filter/get_option',
            'filter_func'       => function( $value, $key ) {
                if( $key == 'la_custom_css'){
                    $value .= '.lastudio-posts .post-thumbnail.single_post_quote_wrap .blog_item--thumbnail {
    padding-bottom: 0;
    height: 250px;
}';
                }
                return $value;
            },
            'filter_priority'   => 10,
            'filter_args'       => 2
        ]
    ];
}