<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_custom_01()
{
    return [
        [
            'key'           => 'layout_blog',
            'value'         => 'col-1c'
        ],
        [
            'key'           => 'main_full_width',
            'value'         => 'yes'
        ],
        [
            'key'           => 'blog_design',
            'value'         => 'grid-5'
        ],
        [
            'key'           => 'blog_thumbnail_height_custom',
            'value'         => '85%'
        ],
        [
            'key'           => 'blog_post_column',
            'value'         => [
                'desktop'           => 3,
                'laptop'            => 3,
                'tablet'            => 3,
                'mobile_landscape'  => 2,
                'mobile'            => 1,
            ]
        ],
        [
            'key'           => 'blog_item_space',
            'value'         => [
                'mobile'    => [
                    'top'       => '10',
                    'right'     => '10',
                    'bottom'    => '10',
                    'left'      => '10'
                ],
                'laptop'    => [
                    'top'       => '20',
                    'right'     => '20',
                    'bottom'    => '20',
                    'left'      => '20'
                ],
                'desktop'    => [
                    'top'       => '40',
                    'right'     => '40',
                    'bottom'    => '40',
                    'left'      => '40'
                ]
            ]
        ],
        [
            'key'           => 'blog_entry_title_font_size',
            'value'         => [
                'mobile_landscape'    => [
                    'font-size'       => '20',
                    'line-height'     => '30',
                    'unit'            => 'px',
                ],
                'laptop'    => [
                    'font-size'       => '26',
                    'line-height'     => '30',
                    'unit'            => 'px',
                ],
                'desktop'    => [
                    'font-size'       => '32',
                    'line-height'     => '36',
                    'unit'            => 'px',
                ]
            ]
        ],
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'Blog Custom 01'
        ]
    ];
}