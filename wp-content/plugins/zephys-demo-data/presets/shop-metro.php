<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_shop_metro()
{
    return [
        [
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ],
        [
            'key' => 'header_transparency',
            'value' => 'yes'
        ],
        [
            'key' => 'main_space_archive_product',
            'value' => [
                'mobile' => [
                    'top' => '0',
                    'bottom' => '50'
                ]
            ]
        ],
        [
            'key' => 'main_full_width_archive_product',
            'value' => 'yes'
        ],
        [
            'key' => 'active_shop_filter',
            'value' => 'off'
        ],
        [
            'key' => 'product_per_page_allow',
            'value' => '13,26,39'
        ],
        [
            'key' => 'hide_shop_toolbar',
            'value' => 'on'
        ],
        [
            'key' => 'product_per_page_default',
            'value' => '13'
        ],
        [
            'key' => 'woocommerce_toggle_grid_list',
            'value' => 'off'
        ],
        [
            'key' => 'shop_catalog_grid_style',
            'value' => 2
        ],
        [
            'key' => 'active_shop_masonry',
            'value' => 'on'
        ],
        [
            'key' => 'shop_masonry_column_type',
            'value' => 'custom'
        ],
        [
            'key' => 'product_masonry_container_width',
            'value' => 1920
        ],
        [
            'key' => 'product_masonry_item_width',
            'value' => 640
        ],
        [
            'key' => 'product_masonry_item_height',
            'value' => 370
        ],
        [
            'key' => 'woocommerce_shop_masonry_custom_columns',
            'value' => [
                'mobile' => 1,
                'mobile_landscape' => 2,
                'tablet' => 3,
                'laptop' => 3
            ]
        ],
        [
            'key' => 'enable_shop_masonry_custom_setting',
            'value' => 'on'
        ],
        [
            'key' => 'shop_masonry_item_setting',
            'value' => [
                0 => [
                    'size_name' => '1w x 2h',
                    'w'         => 1,
                    'h'         => 2,
                ],
                1 => [
                    'size_name' => '1w x 1h',
                    'w'         => 1,
                    'h'         => 1,
                ],
                2 => [
                    'size_name' => '1w x 1h',
                    'w'         => 1,
                    'h'         => 1,
                ],
                3 => [
                    'size_name' => '1w x 1h',
                    'w'         => 1,
                    'h'         => 1,
                ],
                4 => [
                    'size_name' => '1w x 1h',
                    'w'         => 1,
                    'h'         => 1,
                ],
                5 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                6 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                7 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                8 => [
                    'size_name' => '2w x 1.5h',
                    'w'         => 2,
                    'h'         => 1.5,
                ],
                9 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                10 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                11 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
                12 => [
                    'size_name' => '1w x 1.5h',
                    'w'         => 1,
                    'h'         => 1.5,
                ],
            ]
        ],
        [
            'key' => 'shop_item_space',
            'value' => [
                'tablet' => [
                    'left' => 0,
                    'right' => 0,
                    'bottom' => 0
                ],
                'mobile' => [
                    'left' => '10',
                    'right' => '10',
                    'bottom' => '30'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_background',
            'value' => [
                'background-image' => [
                    'url' => '//zephys.la-studioweb.com/wp-content/uploads/2019/05/shop-page-title-bg1.jpg'
                ],
                'background-position' => 'center center',
                'background-size' => 'cover'
            ]
        ],
        [
            'key' => 'page_title_bar_space',
            'value' => [
                'desktop' => [
                    'top' => 260,
                    'bottom' => 140,
                ],
                'laptop' => [
                    'top' => 180,
                    'bottom' => 100,
                ],
                'mobile' => [
                    'top' => 140,
                    'bottom' => 50,
                ],
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 80,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 60,
                    'unit' => 'px'
                ],
                'mobile' => [
                    'font-size' => 50,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_breadcrumb_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 18,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 16,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_family',
            'value' => [
                'color' => '#fff',
                'font-weight' => 700,
                'line-height' => 1,
            ]
        ],
        [
            'key' => 'page_title_bar_text_color|page_title_bar_link_color',
            'value' => '#fff'
        ],
        [
            'filter_name'       => 'zephys/filter/get_option',
            'filter_func'       => function( $value, $key ) {
                if( $key == 'la_custom_css'){
                    $value .= '#main #content-wrap{ max-width: 100%;} #la_shop_products nav.woocommerce-pagination { margin-top: 50px; }';
                }
                return $value;
            },
            'filter_priority'   => 10,
            'filter_args'       => 2
        ]
    ];
}