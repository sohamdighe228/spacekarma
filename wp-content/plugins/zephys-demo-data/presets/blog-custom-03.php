<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_blog_custom_03()
{
    return [
        [
            'key'           => 'layout_blog',
            'value'         => 'col-1c'
        ],
        [
            'key'           => 'main_full_width',
            'value'         => 'no'
        ],
        [
            'key'           => 'blog_design',
            'value'         => 'list-4'
        ],
        [
            'key'           => 'blog_thumbnail_height_custom',
            'value'         => '65%'
        ],
        [
            'key'           => 'blog_item_space',
            'value'         => [
                'laptop'    => [
                    'top'       => '50',
                    'right'     => '0',
                    'bottom'    => '50',
                    'left'      => '0'
                ]
            ]
        ],
        [
            'key'           => 'blog_entry_title_font_size',
            'value'         => [
                'laptop'    => [
                    'font-size'       => '36',
                    'line-height'     => '40',
                    'unit'            => 'px',
                ]
            ]
        ],
        [
            'filter_name'   => 'zephys/filter/current_title',
            'value'         => 'Blog Custom 03'
        ]
    ];
}