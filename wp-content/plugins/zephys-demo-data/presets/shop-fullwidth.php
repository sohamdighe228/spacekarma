<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_zephys_preset_shop_fullwidth()
{
    return [
        [
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ],
        [
            'key' => 'header_transparency',
            'value' => 'yes'
        ],
        [
            'key' => 'main_full_width_archive_product',
            'value' => 'yes'
        ],
        [
            'key' => 'shop_item_space',
            'value' => [
                'mobile' => [
                    'left'      => 10,
                    'right'     => 10,
                    'bottom'    => 30
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_background',
            'value' => [
                'background-image' => [
                    'url' => '//zephys.la-studioweb.com/wp-content/uploads/2019/05/shop-page-title-bg1.jpg'
                ],
                'background-position' => 'center center',
                'background-size' => 'cover'
            ]
        ],
        [
            'key' => 'page_title_bar_space',
            'value' => [
                'desktop' => [
                    'top' => 260,
                    'bottom' => 140,
                ],
                'laptop' => [
                    'top' => 180,
                    'bottom' => 100,
                ],
                'mobile' => [
                    'top' => 140,
                    'bottom' => 50,
                ],
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 80,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 60,
                    'unit' => 'px'
                ],
                'mobile' => [
                    'font-size' => 50,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_breadcrumb_font_size',
            'value' => [
                'desktop' => [
                    'font-size' => 18,
                    'unit' => 'px'
                ],
                'laptop' => [
                    'font-size' => 16,
                    'unit' => 'px'
                ]
            ]
        ],
        [
            'key' => 'page_title_bar_heading_font_family',
            'value' => [
                'color' => '#fff',
                'font-weight' => 700,
                'line-height' => 1,
            ]
        ],
        [
            'key' => 'page_title_bar_text_color|page_title_bar_link_color',
            'value' => '#fff'
        ]
    ];
}