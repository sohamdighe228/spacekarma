<?php

if (!function_exists('la_log')) {
    function la_log($log) {
        if (true === WP_DEBUG) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
    }
}

if(!function_exists('lastudio_elements_template_path')){
    function lastudio_elements_template_path(){
        return apply_filters( 'lastudio-elements/template-path', 'partials/elementor/' );
    }
}

if(!function_exists('lastudio_elements_get_template')){
    function lastudio_elements_get_template( $name = null ){
        $template = locate_template( lastudio_elements_template_path() . $name );

        if ( ! $template ) {
            $template = LASTUDIO_ELEMENTS_PATH  . 'templates/' . $name;
        }
        if ( file_exists( $template ) ) {
            return $template;
        } else {
            return false;
        }
    }
}

if(!function_exists('lastudio_elements_get_loading_icon')){
    function lastudio_elements_get_loading_icon(){
        return '<div class="la-shortcode-loading"><div class="content"><div class="la-loader spinner3"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div></div>';
    }
}

if(!function_exists('lastudio_elements_get_widgets_black_list')){
    function lastudio_elements_get_widgets_black_list( $black_list ){
        $new_black_list = array(
            'WP_Widget_Calendar',
            'WP_Widget_Pages',
            'WP_Widget_Archives',
            'WP_Widget_Media_Audio',
            'WP_Widget_Media_Image',
            'WP_Widget_Media_Gallery',
            'WP_Widget_Media_Video',
            'WP_Widget_Meta',
            'WP_Widget_Text',
            'WP_Widget_RSS',
            'WP_Widget_Custom_HTML',
            'RevSliderWidget',
            'LaStudio_Widget_Recent_Posts',
            'LaStudio_Widget_Product_Sort_By',
            'LaStudio_Widget_Price_Filter_List',
            'LaStudio_Widget_Product_Tag',
            'WP_Widget_Recent_Posts',
            'WP_Widget_Recent_Comments',
            'WC_Widget_Cart',
            'WC_Widget_Layered_Nav_Filters',
            'WC_Widget_Layered_Nav',
            'WC_Widget_Price_Filter',
            'WC_Widget_Product_Categories',
            'WC_Widget_Product_Search',
            'WC_Widget_Product_Tag_Cloud',
            'WC_Widget_Products',
            'WC_Widget_Recently_Viewed',
            'WC_Widget_Top_Rated_Products',
            'WC_Widget_Recent_Reviews',
            'WC_Widget_Rating_Filter'
        );

        $new_black_list = array_merge($black_list, $new_black_list);

        return $new_black_list;
    }
}

add_filter('elementor/widgets/black_list', 'lastudio_elements_get_widgets_black_list', 20);

if(!function_exists('lastudio_elements_get_enabled_modules')){
    function lastudio_elements_get_enabled_modules(){
        $enable_modules = get_option('lastudio_elementor_modules', array());
        $tmp = array();
        if(!empty($enable_modules)){
            foreach ($enable_modules as $module => $active ){
                if(!empty($active)){
                    $tmp[] = 'lastudio-' . $module;
                }
            }
        }
        return $tmp;
    }
}