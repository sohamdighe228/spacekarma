<?php
namespace Lastudio_Elements;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Plugin;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {	exit; } // Exit if accessed directly

/**
 * Main class plugin
 */
class LastudioPlugin {

	/**
	 * @var Plugin
	 */
	private static $_instance;

	/**
	 * @var Manager
	 */
	private $_modules_manager;

	/**
	 * @var array
	 */
	private $_localize_settings = [];


    /**
     * Check if processing elementor widget
     *
     * @var boolean
     */
    private $is_elementor_ajax = false;

	/**
	 * @return string
	 */
	public function get_version() {
		return LASTUDIO_ELEMENTS_VER;
	}

	/**
	 * Throw error on object clone
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'lastudio-elements' ), '1.0.0' );
	}

	/**
	 * Disable unserializing of the class
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'lastudio-elements' ), '1.0.0' );
	}

	/**
	 * @return Plugin
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	private function _includes() {
		require LASTUDIO_ELEMENTS_PATH . 'includes/modules-manager.php';
	}

	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		$filename = strtolower(
			preg_replace(
				[ '/^' . __NAMESPACE__ . '\\\/', '/([a-z])([A-Z])/', '/_/', '/\\\/' ],
				[ '', '$1-$2', '-', DIRECTORY_SEPARATOR ],
				$class
			)
		);

		$filename = LASTUDIO_ELEMENTS_PATH . $filename . '.php';


		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	public function get_localize_settings() {
		return $this->_localize_settings;
	}

	public function add_localize_settings( $setting_key, $setting_value = null ) {


		if ( is_array( $setting_key ) ) {
			$this->_localize_settings = array_replace_recursive( $this->_localize_settings, $setting_key );

			return;
		}

		if ( ! is_array( $setting_value ) || ! isset( $this->_localize_settings[ $setting_key ] ) || ! is_array( $this->_localize_settings[ $setting_key ] ) ) {
			$this->_localize_settings[ $setting_key ] = $setting_value;

			return;
		}

		$this->_localize_settings[ $setting_key ] = array_replace_recursive( $this->_localize_settings[ $setting_key ], $setting_value );
	}

	public function enqueue_custom_icons(){
        wp_enqueue_style(
            'lastudio-dlicon',
            LASTUDIO_ELEMENTS_URL . 'assets/css/lib/dlicon/dlicon.css',
            false,
            LASTUDIO_ELEMENTS_VER
        );

        $asset_font_without_domain = LASTUDIO_ELEMENTS_URL . 'assets/css/lib/dlicon';
        wp_add_inline_style(
            'lastudio-dlicon',
            "@font-face {
                    font-family: 'dliconoutline';
                    src: url('{$asset_font_without_domain}/dlicon.woff2') format('woff2'),
                         url('{$asset_font_without_domain}/dlicon.woff') format('woff'),
                         url('{$asset_font_without_domain}/dlicon.ttf') format('truetype');
                    font-weight: 400;
                    font-style: normal
                }"
        );
    }

    /**
	 * Enqueue frontend styles
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function enqueue_frontend_styles() {

        // Register vendor juxtapose-css styles
        wp_register_style(
            'lastudio-juxtapose-css',
            LASTUDIO_ELEMENTS_URL . 'assets/css/lib/juxtapose/juxtapose.css' ,
            false,
            '1.3.0'
        );

        $default_theme_enabled = apply_filters( 'lastudio-elements/assets/css/default-theme-enabled', true );

        if ( ! $default_theme_enabled ) {
            return;
        }

        wp_enqueue_style(
            'lastudio-elements',
            LASTUDIO_ELEMENTS_URL . 'assets/css/lastudio-elements.css' ,
            false,
            LASTUDIO_ELEMENTS_VER
        );

        if ( is_rtl() ) {
            wp_enqueue_style(
                'lastudio-elements-rtl',
                LASTUDIO_ELEMENTS_URL . 'assets/css/lastudio-elements-rtl.css' ,
                false,
                LASTUDIO_ELEMENTS_VER
            );
        }

        wp_enqueue_style(
            'lastudio-elements-skin',
            LASTUDIO_ELEMENTS_URL . 'assets/css/lastudio-elements-skin.css' ,
            false,
            LASTUDIO_ELEMENTS_VER
        );

	}

    /**
	 * Enqueue frontend scripts
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function enqueue_frontend_scripts() {

        $google_api_key = apply_filters('lastudio-elements/advanced-map/api', '');

        if ( ! empty( $google_api_key ) && ( empty( $api_disabled ) ) ) {

            wp_register_script(
                'google-maps-api',
                add_query_arg(
                    array( 'key' => $google_api_key ),
                    'https://maps.googleapis.com/maps/api/js'
                ),
                false,
                false,
                true
            );
        }

        // Register vendor isotope.pkgd.min.js script
        wp_register_script(
            'lastudio-isotope-js',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/isotope-js/isotope.pkgd.min.js' ,
            array(),
            '3.0.6',
            true
        );

        // Register vendor masonry.pkgd.min.js script
        wp_register_script(
            'lastudio-masonry-js',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/masonry-js/masonry.pkgd.min.js' ,
            array(),
            '4.2.1',
            true
        );

        wp_register_script(
            'lastudio-ease-pack',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/easing/EasePack.min.js'  ,
            null,
            null,
            true
        );

        wp_register_script(
            'lastudio-tween-max',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/easing/TweenMax.min.js'  ,
            null,
            null,
            true
        );

        // Register vendor anime.js script (https://github.com/juliangarnier/anime)
        wp_register_script(
            'lastudio-anime-js',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/anime-js/anime.min.js' ,
            [],
            '2.2.0',
            true
        );

        // Register vendor salvattore.js script (https://github.com/rnmp/salvattore)
        wp_register_script(
            'lastudio-salvattore',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/salvattore/salvattore.min.js',
            [],
            '1.0.9',
            true
        );


        // Register vendor juxtapose.js script
        wp_register_script(
            'lastudio-juxtapose',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/juxtapose/juxtapose.min.js',
            [],
            '1.3.0',
            true
        );

        // Register vendor tablesorter.js script (https://github.com/Mottie/tablesorter)
        wp_register_script(
            'jquery-tablesorter',
            LASTUDIO_ELEMENTS_URL .'assets/js/lib/tablesorter/jquery.tablesorter.min.js',
            [ 'jquery' ],
            '2.30.7',
            true
        );

        wp_register_script(
            'lastudio-elements',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lastudio-elements.min.js' ,
            [ 'jquery', 'elementor-frontend' ],
            LASTUDIO_ELEMENTS_VER,
            true
        );
	}

	public function enqueue_frontend_localize_script(){
        $this->add_localize_settings('messages', array(
            'invalidMail' => esc_html__( 'Please specify a valid e-mail', 'lastudio-elements' ),
        ));

        $this->add_localize_settings('ajaxurl', esc_url( admin_url( 'admin-ajax.php' ) ));

        wp_localize_script(
            'lastudio-elements',
            'lastudioElements',
            apply_filters( 'lastudio-elements/frontend/localize-data', $this->get_localize_settings() )
        );
    }

    /**
	 * Enqueue editor styles
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function enqueue_editor_styles() {

	    $this->enqueue_custom_icons();

		wp_enqueue_style(
			'lastudio-elements-editor',
			LASTUDIO_ELEMENTS_URL . 'assets/css/editor.css',
			[],
			LASTUDIO_ELEMENTS_VER
		);

        wp_enqueue_style(
            'lastudio-elements-font',
            LASTUDIO_ELEMENTS_URL . 'assets/css/lib/lastudioelements-font/css/lastudioelements.css' ,
            [],
            LASTUDIO_ELEMENTS_VER
        );
	}

    /**
	 * Enqueue editor scripts
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function enqueue_editor_scripts() {

        wp_enqueue_script(
            'lastudio-elements-editor',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lastudio-elements-editor.js' ,
            ['jquery'],
            LASTUDIO_ELEMENTS_VER,
            true
        );

        wp_localize_script('lastudio-elements-editor', 'LaCustomBPFE', [
            'laptop' => [
                'name' => __( 'Laptop', 'lastudio-elements' ),
                'text' => __( 'Preview for 1366px', 'lastudio-elements' )
            ],
            'tablet' => [
                'name' => __( 'Tablet Landscape', 'lastudio-elements' ),
                'text' => __( 'Preview for 1024px', 'lastudio-elements' )
            ],
            'tabletportrait' => [
                'name' => __( 'Tablet Portrait', 'lastudio-elements' ),
                'text' => __( 'Preview for 768px', 'lastudio-elements' )
            ]
        ]);

	}

	public function enqueue_panel_scripts() {}

	public function enqueue_panel_styles() {
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	}

    public function enqueue_admin_scripts() {
        wp_enqueue_style(
            'lastudio-elements-admin',
            LASTUDIO_ELEMENTS_URL . 'assets/css/lastudio-elements-admin.css',
            [],
            LASTUDIO_ELEMENTS_VER
        );
    }

	public function elementor_init() {
		$this->_modules_manager = new Modules_Manager();

		// Add element category in panel
		Plugin::instance()->elements_manager->add_category(
			'lastudio', // This is the name of your addon's category and will be used to group your widgets/elements in the Edit sidebar pane!
			[
                'title' => esc_html__( 'LaStudio Elements', 'lastudio-elements' ),
                'icon'  => 'font'
			],
			1
		);
	}

    /**
     * Rewrite core controls.
     *
     * @param  object $controls_manager Controls manager instance.
     * @return void
     */
    public function rewrite_controls( $controls_manager ) {

        $controls = array(
            $controls_manager::ICON => '\Lastudio_Elements\Controls\Control_Icon',
        );

        foreach ( $controls as $control_id => $class_name ) {

            $controls_manager->unregister_control( $control_id );
            $controls_manager->register_control( $control_id, new $class_name() );
        }

    }

    /**
     * Add new controls.
     *
     * @param  object $controls_manager Controls manager instance.
     * @return void
     */
    public function add_controls( $controls_manager ) {

        $grouped = array(
            'lastudio-box-style' => '\Lastudio_Elements\Controls\Group_Control_Box_Style',
        );

        foreach ( $grouped as $control_id => $class_name ) {
            $controls_manager->add_group_control( $control_id, new $class_name() );
        }

    }

    public function remove_default_image_sizes( $sizes ) {
        if (($key = array_search('medium_large', $sizes)) !== false) {
            unset($sizes[$key]);
        }
        return $sizes;
    }

	protected function add_actions() {
		add_action( 'elementor/init', [ $this, 'elementor_init' ] );

        add_action( 'elementor/controls/controls_registered', [ $this, 'rewrite_controls' ], 10 );
        add_action( 'elementor/controls/controls_registered', [ $this, 'add_controls' ], 10 );


        add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'enqueue_editor_scripts' ] );
        add_action( 'elementor/editor/after_enqueue_styles', [ $this, 'enqueue_editor_styles' ] );

		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'enqueue_frontend_scripts' ] );
        add_action( 'elementor/frontend/after_enqueue_scripts', [ $this, 'enqueue_frontend_localize_script' ] );
		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'enqueue_frontend_styles' ] );

        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_custom_icons' ] );
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_custom_icons' ] );
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_admin_scripts' ] );

        add_filter( 'intermediate_image_sizes', [ $this, 'remove_default_image_sizes' ], 500 );

        add_action( 'wp_ajax_elementor_render_widget', [ $this, 'set_elementor_ajax' ], 10, -1 );

        add_filter('single_template', [ $this, 'single_elementor_library_template' ] );

        if(!defined('ELEMENTOR_PRO_VERSION')){
            add_action( 'elementor/frontend/before_enqueue_scripts', [ $this, 'enqueue_motion_fx_script' ] );
        }
        add_action('elementor/element/image-gallery/section_gallery_images/before_section_end', array( $this, 'modify_image_gallery_widget'), 10, 2);
        add_action('elementor/element/image-carousel/section_additional_options/before_section_end', array( $this, 'modify_image_carousel_widget'), 10, 2);
        add_action('elementor/element/icon/section_style_icon/before_section_end', array( $this, 'modify_icon_widget'), 10, 2);
        add_action('elementor/element/divider/section_divider/before_section_end', array( $this, 'modify_divider_widget'), 10, 2);
        add_action('elementor/element/icon-box/section_style_icon/before_section_end', array( $this, 'modify_iconbox_widget'), 10, 2);
        add_action('elementor/element/tabs/section_tabs_style/before_section_end', array( $this, 'modify_tabs_widget_remove_style'), 10, 2);
        add_action('elementor/element/tabs/section_tabs_style/after_section_end', array( $this, 'modify_tabs_widget_add_section'), 10, 2);
	}

	public function single_elementor_library_template( $template ){
        if(is_singular('elementor_library')){
            return LASTUDIO_ELEMENTS_PATH . 'single-elementor_library.php';
        }
        return $template;
    }

    /**
     * Set $this->is_elementor_ajax to true on Elementor AJAX processing
     *
     * @return  void
     */
    public function set_elementor_ajax() {
        $this->is_elementor_ajax = true;
    }

    /**
     * Check if we currently in Elementor mode
     *
     * @return void
     */
    public function in_elementor() {

        $result = false;

        if ( wp_doing_ajax() ) {
            $result = $this->is_elementor_ajax;
        } elseif ( Plugin::instance()->editor->is_edit_mode() || Plugin::instance()->preview->is_preview_mode() ) {
            $result = true;
        }

        /**
         * Allow to filter result before return
         *
         * @var bool $result
         */
        return apply_filters( 'lastudio-elements/in-elementor', $result );
    }

    /**
     * Check if we currently in Elementor editor mode
     *
     * @return void
     */
    public function is_edit_mode() {

        $result = false;

        if ( Plugin::instance()->editor->is_edit_mode() ) {
            $result = true;
        }

        /**
         * Allow to filter result before return
         *
         * @var bool $result
         */
        return apply_filters( 'lastudio-elements/is-edit-mode', $result );
    }

	/**
	 * Plugin constructor.
	 */
	private function __construct() {

		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();

	}


    public function enqueue_motion_fx_script(){
        wp_register_script(
            'lastudio-sticky',
            LASTUDIO_ELEMENTS_URL . 'assets/js/lib/sticky/jquery.sticky.min.js',
            [
                'jquery',
            ],
            LASTUDIO_ELEMENTS_VER,
            true
        );

        wp_enqueue_script(
            'lastudio-motion-fx',
            LASTUDIO_ELEMENTS_URL . 'assets/js/motion-fx.min.js' ,
            [
                'elementor-frontend-modules',
                'lastudio-sticky'
            ],
            LASTUDIO_ELEMENTS_VER,
            true
        );
    }

    public function modify_image_gallery_widget( $element, $args ) {
        $columns_margin = is_rtl() ? '0 0 -{{SIZE}}{{UNIT}} -{{SIZE}}{{UNIT}};' : '0 -{{SIZE}}{{UNIT}} -{{SIZE}}{{UNIT}} 0;';
        $columns_padding = is_rtl() ? '0 0 {{SIZE}}{{UNIT}} {{SIZE}}{{UNIT}};' : '0 {{SIZE}}{{UNIT}} {{SIZE}}{{UNIT}} 0;';

        $element->remove_control('image_spacing');
        $element->remove_control('image_spacing_custom');

        $element->add_control(
            'image_spacing',
            [
                'label' => __( 'Spacing', 'lastudio-elements' ),
                'type' => 'select',
                'options' => [
                    '' => __( 'Default', 'lastudio-elements' ),
                    'custom' => __( 'Custom', 'lastudio-elements' ),
                ],
                'prefix_class' => 'gallery-spacing-',
                'default' => '',
            ]
        );

        $element->add_responsive_control(
            'image_spacing_custom',
            [
                'label' => __( 'Image Spacing', 'lastudio-elements' ),
                'type' => 'slider',
                'show_label' => false,
                'range' => [
                    'px' => [
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'size' => 15,
                ],
                'selectors' => [
                    '{{WRAPPER}} .gallery-item' => 'padding:' . $columns_padding,
                    '{{WRAPPER}} .gallery' => 'margin: ' . $columns_margin,
                ],
                'condition' => [
                    'image_spacing' => 'custom',
                ],
            ]
        );
    }

    public function modify_image_carousel_widget( $element, $args ) {

        $element->add_responsive_control(
            'custom_image_height',
            array(
                'label'      => esc_html__( 'Custom Image Height', 'lastudio-elements' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array( 'px', 'em' ),
                'range'      => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 900,
                    ),
                    'em' => array(
                        'min' => 0,
                        'max' => 100,
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} img.slick-slide-image' => 'height: {{SIZE}}{{UNIT}}; object-fit: cover;',
                ),
            )
        );

        $element->add_responsive_control(
            'slick_list_padding_left',
            array(
                'label'      => esc_html__( 'Padding Left', 'lastudio-elements' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array( '%', 'px', 'em' ),
                'range'      => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 500,
                    ),
                    '%' => array(
                        'min' => 0,
                        'max' => 50,
                    ),
                    'em' => array(
                        'min' => 0,
                        'max' => 20,
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} .slick-list' => 'padding-left: {{SIZE}}{{UNIT}};',
                ),
            )
        );

        $element->add_responsive_control(
            'slick_list_padding_right',
            array(
                'label'      => esc_html__( 'Padding Right', 'lastudio-elements' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array( '%', 'px', 'em' ),
                'range'      => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 500,
                    ),
                    '%' => array(
                        'min' => 0,
                        'max' => 50,
                    ),
                    'em' => array(
                        'min' => 0,
                        'max' => 20,
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} .slick-list' => 'padding-right: {{SIZE}}{{UNIT}};',
                ),
            )
        );
    }

    public function modify_icon_widget( $element, $args ){
        $element->remove_control('size');
        $element->remove_control('icon_padding');
        $element->remove_control('rotate');
        $element->remove_control('border_width');
        $element->remove_control('border_radius');

        $element->add_responsive_control(
            'size',
            [
                'label' => __( 'Size', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 6,
                        'max' => 300,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .elementor-icon' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $element->add_responsive_control(
            'icon_padding',
            [
                'label' => __( 'Padding', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .elementor-icon' => 'padding: {{SIZE}}{{UNIT}};',
                ],
                'range' => [
                    'em' => [
                        'min' => 0,
                        'max' => 5,
                    ],
                ],
                'condition' => [
                    'view!' => 'default',
                ],
            ]
        );

        $element->add_responsive_control(
            'rotate',
            [
                'label' => __( 'Rotate', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                    'unit' => 'deg',
                ],
                'selectors' => [
                    '{{WRAPPER}} .elementor-icon i' => 'transform: rotate({{SIZE}}{{UNIT}});',
                ],
            ]
        );

        $element->add_control(
            'border_width',
            [
                'label' => __( 'Border Width', 'lastudio-elements' ),
                'type' => Controls_Manager::DIMENSIONS,
                'selectors' => [
                    '{{WRAPPER}} .elementor-icon' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'condition' => [
                    'view' => 'framed',
                ],
            ]
        );

        $element->add_responsive_control(
            'border_radius',
            [
                'label' => __( 'Border Radius', 'lastudio-elements' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .elementor-icon' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'condition' => [
                    'view!' => 'default',
                ],
            ]
        );
    }

    public function modify_divider_widget( $element, $args ) {

        $element->remove_control('weight');

        $element->add_responsive_control(
            'weight',
            [
                'label' => __( 'Weight', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 1,
                ],
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 10,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .elementor-divider-separator' => 'border-top-width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
    }

    public function modify_iconbox_widget( $element, $args ) {

        $element->remove_control('icon_padding');

        $element->add_responsive_control(
            'icon_padding',
            [
                'label' => __( 'Padding', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .elementor-icon' => 'padding: {{SIZE}}{{UNIT}};',
                ],
                'range' => [
                    'em' => [
                        'min' => 0,
                        'max' => 5,
                    ],
                ],
                'condition' => [
                    'view!' => 'default',
                ],
            ]
        );
    }


    public function modify_tabs_widget_remove_style( $element, $args ) {
        $element->remove_control('section_tabs_style');
        $element->remove_control('navigation_width');
        $element->remove_control('border_width');
        $element->remove_control('border_color');
        $element->remove_control('background_color');
        $element->remove_control('heading_title');
        $element->remove_control('tab_color');
        $element->remove_control('tab_active_color');
        $element->remove_control('heading_content');
        $element->remove_control('content_color');
        $element->remove_group_control( 'tab_typography', 'typography' );
        $element->remove_group_control( 'content_typography', 'typography' );
    }

    public function modify_tabs_widget_add_section( $element, $args ) {
        $element->start_controls_section(
            'section_tabs_control_style',
            [
                'label' => __( 'Tabs Control', 'elementor' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $element->add_responsive_control(
            'tabs_controls_pos',
            [
                'label'   => esc_html__( 'Tabs Position', 'lastudio-elements' ),
                'type'    => Controls_Manager::CHOOSE,
                'default' => '0',
                'options' => [
                    '0'    => [
                        'title' => esc_html__( 'Left', 'lastudio-elements' ),
                        'icon'  => 'fa fa-arrow-left'
                    ],
                    '1' => [
                        'title' => esc_html__( 'Right', 'lastudio-elements' ),
                        'icon'  => 'fa fa-arrow-right'
                    ]
                ],
                'condition' => [
                    'type' => 'vertical'
                ],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tabs-wrapper' => '-ms-order: {{VALUE}}; -webkit-order: {{VALUE}}; order: {{VALUE}};',
                ]
            ]
        );

        $element->add_responsive_control(
            'navigation_width',
            [
                'label' => __( 'Navigation Width', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['%' , 'px'],
                'default' => [
                    'unit' => '%',
                ],
                'range' => [
                    '%' => [
                        'min' => 10,
                        'max' => 50,
                    ],
                    'px' => [
                        'min' => 50,
                        'max' => 1000,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .elementor-tabs-wrapper' => 'width: {{SIZE}}{{UNIT}}',
                ],
                'condition' => [
                    'type' => 'vertical',
                ]
            ]
        );

        $element->add_responsive_control(
            'tabs_controls_alignment',
            [
                'label'   => esc_html__( 'Controls Position', 'lastudio-elements' ),
                'type'    => Controls_Manager::CHOOSE,
                'default' => 'center',
                'options' => [
                    'flex-start' => [
                        'title' => esc_html__( 'Left', 'lastudio-elements' ),
                        'icon'  => 'fa fa-arrow-left',
                    ],
                    'center' => [
                        'title' => esc_html__( 'Center', 'lastudio-elements' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'flex-end' => [
                        'title' => esc_html__( 'Right', 'lastudio-elements' ),
                        'icon'  => 'fa fa-arrow-right',
                    ],
                ],
                'condition' => [
                    'type' => 'horizontal',
                ],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tabs-wrapper' => '-webkit-justify-content: {{VALUE}}; justify-content: {{VALUE}};',
                ]
            ]
        );

        $element->add_responsive_control(
            'tabs_controls_text_alignment',
            [
                'label'   => esc_html__( 'Text Alignment', 'lastudio-elements' ),
                'type'    => Controls_Manager::CHOOSE,
                'default' => 'center',
                'options' => [
                    'left'    => [
                        'title' => esc_html__( 'Left', 'lastudio-elements' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__( 'Center', 'lastudio-elements' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__( 'Right', 'lastudio-elements' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title' => 'text-align: {{VALUE}};',
                ]
            ]
        );

        $element->add_responsive_control(
            'tabs_control_wrapper_padding',
            [
                'label'      => __( 'Padding', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tabs-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $element->add_responsive_control(
            'tabs_control_wrapper_margin',
            [
                'label'      => __( 'Margin', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tabs-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $element->add_group_control(
            'border',
            [
                'name'        => 'tabs_control_wrapper_border',
                'label'       => esc_html__( 'Border', 'lastudio-elements' ),
                'default'     => '',
                'selector'    => '{{WRAPPER}} .elementor-tabs-wrapper'
            ]
        );

        $element->add_responsive_control(
            'tabs_control_wrapper_border_radius',
            [
                'label'      => __( 'Border Radius', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tabs-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $element->add_group_control(
            'box-shadow',
            [
                'name'     => 'tabs_control_wrapper_box_shadow',
                'selector' => '{{WRAPPER}} .elementor-tabs-wrapper',
            ]
        );

        $element->end_controls_section();

        $element->start_controls_section(
            'section_tabs_control_item_style',
            [
                'label' => __( 'Tabs Control Item', 'elementor' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $element->start_controls_tabs( 'tabs_control_styles' );

        $element->start_controls_tab(
            'tabs_control_normal',
            [
                'label' => esc_html__( 'Normal', 'lastudio-elements' ),
            ]
        );
        $element->add_control(
            'tabs_control_label_color',
            [
                'label'  => esc_html__( 'Text Color', 'lastudio-elements' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' => [
                    '{{WRAPPER}} .lastudio-tab-title' => 'color: {{VALUE}}',
                ]
            ]
        );
        $element->add_control(
            'tabs_control_label_bg_color',
            [
                'label'  => esc_html__( 'Background Color', 'lastudio-elements' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' => [
                    '{{WRAPPER}} .lastudio-tab-title' => 'background-color: {{VALUE}}',
                ]
            ]
        );
        $element->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'tabs_control_label_typography',
                'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .lastudio-tab-title'
            ]
        );
        $element->add_responsive_control(
            'tabs_control_label_padding',
            [
                'label'      => __( 'Padding', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        $element->add_responsive_control(
            'tabs_control_label_margin',
            [
                'label'      => __( 'Margin', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        $element->add_responsive_control(
            'tabs_control_label_border_radius',
            [
                'label'      => esc_html__( 'Border Radius', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        $element->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'        => 'tabs_control_label_border',
                'label'       => esc_html__( 'Border', 'lastudio-elements' ),
                'placeholder' => '1px',
                'default'     => '1px',
                'selector'  => '{{WRAPPER}} .lastudio-tab-title',
            ]
        );
        $element->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'     => 'tabs_control_label_box_shadow',
                'selector'  => '{{WRAPPER}} .lastudio-tab-title'
            ]
        );
        $element->end_controls_tab();

        $element->start_controls_tab(
            'tabs_control_active',
            [
                'label' => esc_html__( 'Active', 'lastudio-elements' ),
            ]
        );
        $element->add_control(
            'tabs_control_label_color_active',
            [
                'label'  => esc_html__( 'Text Color', 'lastudio-elements' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' => [
                    '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active' => 'color: {{VALUE}}'
                ]
            ]
        );
        $element->add_control(
            'tabs_control_label_bg_color_active',
            [
                'label'  => esc_html__( 'Background Color', 'lastudio-elements' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => [
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' => [
                    '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active' => 'background-color: {{VALUE}}'
                ]
            ]
        );
        $element->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'tabs_control_label_typography_active',
                'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active'
            ]
        );
        $element->add_responsive_control(
            'tabs_control_label_padding_active',
            [
                'label'      => __( 'Padding', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        $element->add_responsive_control(
            'tabs_control_label_margin_active',
            [
                'label'      => __( 'Margin', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );
        $element->add_responsive_control(
            'tabs_control_label_border_radius_active',
            [
                'label'      => esc_html__( 'Border Radius', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
                ]
            ]
        );
        $element->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'        => 'tabs_control_label_border_active',
                'label'       => esc_html__( 'Border', 'lastudio-elements' ),
                'placeholder' => '1px',
                'default'     => '1px',
                'selector'  => '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active',
            ]
        );
        $element->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'     => 'tabs_control_label_box_shadow_active',
                'selector'  => '{{WRAPPER}} .lastudio-tab-title:hover, {{WRAPPER}} .lastudio-tab-title.elementor-active'
            ]
        );
        $element->end_controls_tab();

        $element->end_controls_section();


        /**
         * Tabs Content Style Section
         */
        $element->start_controls_section(
            'section_tabs_content_style',
            [
                'label'      => esc_html__( 'Tabs Content', 'lastudio-elements' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
            ]
        );

        $element->add_control(
            'tabs_content_color',
            [
                'label' => __( 'Color', 'elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .elementor-tab-content' => 'color: {{VALUE}};',
                ],
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
            ]
        );

        $element->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'tabs_content_typography',
                'selector' => '{{WRAPPER}} .elementor-tab-content',
                'scheme' => Scheme_Typography::TYPOGRAPHY_3,
            ]
        );

        $element->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'     => 'tabs_content_background',
                'selector' => '{{WRAPPER}} .elementor-tab-content',
            ]
        );

        $element->add_responsive_control(
            'tabs_content_padding',
            [
                'label'      => __( 'Padding', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tab-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $element->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'        => 'tabs_content_border',
                'label'       => esc_html__( 'Border', 'lastudio-elements' ),
                'placeholder' => '1px',
                'default'     => '1px',
                'selectors'   => '{{WRAPPER}} .elementor-tab-content'
            ]
        );

        $element->add_responsive_control(
            'tabs_content_radius',
            [
                'label'      => __( 'Border Radius', 'lastudio-elements' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .elementor-tab-content' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $element->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'     => 'tabs_content_box_shadow',
                'selector' => '{{WRAPPER}} .elementor-tab-content'
            ]
        );

        $element->end_controls_section();
    }

}


if ( ! defined( 'LASTUDIO_ELEMENTS_TESTS' ) ) {
    // In tests we run the instance manually.
    LastudioPlugin::instance();
}