<?php
/**
 * Posts loop start template
 */

$preset = $this->get_settings_for_display('preset');

$show_image     = $this->get_settings_for_display('show_image');
$show_meta      = $this->get_settings_for_display('show_meta');
$show_author    = $this->get_settings_for_display('show_author');
$show_date      = $this->get_settings_for_display('show_date');
$show_comments  = $this->get_settings_for_display('show_comments');
$show_categories= $this->get_settings_for_display('show_categories');
$show_title     = $this->get_settings_for_display('show_title');
$show_more      = $this->get_settings_for_display('show_more');
$show_excerpt   = $this->get_settings_for_display('show_excerpt');



?>
<article class="lastudio-posts__item loop__item grid-item<?php if(has_post_thumbnail()) echo ' has-post-thumbnail'; ?>">
    <div class="lastudio-posts__inner-box"><?php

        if( $show_image == 'yes' ) {
            $srcset = ' srcset="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="';
            lastudio_elements_utility()->get_image( array(
                'size'        => $this->get_settings_for_display( 'thumb_size' ),
                'mobile_size' => $this->get_settings_for_display( 'thumb_size' ),
                'class'       => 'post-thumbnail__link',
                'html'        => '<div class="post-thumbnail"><a href="%1$s" %2$s><img class="post-thumbnail__img wp-post-image la-lazyload-image" src="%3$s" data-src="%3$s" alt="%4$s" %5$s '. $srcset .'></a></div>',
                'placeholder' => false,
                'echo'        => true,
            ), 'post', get_the_ID());
        }

        echo '<div class="lastudio-posts__inner-content">';

        if($preset == 'grid-2'){
            if( $show_meta == 'yes' ) {
                echo '<div class="post-meta">';

                lastudio_elements_utility()->get_author( array(
                    'visible' => $show_author,
                    'class'   => 'posted-by__author',
                    'prefix'  => esc_html__( 'By', 'lastudio-elements' ),
                    'html'    => '<span class="posted-by post-meta__item">%1$s<a href="%2$s" %3$s %4$s rel="author">%5$s%6$s</a></span>',
                    'echo'    => true,
                ), get_the_ID());

                lastudio_elements_utility()->get_date( array(
                    'visible' => $show_date,
                    'class'   => 'post__date-link',
                    'icon'    => '',
                    'html'    => '<span class="post__date post-meta__item">%1$s<a href="%2$s" %3$s %4$s ><time datetime="%5$s" title="%5$s">%6$s%7$s</time></a></span>',
                    'echo'    => true,
                ), get_the_ID());

                lastudio_elements_utility()->get_comment_count( array(
                    'visible' => $show_comments,
                    'class'   => 'post__comments-link',
                    'icon'    => '',
                    'prefix'  => esc_html__( 'Comments: ', 'lastudio-elements' ),
                    'html'    => '<span class="post__comments post-meta__item">%1$s<a href="%2$s" %3$s %4$s>%5$s%6$s</a></span>',
                    'echo'    => true,
                ), get_the_ID());

                lastudio_elements_utility()->get_terms( array(
                    'visible' => $show_categories,
                    'before'  => '<span class="post-terms post-meta__item">',
                    'after'	  => '</span>',
                    'echo'    => true,
                ), get_the_ID());

                echo '</div>';
            }
        }

        if($show_title == 'yes'){
            $title_length = -1;
            $title_ending = $this->get_settings_for_display( 'title_trimmed_ending_text' );

            if ( filter_var( $this->get_settings_for_display( 'title_trimmed' ), FILTER_VALIDATE_BOOLEAN ) ) {
                $title_length = $this->get_settings_for_display( 'title_length' );
            }
            lastudio_elements_utility()->get_title( array(
                'class'  => 'entry-title',
                'html'   => '<h3 %1$s><a href="%2$s">%4$s</a></h3>',
                'length' => $title_length,
                'ending' => $title_ending,
                'echo'   => true,
            ) , 'post', get_the_ID());
        }

        if($preset != 'grid-1' && $preset != 'grid-2'){
            if( $show_meta == 'yes' ) {
                echo '<div class="post-meta">';

                lastudio_elements_utility()->get_author( array(
                    'visible' => $show_author,
                    'class'   => 'posted-by__author',
                    'prefix'  => esc_html__( 'By', 'lastudio-elements' ),
                    'html'    => '<span class="posted-by post-meta__item">%1$s<a href="%2$s" %3$s %4$s rel="author">%5$s%6$s</a></span>',
                    'echo'    => true,
                ), get_the_ID());

                lastudio_elements_utility()->get_date( array(
                    'visible' => $show_date,
                    'class'   => 'post__date-link',
                    'icon'    => '',
                    'html'    => '<span class="post__date post-meta__item">%1$s<a href="%2$s" %3$s %4$s ><time datetime="%5$s" title="%5$s">%6$s%7$s</time></a></span>',
                    'echo'    => true,
                ), get_the_ID());

                lastudio_elements_utility()->get_comment_count( array(
                    'visible' => $show_comments,
                    'class'   => 'post__comments-link',
                    'icon'    => '',
                    'prefix'  => esc_html__( 'Comments: ', 'lastudio-elements' ),
                    'html'    => '<span class="post__comments post-meta__item">%1$s<a href="%2$s" %3$s %4$s>%5$s%6$s</a></span>',
                    'echo'    => true,
                ), get_the_ID());

                lastudio_elements_utility()->get_terms( array(
                    'visible' => $show_categories,
                    'before'  => '<span class="post-terms post-meta__item">',
                    'after'	  => '</span>',
                    'echo'    => true,
                ), get_the_ID());

                echo '</div>';
            }
        }

        if($show_excerpt == 'yes'){
            lastudio_elements_utility()->get_content( array(
                'length'       => intval( $this->get_settings_for_display( 'excerpt_length' ) ),
                'content_type' => 'post_excerpt',
                'html'         => '<div %1$s>%2$s</div>',
                'class'        => 'entry-excerpt',
                'echo'         => true,
            ), 'post', get_the_ID()  );
        }


        if($show_more == 'yes'){
            lastudio_elements_utility()->get_button( array(
                'class' => 'btn btn-primary elementor-button elementor-size-md lastudio-more',
                'text'  => $this->get_settings_for_display( 'more_text' ),
                'icon'  => sprintf('<i class="lastudio-more-icon %1$s"></i>', esc_attr($this->get_settings_for_display( 'more_icon' ))),
                'html'  => '<div class="lastudio-more-wrap"><a href="%1$s" %3$s><span class="btn__text">%4$s</span>%5$s</a></div>',
                'echo'  => true,
            ), 'post', get_the_ID());
        }

        echo '</div>';

        ?></div>
</article>