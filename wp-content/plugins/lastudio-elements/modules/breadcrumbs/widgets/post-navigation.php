<?php
namespace Lastudio_Elements\Modules\Breadcrumbs\Widgets;

use Lastudio_Elements\Base\Lastudio_Widget;

// Elementor Classes
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Lastudio_Elements\Classes\Utils;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Post_Navigation Widget
 */
class Post_Navigation extends Lastudio_Widget {

    public function get_name() {
        return 'lastudio-post-navigation';
    }

    protected function get_widget_title() {
        return esc_html__( 'Post Navigation', 'lastudio-elements' );
    }

    public function get_icon() {
        return 'eicon-post-navigation';
    }

    public function get_keywords() {
        return [ 'post', 'navigation', 'menu', 'links' ];
    }

    protected function _register_controls() {
        $this->start_controls_section(
            'section_post_navigation_content',
            [
                'label' => __( 'Post Navigation', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'show_label',
            [
                'label' => __( 'Label', 'lastudio-elements' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'lastudio-elements' ),
                'label_off' => __( 'Hide', 'lastudio-elements' ),
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'prev_label',
            [
                'label' => __( 'Previous Label', 'lastudio-elements' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'Previous', 'lastudio-elements' ),
                'condition' => [
                    'show_label' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'next_label',
            [
                'label' => __( 'Next Label', 'lastudio-elements' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'Next', 'lastudio-elements' ),
                'condition' => [
                    'show_label' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'all_label',
            [
                'label' => __( 'All Label', 'lastudio-elements' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'View All', 'lastudio-elements' ),
                'condition' => [
                    'show_label' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'show_arrow',
            [
                'label' => __( 'Arrows', 'lastudio-elements' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'lastudio-elements' ),
                'label_off' => __( 'Hide', 'lastudio-elements' ),
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'arrow',
            [
                'label' => __( 'Arrows Type', 'lastudio-elements' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'lastudioicon-left-arrow' => __( 'Default', 'lastudio-elements' ),
                    'fa fa-angle-left' => __( 'Angle', 'lastudio-elements' ),
                    'fa fa-angle-double-left' => __( 'Double Angle', 'lastudio-elements' ),
                    'fa fa-chevron-left' => __( 'Chevron', 'lastudio-elements' ),
                    'fa fa-chevron-circle-left' => __( 'Chevron Circle', 'lastudio-elements' ),
                    'fa fa-caret-left' => __( 'Caret', 'lastudio-elements' ),
                    'fa fa-arrow-left' => __( 'Arrow', 'lastudio-elements' ),
                    'fa fa-long-arrow-left' => __( 'Long Arrow', 'lastudio-elements' ),
                    'fa fa-arrow-circle-left' => __( 'Arrow Circle', 'lastudio-elements' ),
                    'fa fa-arrow-circle-o-left' => __( 'Arrow Circle Negative', 'lastudio-elements' ),
                ],
                'default' => 'lastudioicon-left-arrow',
                'condition' => [
                    'show_arrow' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'show_title',
            [
                'label' => __( 'Post Title', 'lastudio-elements' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'lastudio-elements' ),
                'label_off' => __( 'Hide', 'lastudio-elements' ),
                'default' => 'no',
            ]
        );

        // Filter out post type without taxonomies
        $post_type_options = [];
        $post_type_taxonomies = [];
        foreach ( Utils::get_public_post_types() as $post_type => $post_type_label ) {
            $taxonomies = Utils::get_taxonomies( [ 'object_type' => $post_type ], false );
            if ( empty( $taxonomies ) ) {
                continue;
            }

            $post_type_options[ $post_type ] = $post_type_label;
            $post_type_taxonomies[ $post_type ] = [];
            foreach ( $taxonomies as $taxonomy ) {
                $post_type_taxonomies[ $post_type ][ $taxonomy->name ] = $taxonomy->label;
            }
        }

        $this->add_control(
            'in_same_term',
            [
                'label' => __( 'In same Term', 'lastudio-elements' ),
                'type' => Controls_Manager::SELECT2,
                'options' => $post_type_options,
                'default' => '',
                'multiple' => true,
                'label_block' => true,
                'description' => __( 'Indicates whether next post must be within the same taxonomy term as the current post, this lets you set a taxonomy per each post type', 'lastudio-elements' ),
            ]
        );

        foreach ( $post_type_options as $post_type => $post_type_label ) {
            $this->add_control(
                $post_type . '_taxonomy',
                [
                    'label' => $post_type_label . ' ' . __( 'Taxonomy', 'lastudio-elements' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => $post_type_taxonomies[ $post_type ],
                    'default' => '',
                    'condition' => [
                        'in_same_term' => $post_type,
                    ],
                ]
            );
        }

        $this->add_control(
            'show_all_link',
            [
                'label' => __( 'Show View All', 'lastudio-elements' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'lastudio-elements' ),
                'label_off' => __( 'Hide', 'lastudio-elements' ),
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'all_button_link',
            array(
                'label'       => esc_html__( 'View All Link', 'lastudio-elements' ),
                'type'        => Controls_Manager::URL,
                'placeholder' => 'http://your-link.com',
                'default' => array(
                    'url' => '#',
                ),
                'dynamic' => array( 'active' => true ),
                'condition' => [
                    'show_all_link' => 'yes',
                ],
            )
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'label_style',
            [
                'label' => __( 'Label', 'lastudio-elements' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'show_label' => 'yes',
                ],
            ]
        );

        $this->start_controls_tabs( 'tabs_label_style' );

        $this->start_controls_tab(
            'label_color_normal',
            [
                'label' => __( 'Normal', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'label_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_3,
                ],
                'selectors' => [
                    '{{WRAPPER}} span.post-navigation__link-label' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'label_color_hover',
            [
                'label' => __( 'Hover', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'label_hover_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} span.post-navigation__link-label:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'label_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_2,
                'selector' => '{{WRAPPER}} span.post-navigation__link-label',
                'exclude' => [ 'line_height' ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'title_style',
            [
                'label' => __( 'Title', 'lastudio-elements' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'show_title' => 'yes',
                ],
            ]
        );

        $this->start_controls_tabs( 'tabs_post_navigation_style' );

        $this->start_controls_tab(
            'tab_color_normal',
            [
                'label' => __( 'Normal', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'text_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ],
                'selectors' => [
                    '{{WRAPPER}} span.post-navigation__prev--title, {{WRAPPER}} span.post-navigation__next--title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'tab_color_hover',
            [
                'label' => __( 'Hover', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'hover_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} span.post-navigation__prev--title:hover, {{WRAPPER}} span.post-navigation__next--title:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_2,
                'selector' => '{{WRAPPER}} span.post-navigation__prev--title, {{WRAPPER}} span.post-navigation__next--title',
                'exclude' => [ 'line_height' ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'arrow_style',
            [
                'label' => __( 'Arrow', 'lastudio-elements' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'show_arrow' => 'yes',
                ],
            ]
        );

        $this->start_controls_tabs( 'tabs_post_navigation_arrow_style' );

        $this->start_controls_tab(
            'arrow_color_normal',
            [
                'label' => __( 'Normal', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'arrow_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-navigation__arrow-wrapper' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'arrow_color_hover',
            [
                'label' => __( 'Hover', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'arrow_hover_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .post-navigation__arrow-wrapper:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'arrow_size',
            [
                'label' => __( 'Size', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 6,
                        'max' => 300,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .post-navigation__arrow-wrapper' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'arrow_padding',
            [
                'label' => __( 'Gap', 'lastudio-elements' ),
                'type' => Controls_Manager::SLIDER,
                'selectors' => [
                    'body:not(.rtl) {{WRAPPER}} .post-navigation__arrow-prev' => 'padding-right: {{SIZE}}{{UNIT}};',
                    'body:not(.rtl) {{WRAPPER}} .post-navigation__arrow-next' => 'padding-left: {{SIZE}}{{UNIT}};',
                    'body.rtl {{WRAPPER}} .post-navigation__arrow-prev' => 'padding-left: {{SIZE}}{{UNIT}};',
                    'body.rtl {{WRAPPER}} .post-navigation__arrow-next' => 'padding-right: {{SIZE}}{{UNIT}};',
                ],
                'range' => [
                    'em' => [
                        'min' => 0,
                        'max' => 5,
                    ],
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_active_settings();

        $prev_label = '';
        $next_label = '';
        $all_label = '';
        $prev_arrow = '';
        $next_arrow = '';


        if ('yes' === $settings['show_label'] ) {
            $prev_label = '<span class="post-navigation__prev--label post-navigation__link-label">' . $settings['prev_label'] . '</span>';
            $next_label = '<span class="post-navigation__next--label post-navigation__link-label">' . $settings['next_label'] . '</span>';
            $all_label = '<span class="post-navigation__all--label post-navigation__link-label">' . $settings['all_label'] . '</span>';
        }

        if ( 'yes' === $settings['show_arrow'] ) {
            if ( is_rtl() ) {
                $prev_icon_class = str_replace( 'left', 'right', $settings['arrow'] );
                $next_icon_class = $settings['arrow'];
            } else {
                $prev_icon_class = $settings['arrow'];
                $next_icon_class = str_replace( 'left', 'right', $settings['arrow'] );
            }

            $prev_arrow = '<span class="post-navigation__arrow-wrapper post-navigation__arrow-prev"><i class="' . $prev_icon_class . '" aria-hidden="true"></i><span class="elementor-screen-only">' . esc_html__( 'Previous', 'lastudio-elements' ) . '</span></span>';
            $next_arrow = '<span class="post-navigation__arrow-wrapper post-navigation__arrow-next"><i class="' . $next_icon_class . '" aria-hidden="true"></i><span class="elementor-screen-only">' . esc_html__( 'Next', 'lastudio-elements' ) . '</span></span>';
        }

        $prev_title = '';
        $next_title = '';

        if ( 'yes' === $settings['show_title'] ) {
            $prev_title = '<span class="post-navigation__prev--title">%title</span>';
            $next_title = '<span class="post-navigation__next--title">%title</span>';
        }

        $in_same_term = false;
        $taxonomy = 'category';
        $post_type = get_post_type( get_queried_object_id() );

        if ( ! empty( $settings['in_same_term'] ) && is_array( $settings['in_same_term'] ) && in_array( $post_type, $settings['in_same_term'] ) ) {
            if ( isset( $settings[ $post_type . '_taxonomy' ] ) ) {
                $in_same_term = true;
                $taxonomy = $settings[ $post_type . '_taxonomy' ];
            }
        }


        $allow_view_all = false;
        if('yes' === $settings['show_all_link']){
            $allow_view_all = true;
        }
        $button_url = $this->get_settings_for_display( 'all_button_link' );
        if ( empty( $button_url ) ) {
            $allow_view_all = false;
        }

        if ( is_array( $button_url ) && empty( $button_url['url'] ) ) {
            $allow_view_all = false;
        }

        if ( is_array( $button_url ) ) {
            $this->add_render_attribute( 'url', 'href', $button_url['url'] );

            if ( $button_url['is_external'] ) {
                $this->add_render_attribute( 'url', 'target', '_blank' );
            }

            if ( ! empty( $button_url['nofollow'] ) ) {
                $this->add_render_attribute( 'url', 'rel', 'nofollow' );
            }

        }
        else {
            $this->add_render_attribute( 'url', 'href', $button_url );
        }

        ?>
        <div class="elementor-post-navigation elementor-grid<?php if ( 'yes' !== $settings['show_label'] ) { echo ' has-tooltip';} ?>">
            <div class="elementor-post-navigation__prev elementor-post-navigation__link">
                <?php previous_post_link( '%link', $prev_arrow . '<span class="elementor-post-navigation__link__prev">' . $prev_label . $prev_title . '</span>', $in_same_term, '', $taxonomy ); ?>
            </div>
            <?php if( $allow_view_all ): ?>
            <div class="elementor-post-navigation__all elementor-post-navigation__link">
                <a <?php echo $this->get_render_attribute_string( 'url' ); ?>>
                    <span class="post-navigation__arrow-wrapper post-navigation__arrow-all"><i class="lastudioicon-microsoft" aria-hidden="true"></i><span class="elementor-screen-only"><?php echo esc_html(!empty($settings['all_label']) ? $settings['all_label'] : __( 'View All', 'lastudio-elements' ) ); ?></span></span>
                    <span class="elementor-post-navigation__link__all"><?php echo $all_label; ?></span>
                </a>
            </div>
            <?php endif; ?>
            <div class="elementor-post-navigation__next elementor-post-navigation__link">
                <?php next_post_link( '%link', '<span class="elementor-post-navigation__link__next">' . $next_label . $next_title . '</span>' . $next_arrow, $in_same_term, '', $taxonomy ); ?>
            </div>
        </div>
        <?php
    }

}