<?php
namespace Lastudio_Elements\Modules\Breadcrumbs\Widgets;

use Lastudio_Elements\Base\Lastudio_Widget;

// Elementor Classes
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Breadcrumbs Widget
 */
class Breadcrumbs extends Lastudio_Widget {

    public function get_name() {
        return 'lastudio-breadcrumbs';
    }

    protected function get_widget_title() {
        return esc_html__( 'Breadcrumbs', 'lastudio-elements' );
    }

    public function get_icon() {
        return 'eicon-yoast';
    }

    public function get_keywords() {
        return [ 'yoast', 'seo', 'breadcrumbs', 'internal links' ];
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'section_breadcrumbs_content',
            [
                'label' => __( 'Breadcrumbs', 'lastudio-elements' ),
            ]
        );

        $this->add_responsive_control(
            'align',
            [
                'label' => __( 'Alignment', 'lastudio-elements' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'lastudio-elements' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'lastudio-elements' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'lastudio-elements' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'prefix_class' => 'elementor%s-align-',
            ]
        );

        $this->add_control(
            'html_tag',
            [
                'label' => __( 'HTML Tag', 'lastudio-elements' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '' => __( 'Default', 'lastudio-elements' ),
                    'div' => 'div',
                    'nav' => 'nav',
                    'span' => 'span',
                ],
                'default' => '',
            ]
        );

        $this->add_control(
            'html_description',
            [
                'raw' => __( 'Additional settings are available in the Theme Options', 'lastudio-elements' ) . ' ' . sprintf( '<a href="%s" target="_blank">%s</a>', admin_url( 'themes.php?page=theme_options#tab=20' ), __( 'Breadcrumbs Panel', 'lastudio-elements' ) ),
                'type' => Controls_Manager::RAW_HTML,
                'content_classes' => 'elementor-descriptor',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_style',
            [
                'label' => __( 'Breadcrumbs', 'lastudio-elements' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'typography',
                'selector' => '{{WRAPPER}}',
                'scheme' => Scheme_Typography::TYPOGRAPHY_2,
            ]
        );

        $this->add_control(
            'text_color',
            [
                'label' => __( 'Text Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}}' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->start_controls_tabs( 'tabs_breadcrumbs_style' );

        $this->start_controls_tab(
            'tab_color_normal',
            [
                'label' => __( 'Normal', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'link_color',
            [
                'label' => __( 'Link Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'tab_color_hover',
            [
                'label' => __( 'Hover', 'lastudio-elements' ),
            ]
        );

        $this->add_control(
            'link_hover_color',
            [
                'label' => __( 'Color', 'lastudio-elements' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    private function get_html_tag() {
        $html_tag = $this->get_settings( 'html_tag' );

        if ( empty( $html_tag ) ) {
            $html_tag = 'nav';
        }

        return $html_tag;
    }

    protected function render() {
        do_action('lastudio-elements/render_breadcrumbs_output', $this->get_html_tag());
    }

}