<?php
/**
 * functions
 *
 * @package lastudio-wpjm
 * @author  LaStudio
 * @license GPL-2.0+
 * @copyright  2019, LaStudio
 */
 
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if(!function_exists('lastudio_wpjm_string_to_bool')){
    function lastudio_wpjm_string_to_bool( $value ){
        return ( is_bool( $value ) && $value ) || in_array( $value, array( 1, '1', 'true', 'yes' ), true );
    }
}

/**
 *
 * Output for [jobs] shortcode
 *
 * @param $atts
 * @return string
 */
if(!function_exists('lastudio_wpjm_job_shortcode_outputs')){
    function lastudio_wpjm_job_shortcode_outputs( $atts ){

        global $lastudio_listing_loop;

        ob_start();

        $atts = shortcode_atts(
            apply_filters(
                'job_manager_output_jobs_defaults',
                array(
                    'per_page'                  => get_option( 'job_manager_per_page' ),
                    'orderby'                   => 'featured',
                    'order'                     => 'DESC',

                    // Filters + cats.
                    'show_filters'              => true,
                    'show_categories'           => true,
                    'show_category_multiselect' => get_option( 'job_manager_enable_default_category_multiselect', false ),
                    'show_pagination'           => false,
                    'show_more'                 => true,

                    // Limit what jobs are shown based on category, post status, and type.
                    'categories'                => '',
                    'job_types'                 => '',
                    'post_status'               => '',
                    'featured'                  => null, // True to show only featured, false to hide featured, leave null to show both.
                    'filled'                    => null, // True to show only filled, false to hide filled, leave null to show both/use the settings.

                    // Default values for filters.
                    'location'                  => '',
                    'keywords'                  => '',
                    'selected_category'         => '',
                    'selected_job_types'        => implode( ',', array_values( get_job_listing_types( 'id=>slug' ) ) ),

                    // LA-Studio extra fields
                    'la_carousel_settings'   => '',
                    'regions'                => '',
                    'layout_type'            => 'grid', // Accept value contains grid,list ..
                    'preset'                 => 'type-1',
                    'columns'                => 3,
                    'columns_laptop'         => '',
                    'columns_tablet'         => '',
                    'columns_tabletportrait' => '',
                    'columns_mobile'         => '',
                    'image_size'             => 'full',
                    'element_listing_id'     => '',
                    'show_search_size_range' => false,
                    'show_search_bathroom'   => false,
                    'show_search_bedroom'    => false,
                    'show_search_price_range'=> false,
                    'show_search_button'     => false,
                    'search_button_text'     => 'Find Properties',

                )
            ), $atts
        );

        $lastudio_listing_loop = array(
            'layout_type' => $atts['layout_type'],
            'preset' => $atts['preset'],
            'image_size' => $atts['image_size'],
            'columns' => $atts['columns'],
            'columns_laptop' => $atts['columns_laptop'],
            'columns_tablet' => $atts['columns_tablet'],
            'columns_tabletportrait' => $atts['columns_tabletportrait'],
            'columns_mobile' => $atts['columns_mobile'],
            'la_carousel_settings' => $atts['la_carousel_settings'],
        );

        if ( ! get_option( 'job_manager_enable_categories' ) ) {
            $atts['show_categories'] = false;
        }

        // String and bool handling.
        $atts['show_filters']              = lastudio_wpjm_string_to_bool( $atts['show_filters'] );
        $atts['show_categories']           = lastudio_wpjm_string_to_bool( $atts['show_categories'] );
        $atts['show_category_multiselect'] = lastudio_wpjm_string_to_bool( $atts['show_category_multiselect'] );
        $atts['show_more']                 = lastudio_wpjm_string_to_bool( $atts['show_more'] );
        $atts['show_pagination']           = lastudio_wpjm_string_to_bool( $atts['show_pagination'] );

        if ( ! is_null( $atts['featured'] ) ) {
            $atts['featured'] = ( is_bool( $atts['featured'] ) && $atts['featured'] ) || in_array( $atts['featured'], array( 1, '1', 'true', 'yes' ), true );
        }

        if ( ! is_null( $atts['filled'] ) ) {
            $atts['filled'] = ( is_bool( $atts['filled'] ) && $atts['filled'] ) || in_array( $atts['filled'], array( 1, '1', 'true', 'yes' ), true );
        }

        // Array handling.
        $atts['categories']         = is_array( $atts['categories'] ) ? $atts['categories'] : array_filter( array_map( 'trim', explode( ',', $atts['categories'] ) ) );
        $atts['job_types']          = is_array( $atts['job_types'] ) ? $atts['job_types'] : array_filter( array_map( 'trim', explode( ',', $atts['job_types'] ) ) );
        $atts['post_status']        = is_array( $atts['post_status'] ) ? $atts['post_status'] : array_filter( array_map( 'trim', explode( ',', $atts['post_status'] ) ) );
        $atts['selected_job_types'] = is_array( $atts['selected_job_types'] ) ? $atts['selected_job_types'] : array_filter( array_map( 'trim', explode( ',', $atts['selected_job_types'] ) ) );

        $atts['regions']            = is_array( $atts['regions'] ) ? $atts['regions'] : array_filter( array_map( 'trim', explode( ',', $atts['regions'] ) ) );

        // Get keywords and location from querystring if set.
        if ( ! empty( $_GET['search_keywords'] ) ) {
            $atts['keywords'] = sanitize_text_field( $_GET['search_keywords'] );
        }
        if ( ! empty( $_GET['search_location'] ) ) {
            $atts['location'] = sanitize_text_field( $_GET['search_location'] );
        }
        if ( ! empty( $_GET['search_category'] ) ) {
            $atts['selected_category'] = sanitize_text_field( $_GET['search_category'] );
        }

        $data_attributes = array(
            'location'        => $atts['location'],
            'keywords'        => $atts['keywords'],
            'show_filters'    => $atts['show_filters'] ? 'true' : 'false',
            'show_pagination' => $atts['show_pagination'] ? 'true' : 'false',
            'per_page'        => $atts['per_page'],
            'orderby'         => $atts['orderby'],
            'order'           => $atts['order'],
            'categories'      => implode( ',', $atts['categories'] ),
        );
        if ( $atts['show_filters'] ) {
            get_job_manager_template(
                'job-filters.php',
                array(
                    'per_page'                  => $atts['per_page'],
                    'orderby'                   => $atts['orderby'],
                    'order'                     => $atts['order'],
                    'show_categories'           => $atts['show_categories'],
                    'categories'                => $atts['categories'],
                    'selected_category'         => $atts['selected_category'],
                    'job_types'                 => $atts['job_types'],
                    'atts'                      => $atts,
                    'location'                  => $atts['location'],
                    'keywords'                  => $atts['keywords'],
                    'selected_job_types'        => $atts['selected_job_types'],
                    'show_category_multiselect' => $atts['show_category_multiselect'],
                    'search_regions'            => $atts['regions'],
                )
            );

            get_job_manager_template( 'job-listings-start.php' );
            get_job_manager_template( 'job-listings-end.php' );

            if ( ! $atts['show_pagination'] && $atts['show_more'] ) {
                echo '<a class="load_more_jobs" href="#" style="display:none;"><strong>' . esc_html__( 'Load more listings', 'wp-job-manager' ) . '</strong></a>';
            }
        }
        else {
            $jobs = get_job_listings(
                apply_filters(
                    'job_manager_output_jobs_args',
                    array(
                        'search_location'   => $atts['location'],
                        'search_keywords'   => $atts['keywords'],
                        'post_status'       => $atts['post_status'],
                        'search_categories' => $atts['categories'],
                        'job_types'         => $atts['job_types'],
                        'orderby'           => $atts['orderby'],
                        'order'             => $atts['order'],
                        'posts_per_page'    => $atts['per_page'],
                        'featured'          => $atts['featured'],
                        'filled'            => $atts['filled'],
                        'search_regions'    => $atts['regions'],
                    )
                )
            );

            if ( ! empty( $atts['job_types'] ) ) {
                $data_attributes['job_types'] = implode( ',', $atts['job_types'] );
            }

            if ( $jobs->have_posts() ) {
                get_job_manager_template( 'job-listings-start.php' );
                while ( $jobs->have_posts() ) {
                    $jobs->the_post();
                    get_job_manager_template_part( 'content', 'job_listing' );
                }
                get_job_manager_template( 'job-listings-end.php' );
                if ( $jobs->found_posts > $atts['per_page'] && $atts['show_more'] ) {
                    wp_enqueue_script( 'wp-job-manager-ajax-filters' );
                    if ( $atts['show_pagination'] ) {
                        echo get_job_listing_pagination( $jobs->max_num_pages ); // WPCS: XSS ok.
                    } else {
                        echo '<a class="load_more_jobs" href="#"><strong>' . esc_html__( 'Load more listings', 'wp-job-manager' ) . '</strong></a>';
                    }
                }
            } else {
                do_action( 'job_manager_output_jobs_no_results' );
            }
            wp_reset_postdata();
        }

        $data_attributes_string = '';

        if(!empty($atts['show_search_button']) && !empty($atts['search_button_text'])){
            $data_attributes['externalsearch'] = 'true';
        }

        if ( ! is_null( $atts['featured'] ) ) {
            $data_attributes['featured'] = $atts['featured'] ? 'true' : 'false';
        }
        if ( ! is_null( $atts['filled'] ) ) {
            $data_attributes['filled'] = $atts['filled'] ? 'true' : 'false';
        }
        if ( ! empty( $atts['post_status'] ) ) {
            $data_attributes['post_status'] = implode( ',', $atts['post_status'] );
        }
        foreach ( $data_attributes as $key => $value ) {
            $data_attributes_string .= 'data-' . esc_attr( $key ) . '="' . esc_attr( $value ) . '" ';
        }

        if ( !empty( $atts['element_listing_id'] ) ) {
            $data_attributes_string = 'id="'.esc_attr(trim(preg_replace('/\s+/', '', $atts['element_listing_id']))).'" ' . $data_attributes_string;
        }

        $job_listings_output = apply_filters( 'job_manager_job_listings_output', ob_get_clean() );

        return '<div class="job_listings'.(!empty($atts['show_search_button']) && !empty($atts['search_button_text']) ? ' is-externalsearch' : '').'" ' . $data_attributes_string . '>' . $job_listings_output . '</div>';
    }
}

/**
 * Override the [jobs] shortcode
 */
if(!function_exists('lastudio_wpjm_override_shortcode')){
    function lastudio_wpjm_override_shortcode(){
        remove_shortcode('jobs');
        add_shortcode('jobs', 'lastudio_wpjm_job_shortcode_outputs');
        remove_all_actions('job_manager_job_filters_end', 20);
    }
    add_action('after_setup_theme', 'lastudio_wpjm_override_shortcode');
}

/**
 *
 */

if(!function_exists('lastudio_wpjm_override_query_args_job_get_listings')){
    function lastudio_wpjm_override_query_args_job_get_listings( $query_args, $args ){
        if ( ! empty( $args['search_regions'] ) ) {
            if(!isset($query_args['tax_query'])){
                $query_args['tax_query'] = array();
            }
            $field                     = is_numeric( $args['search_regions'][0] ) ? 'term_id' : 'slug';
            $operator                  = 'IN';
            $query_args['tax_query'][] = array(
                'taxonomy'         => 'job_listing_category',
                'field'            => $field,
                'terms'            => array_values( $args['search_regions'] ),
                'include_children' => 'AND' !== $operator,
                'operator'         => $operator,
            );
        }

        return $query_args;
    }
    add_filter('job_manager_get_listings', 'lastudio_wpjm_override_query_args_job_get_listings', 20, 2);
}

/**
 * Add type to listing search
 */
if(!function_exists('lastudio_wpjm_add_types_to_search')){
    function lastudio_wpjm_add_types_to_search( $atts ){
        $job_types          = is_array( $atts['job_types'] ) ? $atts['job_types'] : array_filter( array_map( 'trim', explode( ',', $atts['job_types'] ) ) );
        $selected_job_types = is_array( $atts['selected_job_types'] ) ? $atts['selected_job_types'] : array_filter( array_map( 'trim', explode( ',', $atts['selected_job_types'] ) ) );
        get_job_manager_template(
            'job-filter-job-types.php',
            array(
                'job_types'          => $job_types,
                'atts'               => $atts,
                'selected_job_types' => $selected_job_types,
            )
        );
    }
    add_action('job_manager_job_filters_search_jobs_end', 'lastudio_wpjm_add_types_to_search', 20);
}

/**
 * Add size to search
 */
if(!function_exists('lastudio_wpjm_add_size_to_search')){
    function lastudio_wpjm_add_size_to_search( $atts ){
        if($atts['show_search_size_range']){
            $size_lists = array(
                '0-2000'        => '0 -> 2000 Sq Ft',
                '2000-3000'     => '2000 -> 3000 Sq Ft',
                '3000-4000'     => '3000 -> 4000 Sq Ft',
                '4000'          => '> 4000 Sq Ft',
            );
            ?>
            <div class="search_size_range">
                <select class="lastudio-wpjm-select" name="filter_size_range">
                    <option value=""><?php esc_html_e('Any Size', 'lastudio-wpjm') ?></option>
                    <?php foreach ($size_lists as $val => $label): ?>
                    <option value="<?php echo esc_attr($val); ?>"><?php echo esc_attr($label); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
<?php
        }
    }
    add_action('job_manager_job_filters_search_jobs_end', 'lastudio_wpjm_add_size_to_search', 25);
}

/**
 * Add bathroom to search
 */
if(!function_exists('lastudio_wpjm_add_bathroom_to_search')){
    function lastudio_wpjm_add_bathroom_to_search( $atts ){
        if($atts['show_search_bathroom']){
            ?>
            <div class="search_bathroom">
                <select class="lastudio-wpjm-select" name="filter_bathroom">
                    <option value=""><?php esc_html_e('Any Bathroom', 'lastudio-wpjm') ?></option>
                    <?php for( $i = 1; $i < 10; $i++): ?>
                    <option value="<?php echo esc_attr($i); ?>"><?php echo $i > 0 && $i < 10 ? '0' . $i : $i; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
<?php
        }
    }
    add_action('job_manager_job_filters_search_jobs_end', 'lastudio_wpjm_add_bathroom_to_search', 30);
}

/**
 * Add bathroom to search
 */
if(!function_exists('lastudio_wpjm_add_bedroom_to_search')){
    function lastudio_wpjm_add_bedroom_to_search( $atts ){
        if($atts['show_search_bedroom']){
            ?>
            <div class="search_bedroom">
                <select class="lastudio-wpjm-select" name="filter_bedroom">
                    <option value=""><?php esc_html_e('Any Bedroom', 'lastudio-wpjm') ?></option>
                    <?php for( $i = 1; $i < 10; $i++): ?>
                        <option value="<?php echo esc_attr($i); ?>"><?php echo $i > 0 && $i < 10 ? '0' . $i : $i; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
<?php
        }
    }
    add_action('job_manager_job_filters_search_jobs_end', 'lastudio_wpjm_add_bedroom_to_search', 35);
}

/**
 * Add price to search
 */
if(!function_exists('lastudio_wpjm_add_price_range_to_search')){
    function lastudio_wpjm_add_price_range_to_search( $atts ){
        if($atts['show_search_price_range']){
            $price_lists = array(
                '0-2000'        => '0 -> $2000',
                '2000-3000'     => '$2000 -> $3000',
                '3000-4000'     => '$3000 -> $4000',
                '4000'          => '> $4000',
            );
            ?>
            <div class="search_price_range">
                <select class="lastudio-wpjm-select" name="filter_price_range">
                    <option value=""><?php esc_html_e('Prices', 'lastudio-wpjm') ?></option>
                    <?php foreach ($price_lists as $val => $label): ?>
                        <option value="<?php echo esc_attr($val); ?>"><?php echo esc_attr($label); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
<?php
        }
    }
    add_action('job_manager_job_filters_search_jobs_end', 'lastudio_wpjm_add_price_range_to_search', 35);
}

/**
 * Add Search button
 */
if(!function_exists('lastudio_wpjm_add_button_to_search')){
    function lastudio_wpjm_add_button_to_search( $atts ){
        if(empty($atts['show_search_button']) || empty($atts['search_button_text'])){
            return;
        }
?><div class="lastudio-search-submit">
    <button class="search-submit button" name="submit"><i class="lastudioicon-search-property"></i><span><?php echo esc_html($atts['search_button_text']); ?></span></button>
        <input type="hidden" class="fake-form-action" data-action="<?php $jobs_page_id = get_option( 'job_manager_jobs_page_id' ); echo get_the_permalink($jobs_page_id); ?>" value="<?php echo get_the_permalink($jobs_page_id); ?>"/>
</div><?php
    }
    add_action('job_manager_job_filters_search_jobs_end', 'lastudio_wpjm_add_button_to_search', 35);
}

/**
 * Remove the default search button
 */

add_filter('job_manager_job_filters_show_submit_button', '__return_false');