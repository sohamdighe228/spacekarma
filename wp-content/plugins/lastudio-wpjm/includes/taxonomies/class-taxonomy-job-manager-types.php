<?php
/**
 * Categories
 *
 * @package lastudio-wpjm
 * @author  LaStudio
 * @license GPL-2.0+
 * @copyright  2019, LaStudio
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LaStudioWPJM_Taxonomy_Types{

	/**
	 *
	 */
	public static function init() {
		add_filter( 'register_taxonomy_job_listing_type_args', array( __CLASS__, 'change_taxonomy_type_label' ), 10 );
	}

	public static function change_taxonomy_type_label($args) {
		$singular = esc_html__( 'Listing Type', 'lastudio-wpjm' );
		$plural   = esc_html__( 'Listings Types', 'lastudio-wpjm' );

		$args['label']  = $plural;
		$args['labels'] = array(
			'name'              => $plural,
			'singular_name'     => $singular,
			'menu_name'         => esc_html__( 'Types', 'lastudio-wpjm' ),
			'search_items'      => sprintf( esc_html__( 'Search %s', 'lastudio-wpjm' ), $plural ),
			'all_items'         => sprintf( esc_html__( 'All %s', 'lastudio-wpjm' ), $plural ),
			'parent_item'       => sprintf( esc_html__( 'Parent %s', 'lastudio-wpjm' ), $singular ),
			'parent_item_colon' => sprintf( esc_html__( 'Parent %s:', 'lastudio-wpjm' ), $singular ),
			'edit_item'         => sprintf( esc_html__( 'Edit %s', 'lastudio-wpjm' ), $singular ),
			'update_item'       => sprintf( esc_html__( 'Update %s', 'lastudio-wpjm' ), $singular ),
			'add_new_item'      => sprintf( esc_html__( 'Add New %s', 'lastudio-wpjm' ), $singular ),
			'new_item_name'     => sprintf( esc_html__( 'New %s Name', 'lastudio-wpjm' ), $singular )
		);

		if ( isset( $args['rewrite'] ) && is_array( $args['rewrite'] ) ) {
			$args['rewrite']['slug'] = _x( 'listing-type', 'Listing type slug - resave permalinks after changing this', 'lastudio-wpjm' );
		}

		return $args;
	}

}

LaStudioWPJM_Taxonomy_Types::init();