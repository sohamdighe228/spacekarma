<?php
/**
 * Regions
 *
 * @package lastudio-wpjm
 * @author  LaStudio
 * @license GPL-2.0+
 * @copyright  2019, LaStudio
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LaStudioWPJM_Taxonomy_Regions{

	/**
	 *
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'definition' ), 1 );
	}

	/**
	 *
	 */
	public static function definition() {
		$labels = array(
			'name'              => __( 'Regions', 'lastudio-wpjm' ),
			'singular_name'     => __( 'Region', 'lastudio-wpjm' ),
			'search_items'      => __( 'Search Regions', 'lastudio-wpjm' ),
			'all_items'         => __( 'All Regions', 'lastudio-wpjm' ),
			'parent_item'       => __( 'Parent Region', 'lastudio-wpjm' ),
			'parent_item_colon' => __( 'Parent Region:', 'lastudio-wpjm' ),
			'edit_item'         => __( 'Edit', 'lastudio-wpjm' ),
			'update_item'       => __( 'Update', 'lastudio-wpjm' ),
			'add_new_item'      => __( 'Add New', 'lastudio-wpjm' ),
			'new_item_name'     => __( 'New Region', 'lastudio-wpjm' ),
			'menu_name'         => __( 'Regions', 'lastudio-wpjm' ),
		);

		register_taxonomy( 'job_listing_region', 'job_listing', array(
			'labels'            => apply_filters( 'lastudio_wpjm_taxomony_booking_amenities_labels', $labels ),
			'hierarchical'      => true,
			'query_var'         => 'region',
			'rewrite'           => array( 'slug' => __( 'region', 'lastudio-wpjm' ) ),
			'public'            => true,
			'show_ui'           => true,
			'show_in_rest'		=> true
		) );
	}
}

LaStudioWPJM_Taxonomy_Regions::init();