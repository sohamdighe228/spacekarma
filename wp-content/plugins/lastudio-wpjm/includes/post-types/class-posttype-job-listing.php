<?php
/**
 * Categories
 *
 * @package lastudio-wpjm
 * @author  LaStudio
 * @license GPL-2.0+
 * @copyright  2019, LaStudio
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class LaStudioWPJM_Job_Listing{

	public static function init() {

        add_action( 'add_meta_boxes', array( __CLASS__, 'add_meta_boxes' ), 30 );
        add_action( 'job_manager_save_job_listing', array( __CLASS__, 'save_job_listing_data' ), 30, 2 );

		add_filter( 'register_post_type_job_listing', array( __CLASS__, 'change_post_type_label' ), 10 );
        add_filter( 'job_manager_job_listing_data_fields', array( __CLASS__, 'add_custom_fields' ), 1 );

        add_filter( 'submit_job_form_fields', array( __CLASS__, 'custom_submit_job_form_fields' ) );

	}

	public static function add_meta_boxes(){
        add_meta_box( 'job_manager_job_listing_gallery', __( 'Product gallery', 'lastudio-wpjm' ), array(__CLASS__, 'metabox_output'), 'job_listing', 'side', 'low' );
    }

	public static function change_post_type_label($args) {
        $singular = esc_html__( 'Listing', 'lastudio-wpjm' );
        $plural   = esc_html__( 'Listings', 'lastudio-wpjm' );

        $args['labels']      = array(
            'name'               => $plural,
            'singular_name'      => $singular,
            'menu_name'          => $plural,
            'all_items'          => sprintf( esc_html__( 'All %s', 'lastudio-wpjm' ), $plural ),
            'add_new'            => esc_html__( 'Add New', 'lastudio-wpjm' ),
            'add_new_item'       => sprintf( esc_html__( 'Add %s', 'lastudio-wpjm' ), $singular ),
            'edit'               => esc_html__( 'Edit', 'lastudio-wpjm' ),
            'edit_item'          => sprintf( esc_html__( 'Edit %s', 'lastudio-wpjm' ), $singular ),
            'new_item'           => sprintf( esc_html__( 'New %s', 'lastudio-wpjm' ), $singular ),
            'view'               => sprintf( esc_html__( 'View %s', 'lastudio-wpjm' ), $singular ),
            'view_item'          => sprintf( esc_html__( 'View %s', 'lastudio-wpjm' ), $singular ),
            'search_items'       => sprintf( esc_html__( 'Search %s', 'lastudio-wpjm' ), $plural ),
            'not_found'          => sprintf( esc_html__( 'No %s found', 'lastudio-wpjm' ), $plural ),
            'not_found_in_trash' => sprintf( esc_html__( 'No %s found in trash', 'lastudio-wpjm' ), $plural ),
            'parent'             => sprintf( esc_html__( 'Parent %s', 'lastudio-wpjm' ), $singular )
        );
        $args['description'] = sprintf( esc_html__( 'This is where you can create and manage %s.', 'lastudio-wpjm' ), $plural );
        $args['supports']    = array( 'title', 'editor', 'custom-fields', 'publicize', 'comments', 'thumbnail' );
        $args['rewrite']     = array( 'slug' => esc_html__( 'listings', 'lastudio-wpjm' ) );
        $args['has_archive'] = esc_html__( 'listings', 'lastudio-wpjm' );

        return $args;
	}

	public static function metabox_output( $post ){
        wp_enqueue_style( 'lastudio-wpjm-admin-css' );
        wp_enqueue_script( 'jquery-ui-sortable' );
	    wp_enqueue_script('lastudio-wpjm-admin-scripts');
        ?>
        <div id="wpjm_listing_images_container">
            <ul class="product_images">
                <?php
                $product_image_gallery = get_post_meta( $post->ID, '_company_image_gallery', true);
                $product_image_gallery = explode(',', $product_image_gallery);

                $attachments         = array_filter( $product_image_gallery );
                $update_meta         = false;
                $updated_gallery_ids = array();

                if ( ! empty( $attachments ) ) {
                    foreach ( $attachments as $attachment_id ) {
                        $attachment = wp_get_attachment_image( $attachment_id, 'thumbnail' );

                        // if attachment is empty skip.
                        if ( empty( $attachment ) ) {
                            $update_meta = true;
                            continue;
                        }

                        echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . $attachment . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . esc_attr__( 'Delete image', 'lastudio-wpjm' ) . '">' . __( 'Delete', 'lastudio-wpjm' ) . '</a></li>
								</ul>
							</li>';

                        // rebuild ids to be saved.
                        $updated_gallery_ids[] = $attachment_id;
                    }

                    // need to update product meta to set new gallery ids
                    if ( $update_meta ) {
                        update_post_meta( $post->ID, '_company_image_gallery', implode( ',', $updated_gallery_ids ) );
                    }
                }
                ?>
            </ul>
            <input type="hidden" id="_company_image_gallery" name="_company_image_gallery" value="<?php echo esc_attr( implode( ',', $updated_gallery_ids ) ); ?>" />
        </div>
        <p class="wpjm_add_listing_images hide-if-no-js">
            <a href="#" data-choose="<?php esc_attr_e( 'Add images to listing gallery', 'lastudio-wpjm' ); ?>" data-update="<?php esc_attr_e( 'Add to gallery', 'lastudio-wpjm' ); ?>" data-delete="<?php esc_attr_e( 'Delete image', 'lastudio-wpjm' ); ?>" data-text="<?php esc_attr_e( 'Delete', 'lastudio-wpjm' ); ?>"><?php _e( 'Add listing gallery images', 'lastudio-wpjm' ); ?></a>
        </p>
        <?php
    }

    public static function add_custom_fields( $fields ) {

        // reorder fields

        $fields['_company_twitter']['priority'] = 2.6;

        $fields['_company_website']['priority'] = 1.1;
        $fields['_company_tagline']['priority'] = 1.2;
        $fields['_job_expires']['priority'] = 1.3;


        $fields['_company_price'] = array(
            'label' => esc_html__('Price', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => '',
            'priority' => 1.5
        );

        $fields['_company_price_text'] = array(
            'label' => esc_html__('Price text', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => '',
            'priority' => 1.6
        );

        $fields['_company_bedroom'] = array(
            'label' => esc_html__('Number of bedroom', 'lastudio-wpjm'),
            'type' => 'select',
            'required' => false,
            'priority' => 1.7,
            'options' => array(
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10
            )
        );

        $fields['_company_bathroom'] = array(
            'label' => esc_html__('Number of bathroom', 'lastudio-wpjm'),
            'type' => 'select',
            'required' => false,
            'priority' => 1.8,
            'options' => array(
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10
            )
        );

        $fields['_company_gara'] = array(
            'label' => esc_html__('Number of garages', 'lastudio-wpjm'),
            'type' => 'select',
            'required' => false,
            'priority' => 1.9,
            'options' => array(
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10
            )
        );

        $fields['_company_size'] = array(
            'label' => esc_html__('Size', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => '',
            'priority' => 2.0
        );

        $fields['_company_size_text'] = array(
            'label' => esc_html__('Size Text', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => '',
            'priority' => 2.1
        );

        $fields['_company_phone'] = array(
            'label'       => esc_html__( 'Phone', 'lastudio-wpjm' ),
            'type'        => 'text',
            'placeholder' => esc_html__( 'e.g +84-669-996', 'lastudio-wpjm' ),
            'priority'    => 2.4,
            'required' => false,
        );

        $fields['_company_email'] = array(
            'label'       => esc_html__( 'Email', 'lastudio-wpjm' ),
            'type'        => 'text',
            'placeholder' => esc_html__( 'youremail@email.com', 'lastudio-wpjm' ),
            'description' => '',
            'priority'    => 2.5,
            'required' => false,
        );

        $fields['_company_twitter']['label'] = esc_html__('Twitter url', 'lastudio-wpjm');
        $fields['_company_twitter']['placeholder'] = esc_html__('http://twitter.com/yourusername', 'lastudio-wpjm');
        $fields['_company_facebook'] = array(
            'label' => esc_html__('Facebook url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://facebook.com/yourusername', 'lastudio-wpjm'),
            'priority' => 2.7
        );
        $fields['_company_linkedin'] = array(
            'label' => esc_html__('Linkedin url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://linkedin.com/yourusername', 'lastudio-wpjm'),
            'priority' => 2.8
        );
        $fields['_company_pinterest'] = array(
            'label' => esc_html__('Pinterest url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://pinterest.com/yourusername', 'lastudio-wpjm'),
            'priority' => 2.9
        );
        $fields['_company_instagram'] = array(
            'label' => esc_html__('Instagram url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://instagram.com/yourusername', 'lastudio-wpjm'),
            'priority' => 3.0
        );


        unset( $fields['_company_logo'] );
        unset( $fields['_company_name'] );
        unset( $fields['_application'] );
        return $fields;
    }

    public static function save_job_listing_data( $post_id, $post ) {
        if ( isset( $_POST[ '_company_image_gallery' ] ) ) {
            update_post_meta( $post_id, '_company_image_gallery', sanitize_text_field($_POST['_company_image_gallery']) );
        }
    }

    public static function custom_submit_job_form_fields( $fields ) {

	    array_walk_recursive( $fields, array( __CLASS__, 'replace_jobs_with_listings' ) );

        // Section 2
        $fields['job']['job_title']['label']       = esc_html__( 'Listing Name', 'lastudio-wpjm' );
        $fields['job']['job_title']['placeholder'] = esc_html__( 'Your listing name', 'lastudio-wpjm' );
        $fields['job']['job_title']['section'] = 'section-2';

        // tagline
        $fields['company']['company_tagline']['section'] = 'section-2';
        $fields['company']['company_tagline']['priority'] = 2;

        // category
        $fields['job']['job_category']['priority']    = 2.4;
        $fields['job']['job_category']['placeholder'] = esc_html__( 'Choose one or more categories', 'lastudio-wpjm' );

        $fields['job']['job_category']['label'] = esc_html__( 'Listing category', 'lastudio-wpjm' );
        $fields['job']['job_category']['description'] = sprintf( '<span class="description_tooltip right">%s</span>', esc_html__( 'Visitors can filter their search by the categories they want - so make sure you choose them wisely and include all the relevant ones', 'lastudio-wpjm' ) );
        $fields['job']['job_category']['section'] = 'section-2';

        // region
        $fields['job']['job_regions'] = array(
            'type' => 'term-select',
            'taxonomy' => 'job_listing_region',
            'priority' => 2.5,
            'required' => false,
            'placeholder' => esc_html__( 'Add Region', 'lastudio-wpjm' ),
            'label' => esc_html__( 'Listing Region', 'lastudio-wpjm' ),
            'description' => '',
            'default' => '',
            'section' => 'section-2',
        );


        $fields['company']['company_price'] = array(
            'label' => esc_html__('Price', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'priority' => 3.3,
            'placeholder' => esc_html__( 'Number only', 'lastudio-wpjm' ),
            'section' => 'section-2',
        );
        $fields['company']['company_price_text'] = array(
            'label' => esc_html__('Price Text', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'priority' => 3.5,
            'placeholder' => esc_html__( 'e.g $100', 'lastudio-wpjm' ),
            'section' => 'section-2',
        );

        $fields['company']['company_bedroom'] = array(
            'label' => esc_html__('Number of bedroom', 'lastudio-wpjm'),
            'type' => 'select',
            'required' => false,
            'priority' => 3.6,
            'options' => array(
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10
            ),
            'section' => 'section-2',
        );

        $fields['company']['company_bathroom'] = array(
            'label' => esc_html__('Number of bathroom', 'lastudio-wpjm'),
            'type' => 'select',
            'required' => false,
            'priority' => 3.7,
            'options' => array(
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10
            ),
            'section' => 'section-2',
        );

        $fields['company']['company_gara'] = array(
            'label' => esc_html__('Number of garages', 'lastudio-wpjm'),
            'type' => 'select',
            'required' => false,
            'priority' => 3.8,
            'options' => array(
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10
            ),
            'section' => 'section-2',
        );

        // Section 3
        $fields['job']['job_description']['priority']    = 2.2;
        $fields['job']['job_description']['placeholder'] = esc_html__( 'An overview of your listing and the things you love about it.', 'lastudio-wpjm' );
        $fields['job']['job_description']['section'] = 'section-3';


        // Section 4
        $fields['job']['job_location']['priority'] = 2.3;
        $fields['job']['job_location']['placeholder'] = esc_html__( 'e.g 34 Wigmore Street, London', 'lastudio-wpjm' );
        $fields['job']['job_location']['description'] = esc_html__( 'Leave this blank if the location is not important.', 'lastudio-wpjm' );
        $fields['job']['job_location']['section'] = 'section-4';


        $fields['company']['company_phone'] = array(
            'label'       => esc_html__( 'Phone', 'lastudio-wpjm' ),
            'type'        => 'text',
            'placeholder' => esc_html__( 'e.g +84-669-996', 'lastudio-wpjm' ),
            'required'    => false,
            'priority'    => 3.7,
            'section' => 'section-4'
        );
        $fields['company']['company_email'] = array(
            'label'       => esc_html__( 'Email', 'lastudio-wpjm' ),
            'type'        => 'text',
            'placeholder' => esc_html__( 'e.g youremail@email.com', 'lastudio-wpjm' ),
            'required'    => false,
            'priority'    => 3.8,
            'section' => 'section-4'
        );

        $fields['company']['company_website']['priority']    = 3.2;
        $fields['company']['company_website']['placeholder'] = esc_html__( 'e.g yourwebsite.com, London', 'lastudio-wpjm' );
        $fields['company']['company_website']['description'] = sprintf( '<span class="description_tooltip left">%s</span>', esc_html__( 'You can add more similar panels to better help the user fill the form', 'lastudio-wpjm' ) );
        $fields['company']['company_website']['section'] = 'section-4';

        // Section 5

        // Section 6
        $fields['company']['company_logo']['label']       = esc_html__( 'Featured Image', 'lastudio-wpjm' );
        $fields['company']['company_logo']['priority']    = 2.7;
        $fields['company']['company_logo']['multiple']    = false;
        $fields['company']['company_logo']['description'] = esc_html__( 'The image will be shown on listing cards.', 'lastudio-wpjm' );
        $fields['company']['company_logo']['section'] = 'section-6';

        $fields['company']['gallery_images'] = array(
            'label' => esc_html__( 'Gallery Images', 'lastudio-wpjm' ),
            'priority' => 2.9,
            'required' => false,
            'type' => 'file',
            'ajax' => true,
            'placeholder' => '',
            'allowed_mime_types' => $fields['company']['company_logo']['allowed_mime_types'],
            'multiple' => true,
            'section' => 'section-6',
        );

        $fields['company']['company_video']['priority']    = 3.0;
        $fields['company']['company_video']['section']    = 'section-6';


        // Section 6
        $fields['company']['company_twitter']['label'] = esc_html__('Twitter url', 'lastudio-wpjm');
        $fields['company']['company_twitter']['placeholder'] = esc_html__('http://twitter.com/yourusername', 'lastudio-wpjm');
        $fields['company']['company_twitter']['section'] = 'section-7';
        $fields['company']['company_facebook'] = array(
            'label' => esc_html__('Facebook url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://facebook.com/yourusername', 'lastudio-wpjm'),
            'priority' => 7,
            'section'    => 'section-7'
        );
        $fields['company']['company_linkedin'] = array(
            'label' => esc_html__('Linkedin url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://linkedin.com/yourusername', 'lastudio-wpjm'),
            'priority' => 7.1,
            'section'    => 'section-7'
        );

        $fields['company']['company_instagram'] = array(
            'label' => esc_html__('Instagram url', 'lastudio-wpjm'),
            'type' => 'text',
            'required' => false,
            'placeholder' => esc_html__('http://instagram.com/yourusername', 'lastudio-wpjm'),
            'priority' => 7.2,
            'section'    => 'section-7'
        );

        // temporary unsets
        unset( $fields['job']['job_type'] );
        unset( $fields['company']['company_name'] );
        unset( $fields['job']['application'] );

        return $fields;
    }

    public static function replace_jobs_with_listings( &$item, $key ){
        if ( $item === 'Job Listings' ) {
            $item = esc_html__( 'Listing', 'lastudio-wpjm' );
        }

        if ( $item === 'Job Submission' ) {
            $item = esc_html__( 'Submission', 'lastudio-wpjm' );
        }

        if ( $key === 'desc' || $key === 'any' || $key === 'all' || $key === 'label' ) {
            if ( is_numeric( strpos( $item, 'Job' ) ) ) {
                $item = str_replace( 'Job', esc_html__( 'Listing', 'lastudio-wpjm' ), $item );
            }
        }
    }
}

LaStudioWPJM_Job_Listing::init();