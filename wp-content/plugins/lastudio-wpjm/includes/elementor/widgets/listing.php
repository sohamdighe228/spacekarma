<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class LaStudio_WPJM_Widgets_Listing extends Widget_Base {

    public function get_name() {
        return 'lastudio-listing';
    }

    public function get_categories() {
        return [ 'lastudio' ];
    }

    public function get_title() {
        return __( 'LaStudio Listing', 'lastudio-wpjm' );
    }

    public function get_icon() {
        return 'eicon-bullet-list';
    }

    public function get_keywords() {
        return [ 'listing', 'property', 'estate' ];
    }

    public function get_script_depends() {
        return [ 'lastudio-wpjm-scripts'];
    }

    public function get_style_depends() {
        return ['lastudio-wpjm-style'];
    }

    protected function _register_controls() {

        $css_scheme = apply_filters(
            'lastudio-wpjm/listing/css-scheme',
            array(
                'wrap'             => 'ul.job_listings',
                'item'             => 'li.job_listing',
                'image_wrap'       => '.listing__image figure',
                'image_instance'   => '.listing__image figure img',
                'slick_list'       => '.job_listings .slick-list',
                'content'          => '.listing__content',
                'content_inner'    => '.listing__content-inner',
                'title'            => '.listing__content-title',
                'price'            => '.listing__content-price',
                'meta'             => '.listing__content-meta',
                'meta_icon'        => '.listing__content-metaitem > i',
                'search_wrap'      => '.job_filters .search_jobs',
                'search_field'     => '.job_filters .search_jobs > div',
                'search_button_wrap' => '.job_filters .search_jobs > div.lastudio-search-submit',
                'search_button'    => '.job_filters .search_jobs > div.lastudio-search-submit .search-submit'
            )
        );

        $preset_type = apply_filters(
            'lastudio-wpjm/listing/control/preset',
            array(
                'type-1' => esc_html__( 'Type-1', 'lastudio-wpjm' ),
                'type-2' => esc_html__( 'Type-2', 'lastudio-wpjm' ),
                'type-3' => esc_html__( 'Type-3', 'lastudio-wpjm' ),
            )
        );


        $this->start_controls_section(
            'section_query',
            [
                'label' => __( 'Query', 'lastudio-wpjm' ),
            ]
        );

        $this->add_control(
            'show_filters',
            array(
                'label'        => esc_html__( 'Show Filters', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true'
            )
        );

        $this->add_control(
            'show_search_size_range',
            array(
                'label'        => esc_html__( 'Show Search Size Range', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'condition' => array(
                    'show_filters' => 'true'
                )
            )
        );

        $this->add_control(
            'show_search_bathroom',
            array(
                'label'        => esc_html__( 'Show Search Bathroom', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'condition' => array(
                    'show_filters' => 'true'
                )
            )
        );

        $this->add_control(
            'show_search_bedroom',
            array(
                'label'        => esc_html__( 'Show Search Bedroom', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'condition' => array(
                    'show_filters' => 'true'
                )
            )
        );

        $this->add_control(
            'show_search_price_range',
            array(
                'label'        => esc_html__( 'Show Search Price Range', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'condition' => array(
                    'show_filters' => 'true'
                )
            )
        );

        $this->add_control(
            'show_search_button',
            array(
                'label'        => esc_html__( 'Show Search Button', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'condition' => array(
                    'show_filters' => 'true',
                )
            )
        );

        $this->add_control(
            'search_button_text',
            [
                'label' => __( 'Button Text', 'elementor' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Find Properties',
                'condition' => array(
                    'show_filters' => 'true',
                    'show_search_button' => 'true',
                )
            ]
        );

        if(lastudio_wpjm_string_to_bool(get_option('job_manager_enable_categories'))){
            $this->add_control(
                'categories',
                [
                    'label' => __( 'Listing Category', 'lastudio-wpjm' ),
                    'type' => 'lastudio_query',
                    'post_type' => 'job_listing',
                    'options' => [],
                    'label_block' => true,
                    'multiple' => true,
                    'filter_type' => 'taxonomy',
                    'include_type' => false,
                    'object_type' => 'job_listing_category',
                ]
            );
        }

        if(lastudio_wpjm_string_to_bool(get_option('job_manager_enable_types'))) {
            $this->add_control('job_types', [
                'label' => __('Listing Types', 'lastudio-wpjm'),
                'type' => 'lastudio_query',
                'post_type' => 'job_listing',
                'options' => [],
                'label_block' => true,
                'multiple' => lastudio_wpjm_string_to_bool(get_option('job_manager_multi_job_type')),
                'filter_type' => 'taxonomy',
                'include_type' => false,
                'object_type' => 'job_listing_type',
            ]);
        }

        $this->add_control(
            'regions',
            [
                'label' => __( 'Listing Regions', 'lastudio-wpjm' ),
                'type' => 'lastudio_query',
                'post_type' => 'job_listing',
                'options' => [],
                'label_block' => true,
                'multiple' => true,
                'filter_type' => 'taxonomy',
                'include_type' => false,
                'object_type' => 'job_listing_region',
            ]
        );

        $this->add_control(
            'post_status',
            [
                'label' => __( 'Status', 'lastudio-wpjm' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'publish',
                'options' => [
                    'publish'     => __( 'Publish', 'lastudio-wpjm' ),
                    'expired'     => __( 'Expired', 'lastudio-wpjm' )
                ]
            ]
        );

        $this->add_control(
            'per_page',
            [
                'label' => __( 'Posts Per Page', 'lastudio-wpjm' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 3
            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __( 'Order By', 'lastudio-wpjm' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'featured',
                'options' => [
                    'title'     => __( 'Title', 'lastudio-wpjm' ),
                    'ID'        => __( 'ID', 'lastudio-wpjm' ),
                    'name'      => __( 'Name', 'lastudio-wpjm' ),
                    'date'      => __( 'Date', 'lastudio-wpjm' ),
                    'modified'  => __( 'Modified', 'lastudio-wpjm' ),
                    'rand'      => __( 'Random', 'lastudio-wpjm' ),
                    'featured'  => __( 'Featured', 'lastudio-wpjm' ),
                    'rand_featured'  => __( 'Random Featured', 'lastudio-wpjm' )
                ]
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __( 'Order', 'lastudio-wpjm' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'desc',
                'options' => [
                    'asc' => __( 'ASC', 'lastudio-wpjm' ),
                    'desc' => __( 'DESC', 'lastudio-wpjm' ),
                ]
            ]
        );

        $this->add_control(
            'show_pagination',
            [
                'label' => __( 'Pagination', 'lastudio-wpjm' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => '',
                'return_value' => 'true'
            ]
        );

        $this->add_control(
            'show_more',
            [
                'label' => __( 'Show Load More', 'lastudio-wpjm' ),
                'type' => Controls_Manager::SWITCHER,
                'return_value' => 'true',
                'default' => 'true',
                'condition' => array(
                    'show_pagination!' => 'true'
                )
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_settings',
            array(
                'label' => esc_html__( 'Layout', 'lastudio-wpjm' ),
            )
        );

        $this->add_control(
            'layout_type',
            array(
                'label'   => esc_html__( 'Layout Type', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'grid',
                'options' => array(
                    'grid'    => esc_html__( 'Grid', 'lastudio-wpjm' )
                ),
            )
        );

        $this->add_control(
            'preset',
            array(
                'label'   => esc_html__( 'Preset', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'type-1',
                'options' => $preset_type
            )
        );

        $this->add_control(
            'image_size',
            array(
                'type'       => 'select',
                'label'      => esc_html__( 'Images Size', 'lastudio-wpjm' ),
                'default'    => 'full',
                'options'    => lastudio_elements_tools()->get_image_sizes()
            )
        );

        $this->add_control(
            'enable_custom_image_height',
            array(
                'label'        => esc_html__( 'Enable Custom Image Height', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'default'      => ''
            )
        );

        $this->add_responsive_control(
            'custom_image_height',
            array(
                'label' => esc_html__( 'Image Height', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    'px' => array(
                        'min' => 100,
                        'max' => 1000,
                    ),
                    '%' => [
                        'min' => 0,
                        'max' => 200,
                    ],
                    'vh' => array(
                        'min' => 0,
                        'max' => 100,
                    )
                ),
                'size_units' => ['px', '%', 'vh'],
                'default' => [
                    'size' => 300,
                    'unit' => 'px'
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['image_wrap'] => 'padding-bottom: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} ' . $css_scheme['image_instance'] => 'object-fit: cover; position: absolute; width: 100%; height: 100%',
                ),
                'render_type' => 'template',
                'condition' => [
                    'enable_custom_image_height!' => ''
                ]
            )
        );

        $this->add_responsive_control(
            'columns',
            array(
                'label'   => esc_html__( 'Columns', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 3,
                'options' => lastudio_elements_tools()->get_select_range( 6 )
            )
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_carousel',
            array(
                'label' => esc_html__( 'Carousel', 'lastudio-wpjm' ),
                'condition' => array(
                    'layout_type' => array(
                        'grid',
                        'list'
                    )
                )
            )
        );

        $this->add_control(
            'carousel_enabled',
            array(
                'label'        => esc_html__( 'Enable Carousel', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'yes',
                'default'      => '',
            )
        );

        $this->add_control(
            'slides_to_scroll',
            array(
                'label'     => esc_html__( 'Slides to Scroll', 'lastudio-wpjm' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => '1',
                'options'   => lastudio_elements_tools()->get_select_range( 6 ),
                'condition' => array(
                    'columns!' => '1',
                ),
            )
        );

        $this->add_control(
            'arrows',
            array(
                'label'        => esc_html__( 'Show Arrows Navigation', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'default'      => 'true',
            )
        );

        $this->add_control(
            'prev_arrow',
            array(
                'label'   => esc_html__( 'Prev Arrow Icon', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'fa fa-angle-left',
                'options' => lastudio_elements_tools()->get_available_prev_arrows_list(),
                'condition' => array(
                    'arrows' => 'true',
                ),
            )
        );

        $this->add_control(
            'next_arrow',
            array(
                'label'   => esc_html__( 'Next Arrow Icon', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'fa fa-angle-right',
                'options' => lastudio_elements_tools()->get_available_next_arrows_list(),
                'condition' => array(
                    'arrows' => 'true',
                ),
            )
        );

        $this->add_control(
            'dots',
            array(
                'label'        => esc_html__( 'Show Dots Navigation', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'default'      => '',
            )
        );

        $this->add_control(
            'pause_on_hover',
            array(
                'label'        => esc_html__( 'Pause on Hover', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'default'      => '',
            )
        );

        $this->add_control(
            'autoplay',
            array(
                'label'        => esc_html__( 'Autoplay', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'default'      => 'true',
            )
        );

        $this->add_control(
            'autoplay_speed',
            array(
                'label'     => esc_html__( 'Autoplay Speed', 'lastudio-wpjm' ),
                'type'      => Controls_Manager::NUMBER,
                'default'   => 5000,
                'condition' => array(
                    'autoplay' => 'true',
                ),
            )
        );

        $this->add_control(
            'infinite',
            array(
                'label'        => esc_html__( 'Infinite Loop', 'lastudio-wpjm' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => esc_html__( 'Yes', 'lastudio-wpjm' ),
                'label_off'    => esc_html__( 'No', 'lastudio-wpjm' ),
                'return_value' => 'true',
                'default'      => 'true',
            )
        );

        $this->add_control(
            'effect',
            array(
                'label'   => esc_html__( 'Effect', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'slide',
                'options' => array(
                    'slide' => esc_html__( 'Slide', 'lastudio-wpjm' ),
                    'fade'  => esc_html__( 'Fade', 'lastudio-wpjm' ),
                ),
                'condition' => array(
                    'columns' => '1',
                ),
            )
        );

        $this->add_control(
            'speed',
            array(
                'label'   => esc_html__( 'Animation Speed', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::NUMBER,
                'default' => 500,
            )
        );

        $this->add_responsive_control(
            'slick_list_padding_left',
            array(
                'label'      => esc_html__( 'Padding Left', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array( '%', 'px', 'em' ),
                'range'      => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 500,
                    ),
                    '%' => array(
                        'min' => 0,
                        'max' => 50,
                    ),
                    'em' => array(
                        'min' => 0,
                        'max' => 20,
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['slick_list'] . '' => 'padding-left: {{SIZE}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'slick_list_padding_right',
            array(
                'label'      => esc_html__( 'Padding Right', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array( '%', 'px', 'em' ),
                'range'      => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 500,
                    ),
                    '%' => array(
                        'min' => 0,
                        'max' => 50,
                    ),
                    'em' => array(
                        'min' => 0,
                        'max' => 20,
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['slick_list'] . '' => 'padding-right: {{SIZE}}{{UNIT}};',
                ),
            )
        );

        $this->end_controls_section();


        /**
         * Search Box Style Section
         */
        $this->start_controls_section(
            'section_search_style',
            array(
                'label'      => esc_html__( 'Search Box', 'lastudio-wpjm' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
                'condition' => array(
                    'show_filters' => 'true'
                )
            )
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            array(
                'name'     => 'searchbox_background',
                'selector' => '{{WRAPPER}} ' . $css_scheme['search_wrap'],
            )
        );

        $this->add_responsive_control(
            'searchbox_padding',
            array(
                'label'      => __( 'Box Padding', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['search_wrap'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'searchbox_margin',
            array(
                'label'      => __( 'Box Margin', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['search_wrap'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'sfield_width',
            array(
                'label' => esc_html__( 'Search Field Width', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    '%' => array(
                        'min' => 20,
                        'max' => 100,
                    )
                ),
                'size_units' => ['%'],
                'default' => [
                    'size' => 33,
                    'unit' => '%'
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['search_field']   => 'width: {{SIZE}}%;'
                ),
            )
        );

        $this->add_responsive_control(
            'sfield_padding',
            array(
                'label' => esc_html__( 'Search Field Padding', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 100,
                    ),
                ),
                'unit' => 'px',
                'default' => [
                    'size' => 15,
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['search_field']   => 'padding-left: {{SIZE}}{{UNIT}};padding-right: {{SIZE}}{{UNIT}};'
                ),
            )
        );

        $this->add_responsive_control(
            'sfield_margin',
            array(
                'label' => esc_html__( 'Search Field Margin', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 100,
                    ),
                ),
                'default' => [
                    'size' => 15,
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['search_field']   => 'margin-bottom: {{SIZE}}{{UNIT}};'
                ),
            )
        );

        $this->add_responsive_control(
            'sfield_button_width',
            array(
                'label' => esc_html__( 'Search Button Width', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    '%' => array(
                        'min' => 20,
                        'max' => 100,
                    )
                ),
                'size_units' => ['%'],
                'default' => [
                    'size' => 33,
                    'unit' => '%'
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['search_button_wrap']   => 'width: {{SIZE}}%;'
                )
            )
        );

        $this->add_responsive_control(
            'sfield_input_height',
            array(
                'label' => esc_html__( 'Input Field Height', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    'px' => array(
                        'min' => 20,
                        'max' => 100,
                    )
                ),
                'size_units' => ['px'],
                'default' => [
                    'size' => 80,
                    'unit' => 'px'
                ],
                'selectors' => array(
                    '{{WRAPPER}} .job_filters input[type="text"]'   => 'height: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .job_filters select' => 'height: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .job_filters .lastudio-search-submit .search-submit' => 'height: {{SIZE}}{{UNIT}};'
                )
            )
        );

        $this->end_controls_section();


        /**
         * General Style Section
         */

        $this->start_controls_section(
            'section_listing_general_style',
            array(
                'label'      => esc_html__( 'General', 'lastudio-wpjm' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
            )
        );

        $this->add_responsive_control(
            'item_margin',
            array(
                'label' => esc_html__( 'Items Margin', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 50,
                    ),
                ),
                'default' => [
                    'size' => 10,
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['item']   => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'item_padding',
            array(
                'label' => esc_html__( 'Items Padding', 'lastudio-wpjm' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 50,
                    ),
                ),
                'default' => [
                    'size' => 10,
                ],
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['item']     => 'padding: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} ' . $css_scheme['wrap']     => 'margin-left: -{{SIZE}}{{UNIT}};margin-right: -{{SIZE}}{{UNIT}};',
                ),
            )
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_content_style',
            array(
                'label'      => esc_html__( 'Content', 'lastudio-wpjm' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
            )
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            array(
                'name'     => 'content_background',
                'selector' => '{{WRAPPER}} ' . $css_scheme['content'],
            )
        );

        $this->add_responsive_control(
            'content_padding',
            array(
                'label'      => __( 'Padding', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['content'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            array(
                'name'     => 'content_box_shadow',
                'selector' => '{{WRAPPER}} ' . $css_scheme['content'],
            )
        );

        $this->end_controls_section();

        /**
         * Title Style Section
         */
        $this->start_controls_section(
            'section_title_style',
            array(
                'label'      => esc_html__( 'Title', 'lastudio-wpjm' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
            )
        );

        $this->add_control(
            'title_color',
            array(
                'label'  => esc_html__( 'Color', 'lastudio-wpjm' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => array(
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ),
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['title'] => 'color: {{VALUE}}',
                ),
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name'     => 'title_typography',
                'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} ' . $css_scheme['title'],
            )
        );

        $this->add_responsive_control(
            'title_padding',
            array(
                'label'      => __( 'Padding', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['title'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'title_margin',
            array(
                'label'      => __( 'Margin', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['title'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'title_text_alignment',
            array(
                'label'   => esc_html__( 'Text Alignment', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::CHOOSE,
                'default' => '',
                'options' => array(
                    'left'    => array(
                        'title' => esc_html__( 'Left', 'lastudio-wpjm' ),
                        'icon'  => 'fa fa-align-left',
                    ),
                    'center' => array(
                        'title' => esc_html__( 'Center', 'lastudio-wpjm' ),
                        'icon'  => 'fa fa-align-center',
                    ),
                    'right' => array(
                        'title' => esc_html__( 'Right', 'lastudio-wpjm' ),
                        'icon'  => 'fa fa-align-right',
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['title'] => 'text-align: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_section();

        /**
         * Price Style Section
         */
        $this->start_controls_section(
            'section_price_style',
            array(
                'label'      => esc_html__( 'Price', 'lastudio-wpjm' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
            )
        );

        $this->add_control(
            'price_color',
            array(
                'label'  => esc_html__( 'Color', 'lastudio-wpjm' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => array(
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ),
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['price'] => 'color: {{VALUE}}',
                ),
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name'     => 'price_typography',
                'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} ' . $css_scheme['price'],
            )
        );

        $this->add_responsive_control(
            'price_padding',
            array(
                'label'      => __( 'Padding', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['price'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'price_margin',
            array(
                'label'      => __( 'Margin', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['price'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'price_text_alignment',
            array(
                'label'   => esc_html__( 'Text Alignment', 'lastudio-wpjm' ),
                'type'    => Controls_Manager::CHOOSE,
                'default' => '',
                'options' => array(
                    'left'    => array(
                        'title' => esc_html__( 'Left', 'lastudio-wpjm' ),
                        'icon'  => 'fa fa-align-left',
                    ),
                    'center' => array(
                        'title' => esc_html__( 'Center', 'lastudio-wpjm' ),
                        'icon'  => 'fa fa-align-center',
                    ),
                    'right' => array(
                        'title' => esc_html__( 'Right', 'lastudio-wpjm' ),
                        'icon'  => 'fa fa-align-right',
                    ),
                ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['price'] => 'text-align: {{VALUE}};',
                ),
            )
        );

        $this->end_controls_section();

        /**
         * Meta Style Section
         */
        $this->start_controls_section(
            'section_meta_style',
            array(
                'label'      => esc_html__( 'Meta', 'lastudio-wpjm' ),
                'tab'        => Controls_Manager::TAB_STYLE,
                'show_label' => false,
            )
        );

        $this->add_control(
            'meta_color',
            array(
                'label'  => esc_html__( 'Color', 'lastudio-wpjm' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => array(
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ),
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['meta'] => 'color: {{VALUE}}',
                ),
            )
        );

        $this->add_control(
            'meta_icon_color',
            array(
                'label'  => esc_html__( 'Icon Color', 'lastudio-wpjm' ),
                'type'   => Controls_Manager::COLOR,
                'scheme' => array(
                    'type'  => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_2,
                ),
                'selectors' => array(
                    '{{WRAPPER}} ' . $css_scheme['meta_icon'] => 'color: {{VALUE}}',
                ),
            )
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name'     => 'meta_typography',
                'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} ' . $css_scheme['meta'],
            )
        );

        $this->add_responsive_control(
            'meta_padding',
            array(
                'label'      => __( 'Padding', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['meta'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );

        $this->add_responsive_control(
            'meta_margin',
            array(
                'label'      => __( 'Margin', 'lastudio-wpjm' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'  => array(
                    '{{WRAPPER}} ' . $css_scheme['meta'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ),
            )
        );


        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        $settings['la_carousel_settings'] = $this->generate_carousel_setting_json();
        echo lastudio_wpjm_job_shortcode_outputs( $settings );
    }

    protected function generate_carousel_setting_json(){
        $settings = $this->get_settings();

        $json_data = '';

        if ( 'yes' !== $settings['carousel_enabled'] ) {
            return $json_data;
        }

        $is_rtl = is_rtl();

        $desktop_col = absint( $settings['columns'] );
        $laptop_col = absint( $settings['columns_laptop'] );
        $tablet_col = absint( $settings['columns_tablet'] );
        $tabletportrait_col = absint( isset($settings['columns_tabletportrait']) ? $settings['columns_tabletportrait'] : $settings['columns_tablet'] );
        $mobile_col = absint( $settings['columns_mobile'] );

        if($laptop_col == 0){
            $laptop_col = $desktop_col;
        }
        if($tablet_col == 0){
            $tablet_col = $laptop_col;
        }
        if($tabletportrait_col == 0){
            $tabletportrait_col = $tablet_col;
        }
        if($mobile_col == 0){
            $mobile_col = 1;
        }

        $slidesToShow = array(
            'desktop'           => $desktop_col,
            'laptop'            => $laptop_col,
            'tablet'            => $tablet_col,
            'tabletportrait'    => $tabletportrait_col,
            'mobile'            => $mobile_col
        );

        $options = array(
            'slidesToShow'   => $slidesToShow,
            'autoplaySpeed'  => absint( $settings['autoplay_speed'] ),
            'autoplay'       => filter_var( $settings['autoplay'], FILTER_VALIDATE_BOOLEAN ),
            'infinite'       => filter_var( $settings['infinite'], FILTER_VALIDATE_BOOLEAN ),
            'pauseOnHover'   => filter_var( $settings['pause_on_hover'], FILTER_VALIDATE_BOOLEAN ),
            'speed'          => absint( $settings['speed'] ),
            'arrows'         => filter_var( $settings['arrows'], FILTER_VALIDATE_BOOLEAN ),
            'dots'           => filter_var( $settings['dots'], FILTER_VALIDATE_BOOLEAN ),
            'slidesToScroll' => absint( $settings['slides_to_scroll'] ),
            'prevArrow'      => lastudio_elements_tools()->get_carousel_arrow(
                array( $settings['prev_arrow'], 'prev-arrow' )
            ),
            'nextArrow'      => lastudio_elements_tools()->get_carousel_arrow(
                array( $settings['next_arrow'], 'next-arrow' )
            ),
            'rtl' => $is_rtl,
        );

        if ( 1 === absint( $settings['columns'] ) ) {
            $options['fade'] = ( 'fade' === $settings['effect'] );
        }

        $json_data = htmlspecialchars( json_encode( $options ) );

        return $json_data;

    }
}

Plugin::instance()->widgets_manager->register_widget_type( new LaStudio_WPJM_Widgets_Listing() );