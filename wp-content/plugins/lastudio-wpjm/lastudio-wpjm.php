<?php
/**
 * Plugin Name: LaStudio Job Advanced
 * Plugin URI:  https://la-studioweb.com
 * Description: This plugin use only for LA-Studio theme with WP Job Manager
 * Version:     1.0.0
 * Author:      LaStudio
 * Author URI:  https://la-studioweb.com
 * Text Domain: lastudio-wpjm
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /languages
 *
 * @package lastudio-wpjm
 * @author  LaStudio
 * @license GPL-2.0+
 * @copyright  2019, LaStudio
 */
if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists("LaStudioWPJM") ) {
	
	final class LaStudioWPJM {

		private static $instance;

		public static function getInstance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof LaStudioWPJM ) ) {
				self::$instance = new LaStudioWPJM;
				self::$instance->setup_constants();

				add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );

				add_action( 'elementor/widgets/widgets_registered', array( self::$instance, 'widgets_registered' ) );

				self::$instance->includes();
			}

			return self::$instance;
		}

		/**
		 *
		 */
		public function setup_constants(){
			
			// Plugin Folder Path
			if ( ! defined( 'LASTUDIO_WPJM_PLUGIN_DIR' ) ) {
				define( 'LASTUDIO_WPJM_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin Folder URL
			if ( ! defined( 'LASTUDIO_WPJM_PLUGIN_URL' ) ) {
				define( 'LASTUDIO_WPJM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin Root File
			if ( ! defined( 'LASTUDIO_WPJM_PLUGIN_FILE' ) ) {
				define( 'LASTUDIO_WPJM_PLUGIN_FILE', __FILE__ );
			}
		}

		public function includes() {
			require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/mixes-functions.php';
			require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/class-template-loader.php';
            require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/post-types/class-posttype-job-listing.php';
            require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/taxonomies/class-taxonomy-job-manager-categories.php';
            require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/taxonomies/class-taxonomy-job-manager-regions.php';
            require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/taxonomies/class-taxonomy-job-manager-types.php';

			add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
            add_action( 'script_loader_src', array( $this, 'override_wpjm_ajax_filters' ), 10, 2 );
		}

		public function widgets_registered(){
            require_once LASTUDIO_WPJM_PLUGIN_DIR . 'includes/elementor/widgets/listing.php';
        }

        public function override_wpjm_ajax_filters( $src, $handler ){
            if($handler == 'wp-job-manager-ajax-filters'){
                return get_theme_file_uri('assets/js/lib/wpjm-ajax-filters.min.js');
            }
            return $src;
        }

		public function admin_scripts(){
            wp_register_script( 'lastudio-wpjm-admin-scripts', LASTUDIO_WPJM_PLUGIN_URL . 'assets/admin.js', array( 'jquery' ), '', true );
            wp_register_style( 'lastudio-wpjm-admin-css', LASTUDIO_WPJM_PLUGIN_URL . 'assets/admin.css');
        }

		public function scripts() {
			wp_enqueue_style('lastudio-wpjm-style', get_theme_file_uri ('/assets/css/listing.css' ));
		}

		/**
		 *
		 */
		public function load_textdomain() {
            load_plugin_textdomain( 'lastudio-wpjm', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}
	}
}

function LaStudioWPJM() {
	return LaStudioWPJM::getInstance();
}

LaStudioWPJM();
