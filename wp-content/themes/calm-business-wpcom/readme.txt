

== Changelog ==

= 21 June 2019 =
* Calm Business 1.6: various style

= 7 June 2019 =
* Heading Size Adjustment
* Fix the overflowing Contact Form input fields
* Update screenshot to include the logo

= 5 June 2019 =
* Calm Business 1.5: editor font size mismatch

= 4 June 2019 =
* Calm Business 1.4: style

= 29 May 2019 =
* page templates project

= 24 May 2019 =
* Calm Business 1.3: Content structure

= 20 May 2019 =
* v1.2 style

= 9 May 2019 =
* Various

= 23 April 2019 =
* Fix `.alignwide` Columns Block padding issues

= 22 March 2019 =
* Remove duplicated code comments
* Add Header block styling
* Fix for post meta stacking on mobile

= 14 February 2019 =
* Update quote block border styles to work better with the new styles planned for Gutenberg 5.2.

= 1 February 2019 =
* Centre page title when no posts are found, and add a fallback for the Media & Text block for IE11.
* Add build tools to theme.
* Adding Glotpress project for the theme.

= 31 January 2019 =
* renamed from calm2019 to calm-business.
