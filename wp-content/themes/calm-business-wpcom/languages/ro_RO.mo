��          <      \       p      q      �     �   K  �            z                     Display Site Title Poppins font: on or offon Your classy establishment needs an equally classy website to showcase your stylish rooms and quality products! With its bold typography and peaceful color scheme, Calm Business exudes a calm, inviting atmosphere as a bed and breakfast, time share, or brick & mortar store fronts. PO-Revision-Date: 2019-02-03 09:31:42+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: ro
Project-Id-Version: WordPress.com - Themes - Calm-business
 Afișează titlu sit on Stabilimentul tău elegant are nevoie de un site la fel de rafinat pentru a-ți prezenta camerele elegante și produsele de calitate! Cu tipografice aldine și o schemă de culori liniștitoare, Calm Business emană o atmosferă calmă și primitoare, cum ar fi un pat confortabil și un mic dejun copios, o proprietate comună sau magazine de vânzare cu amănuntul sau online. 