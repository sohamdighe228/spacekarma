(function ($) {
    "use strict";

    var LaStudio = window.LaStudio || {};

    LaStudio.component = window.LaStudio.component || {};

    var $document = $(document),
        $window = $(window),
        $body = $('body');

    // Initialize global variable

    var HeaderBuilder = {

        init: function(){

            var $header_builder = $('#lastudio-header-builder');

            // Navigation Current Menu
            $('.menu li.current-menu-item, .menu li.current_page_item, #side-nav li.current_page_item, .menu li.current-menu-ancestor, .menu li ul li ul li.current-menu-item , .hamburger-nav li.current-menu-item, .hamburger-nav li.current_page_item, .hamburger-nav li.current-menu-ancestor, .hamburger-nav li ul li ul li.current-menu-item, .full-menu li.current-menu-item, .full-menu li.current_page_item, .full-menu li.current-menu-ancestor, .full-menu li ul li ul li.current-menu-item ').addClass('current');
            $('.menu li ul li:has(ul)').addClass('submenu');


            // Social modal
            var header_social = $('.header-social-modal-wrap').html();
            $('.header-social-modal-wrap').remove();
            $('.main-slide-toggle').append(header_social);

            // Search modal Type 2
            var header_search_type2 = $('.header-search-modal-wrap').html();
            $('.header-search-modal-wrap').remove();
            $('.main-slide-toggle').append(header_search_type2);

            // Search Full
            var $header_search_typefull = $('.header-search-full-wrap').first();
            if($header_search_typefull.length){
                $('.searchform-fly > p').replaceWith($header_search_typefull.find('.searchform-fly-text'));
                $('.searchform-fly > .search-form').replaceWith($header_search_typefull.find('.search-form'));
                $('.header-search-full-wrap').remove();
                LaStudio.core.InstanceSearch();
            }

            // Moving Hamburger
            $('.la-hamburger-wrap').each(function () {
                $(this).appendTo($body);
            });

            // Social dropdown
            $('.lahb-social .js-social_trigger_dropdown').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.header-social-dropdown-wrap').fadeToggle('fast');
            });
            $('.header-social-dropdown-wrap a').on('click', function (e) {
                $('.header-social-dropdown-wrap').css({
                    display: 'none'
                });
            });

            // Social Toggles
            $('.lahb-social .js-social_trigger_slide').on('click', function (e) {
                e.preventDefault();
                if( $header_builder.find('.la-header-social').hasClass('opened') ) {
                    $header_builder.find('.main-slide-toggle').slideUp('opened');
                    $header_builder.find('.la-header-social').removeClass('opened');
                }
                else{
                    $header_builder.find('.main-slide-toggle').slideDown(240);
                    $header_builder.find('#header-search-modal').slideUp(240);
                    $header_builder.find('#header-social-modal').slideDown(240);
                    $header_builder.find('.la-header-social').addClass('opened');
                    $header_builder.find('.la-header-search').removeClass('opened');
                }
            });

            $document.on('click', function (e) {
                if( $(e.target).hasClass('js-social_trigger_slide')){
                    return;
                }
                if ($header_builder.find('.la-header-social').hasClass('opened')) {
                    $header_builder.find('.main-slide-toggle').slideUp('opened');
                    $header_builder.find('.la-header-social').removeClass('opened');
                }
            });

            // Search full

            $('.lahb-cart > a').on('click', function (e) {
                if(!$(this).closest('.lahb-cart').hasClass('force-display-on-mobile')){
                    if($window.width() > 767){
                        e.preventDefault();
                        $('body').toggleClass('open-cart-aside');
                    }
                }
                else{
                    e.preventDefault();
                    $('body').toggleClass('open-cart-aside');
                }
            });

            $('.lahb-search.lahb-header-full > a').on('click', function (e) {
                e.preventDefault()
                $('body').addClass('open-search-form');
                setTimeout(function(){
                    $('.searchform-fly .search-field').focus();
                }, 600);
            });

            // Search Toggles
            $('.lahb-search .js-search_trigger_slide').on('click', function (e) {

                if ($header_builder.find('.la-header-search').hasClass('opened')) {
                    $header_builder.find('.main-slide-toggle').slideUp('opened');
                    $header_builder.find('.la-header-search').removeClass('opened');
                }
                else {
                    $header_builder.find('.main-slide-toggle').slideDown(240);
                    $header_builder.find('#header-social-modal').slideUp(240);
                    $header_builder.find('#header-search-modal').slideDown(240);
                    $header_builder.find('.la-header-search').addClass('opened');
                    $header_builder.find('.la-header-social').removeClass('opened');
                    $header_builder.find('#header-search-modal .search-field').focus();
                }
            });

            $document.on('click', function (e) {
                if( $(e.target).hasClass('js-search_trigger_slide') || $(e.target).closest('.js-search_trigger_slide').length ) {
                    return;
                }
                if($('.lahb-search .js-search_trigger_slide').length){
                    if ($header_builder.find('.la-header-search').hasClass('opened')) {
                        $header_builder.find('.main-slide-toggle').slideUp('opened');
                        $header_builder.find('.la-header-search').removeClass('opened');
                    }
                }
            });


            if ($.fn.niceSelect) {
                $('.la-polylang-switcher-dropdown select').niceSelect();
            }

            if ($.fn.superfish) {
                $('.lahb-area:not(.lahb-vertical) .lahb-nav-wrap:not(.has-megamenu) ul.menu').superfish({
                    delay: 300,
                    hoverClass: 'la-menu-hover',
                    animation: {
                        opacity: "show",
                        height: 'show'
                    },
                    animationOut: {
                        opacity: "hide",
                        height: 'hide'
                    },
                    easing: 'easeOutQuint',
                    speed: 100,
                    speedOut: 0,
                    pathLevels: 2
                });
            }

            $('.lahb-nav-wrap .menu li a').addClass('hcolorf');

            // Hamburger Menu
            var $hamurgerMenuWrapClone = $('.hamburger-type-toggle').find('.hamburger-menu-wrap');
            if ($hamurgerMenuWrapClone.length > 0) {
                $hamurgerMenuWrapClone.appendTo('body');
                $('.hamburger-type-toggle .la-hamuburger-bg').remove();
            }

            if ($('.hamburger-menu-wrap').hasClass('toggle-right')) {
                $('body').addClass('lahb-body lahmb-right');
            }
            else if ($('.hamburger-menu-wrap').hasClass('toggle-left')) {
                $('body').addClass('lahb-body lahmb-left');
            }

            if ($.fn.niceScroll) {
                //Hamburger Nicescroll
                $('.hamburger-menu-main').niceScroll({
                    scrollbarid: 'lahb-hamburger-scroll',
                    cursorwidth: "5px",
                    autohidemode: true
                });
            }

            $('.btn-close-hamburger-menu').on('click', function (e) {
                e.preventDefault();
                $body.removeClass('is-open');
                $('.lahb-hamburger-menu').removeClass('is-open');
                $('.hamburger-menu-wrap').removeClass('hm-open');
                if($.fn.getNiceScroll){
                    $('.hamburger-menu-main').getNiceScroll().resize();
                }
            });

            $('.hamburger-type-toggle a.lahb-icon-element').on('click', function (e) {
                e.preventDefault();
                var $that = $(this),
                    $_parent = $that.closest('.lahb-hamburger-menu'),
                    _cpt_id = $that.attr('data-id');

                if($_parent.hasClass('is-open')){
                    $('.btn-close-hamburger-menu').trigger('click');
                }
                else{
                    $_parent.addClass('is-open');
                    $body.addClass('is-open');
                    $body.find('.hamburger-menu-wrap.hamburger-menu-wrap-' + _cpt_id).addClass('hm-open');
                    if($.fn.getNiceScroll){
                        $('.hamburger-menu-main').getNiceScroll().resize();
                    }
                }

            });



            $('.hamburger-nav.toggle-menu').find('li').each(function () {
                var $list_item = $(this);

                if ($list_item.children('ul').length) {
                    $list_item.children('a').append('<i class="hamburger-nav-icon lastudioicon-down-arrow"></i>');
                }

                $('> a > .hamburger-nav-icon', $list_item).on('click', function (e) {
                    e.preventDefault();
                    var $that = $(this);
                    if( $that.hasClass('active') ){
                        $that.removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                        $('>ul', $list_item).stop().slideUp();
                    }
                    else{
                        $that.removeClass('lastudioicon-down-arrow').addClass('lastudioicon-up-arrow active');
                        $('>ul', $list_item).stop().slideDown(350, function () {
                            if($.fn.getNiceScroll){
                                $('.hamburger-menu-main').getNiceScroll().resize();
                            }
                        });
                    }
                })
            });

            //Full hamburger Menu
            $('.hamburger-type-full .js-hamburger_trigger').on('click', function (e) {
                e.preventDefault();
                var $that = $(this);
                if( $that.hasClass('open-button') ){
                    $('.la-hamburger-wrap-' + $that.data('id')).removeClass('open-menu');
                    $that.removeClass('open-button').addClass('close-button');
                    $('body').removeClass('opem-lahb-iconmenu');
                }
                else{
                    $('.la-hamburger-wrap-' + $that.data('id')).addClass('open-menu');
                    $that.removeClass('close-button').addClass('open-button');
                    $('body').addClass('opem-lahb-iconmenu');
                }
            });

            $('.btn-close-hamburger-menu-full').on('click', function (e) {
                e.preventDefault();
                $('.js-hamburger_trigger').removeClass('open-button').addClass('close-button');
                $('.la-hamburger-wrap').removeClass('open-menu');
                $('body').removeClass('opem-lahb-iconmenu');
            });

            $('.full-menu li > ul').each(function () {
                var $ul = $(this);
                $ul.prev('a').append('<i class="hamburger-nav-icon lastudioicon-down-arrow"></i>');
            });

            $('.full-menu').on('click', 'li .hamburger-nav-icon', function (e) {
                e.preventDefault();
                var $that = $(this),
                    $li_parent = $that.closest('li');

                if ($li_parent.hasClass('open')) {
                    $that.removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                    $li_parent.removeClass('open');
                    $li_parent.find('li').removeClass('open');
                    $li_parent.find('ul').stop().slideUp();
                    $li_parent.find('.hamburger-nav-icon').removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                }
                else {
                    $li_parent.addClass('open');
                    $that.removeClass('lastudioicon-down-arrow').addClass('active lastudioicon-up-arrow');
                    $li_parent.find('>ul').stop().slideDown();
                    $li_parent.siblings().removeClass('open').find('ul').stop().slideUp();
                    $li_parent.siblings().find('li').removeClass('open');
                    $li_parent.siblings().find('.hamburger-nav-icon').removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                }
            });

            $('.touchevents .full-menu').on('touchend', 'li .hamburger-nav-icon', function (e) {
                e.preventDefault();
                var $that = $(this),
                    $li_parent = $that.closest('li');

                if ($li_parent.hasClass('open')) {
                    $that.removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                    $li_parent.removeClass('open');
                    $li_parent.find('li').removeClass('open');
                    $li_parent.find('ul').stop().slideUp();
                    $li_parent.find('.hamburger-nav-icon').removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                }
                else {
                    $li_parent.addClass('open');
                    $that.removeClass('lastudioicon-down-arrow').addClass('active lastudioicon-up-arrow');
                    $li_parent.find('>ul').stop().slideDown();
                    $li_parent.siblings().removeClass('open').find('ul').stop().slideUp();
                    $li_parent.siblings().find('li').removeClass('open');
                    $li_parent.siblings().find('.hamburger-nav-icon').removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                }
            });

            // Toggle search form
            $('.lahb-search .js-search_trigger_toggle').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.lahb-search-form-box').toggleClass('show-sbox');
            });

            $document.on('click', function (e) {
                if( $(e.target).hasClass('js-search_trigger_toggle') || $(e.target).closest('.js-search_trigger_toggle').length){
                    return;
                }
                if( $('.lahb-search-form-box').length ) {
                    if( $(e.target).closest('.lahb-search-form-box').length == 0){
                        $('.lahb-search-form-box').removeClass('show-sbox');
                    }
                }
            });

            // Responsive Menu
            $('.lahb-responsive-menu-icon-wrap').on('click', function (e) {
                e.preventDefault();
                var $toggleMenuIcon = $(this),
                    uniqid = $toggleMenuIcon.data('uniqid'),
                    $responsiveMenu = $('.lahb-responsive-menu-wrap[data-uniqid="' + uniqid + '"]'),
                    $closeIcon = $responsiveMenu.find('.close-responsive-nav');

                if ($responsiveMenu.hasClass('open') === false) {
                    $toggleMenuIcon.addClass('open-icon-wrap').children().addClass('open');
                    $closeIcon.addClass('open-icon-wrap').children().addClass('open');
                    $responsiveMenu.animate({'left': 0}, 350).addClass('open');
                } else {
                    $toggleMenuIcon.removeClass('open-icon-wrap').children().removeClass('open');
                    $closeIcon.removeClass('open-icon-wrap').children().removeClass('open');
                    $responsiveMenu.animate({'left': -280}, 350).removeClass('open');
                }
            });

            $('.lahb-responsive-menu-wrap').each(function () {
                var $this = $(this),
                    uniqid = $this.data('uniqid'),
                    $responsiveMenu = $this.clone(),
                    $closeIcon = $responsiveMenu.find('.close-responsive-nav'),
                    $toggleMenuIcon = $('.lahb-responsive-menu-icon-wrap[data-uniqid="' + uniqid + '"]');

                // append responsive menu to lastudio header builder wrap
                $this.remove();
                $('#lastudio-header-builder').append($responsiveMenu);

                // add arrow down to parent menus
                $responsiveMenu.find('li').each(function () {
                    var $list_item = $(this);

                    if ($list_item.children('ul').length) {
                        $list_item.children('a').append('<i class="lastudioicon-down-arrow respo-nav-icon"></i>');
                    }

                    $('> a > .respo-nav-icon', $list_item).on('click', function (e) {
                        e.preventDefault();
                        var $that = $(this);
                        if( $that.hasClass('active') ){
                            $that.removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                            $('>ul', $list_item).stop().slideUp(350);
                        }
                        else{
                            $that.removeClass('lastudioicon-down-arrow').addClass('lastudioicon-up-arrow active');
                            $('>ul', $list_item).stop().slideDown(350);
                        }
                    });
                });

                // close responsive menu
                $closeIcon.on('click', function () {
                    if ($toggleMenuIcon.hasClass('open-icon-wrap')) {
                        $toggleMenuIcon.removeClass('open-icon-wrap').children().removeClass('open');
                        $closeIcon.removeClass('open-icon-wrap').children().removeClass('open');
                    }
                    else {
                        $toggleMenuIcon.addClass('open-icon-wrap').children().addClass('open');
                        $closeIcon.addClass('open-icon-wrap').children().addClass('open');
                    }

                    if ($responsiveMenu.hasClass('open') === true) {
                        $responsiveMenu.animate({'left': -280}, 350).removeClass('open');
                    }
                });

                $responsiveMenu.on('click', 'li.menu-item:not(.menu-item-has-children) > a', function (e) {
                    $toggleMenuIcon.removeClass('open-icon-wrap').children().removeClass('open');
                    $closeIcon.removeClass('open-icon-wrap').children().removeClass('open');
                    $responsiveMenu.animate({'left': -280}, 0).removeClass('open');
                });
            });

            // Login Dropdown

            $('.lahb-login .js-login_trigger_dropdown').each(function () {
                var $this = $(this);
                if($this.siblings('.lahb-modal-login').length == 0){
                    $('.lahb-modal-login.la-element-dropdown').first().clone().appendTo($this.parent());
                }
            });

            $('.lahb-login .js-login_trigger_dropdown').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.lahb-modal-login').fadeToggle('fast');
            });


            // Contact Dropdown
            $('.lahb-contact .js-contact_trigger_dropdown').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.la-contact-form').fadeToggle('fast');
            });
            $document.on('click', function (e) {
                if( $(e.target).hasClass('js-contact_trigger_dropdown')){
                    return;
                }
                if( $('.la-contact-form.la-element-dropdown').length ) {
                    if($(e.target).closest('.la-contact-form.la-element-dropdown').length == 0){
                        $('.la-contact-form.la-element-dropdown').css({
                            'display': 'none'
                        })
                    }
                }
            });


            // Icon Menu Dropdown

            $('.lahb-icon-menu .js-icon_menu_trigger').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.lahb-icon-menu-content').fadeToggle('fast');
            });

            $document.on('click', function (e) {
                if( $(e.target).hasClass('js-icon_menu_trigger')){
                    return;
                }
                if( $('.la-element-dropdown.lahb-icon-menu-content').length ) {
                    if($(e.target).closest('.la-element-dropdown.lahb-icon-menu-content').length == 0){
                        $('.la-element-dropdown.lahb-icon-menu-content').css({
                            'display': 'none'
                        })
                    }
                }
            });

            // Wishlist Dropdown
            $('.lahb-wishlist').each(function (index, el) {
                $(this).find('#la-wishlist-icon').on('click', function (event) {
                    $(this).siblings('.la-header-wishlist-wrap').fadeToggle('fast', function () {
                        if ($(".la-header-wishlist-wrap").is(":visible")) {
                            $(document).on('click', function (e) {
                                var target = $(e.target);
                                if (target.parents('.lahb-wishlist').length)
                                    return;
                                $(".la-header-wishlist-wrap").css({
                                    display: 'none'
                                });
                            });
                        }
                    });
                });
            });

            $('.la-header-wishlist-content-wrap').find('.la-wishlist-total').addClass('colorf');


            /* Profile Socials */

            $('.lahb-profile-socials-text')
                .on('mouseenter', function () {
                    $(this).closest('.lahb-profile-socials-wrap').find('.lahb-profile-socials-icons').removeClass('profile-socials-hide').addClass('profile-socials-show');
                })
                .on('mouseleave', function () {
                    $(this).closest('.lahb-profile-socials-wrap').find('.lahb-profile-socials-icons').removeClass('profile-socials-show').addClass('profile-socials-hide');
                });


            /* Vertical Header */

            // Toggle Vertical

            $('.lahb-vertical-toggle-wrap .vertical-toggle-icon').on('click', function (e) {
                e.preventDefault();
                $body.toggleClass('open-lahb-vertical');
            });

            if($.fn.niceScroll){
                // Vertical Nicescroll
                $('.lahb-vertical .lahb-content-wrap').niceScroll({
                    scrollbarid: 'lahb-vertical-menu-scroll',
                    cursorwidth: "5px",
                    autohidemode: true
                });
            }

            // Vertical Menu
            $('.lahb-vertical .lahb-nav-wrap').removeClass('has-megamenu has-parent-arrow');
            $('.lahb-vertical .lahb-nav-wrap li.mega').removeClass('mega');
            $('.lahb-vertical .lahb-nav-wrap li.mm-popup-wide').removeClass('mm-popup-wide');
            $('.lahb-vertical .menu li').each(function () {
                var $list_item = $(this);

                if ($list_item.children('ul').length) {
                    $list_item.children('a').removeClass('sf-with-ul').append('<i class="lastudioicon-down-arrow lahb-vertical-nav-icon"></i>');
                }

                $('> a > .lahb-vertical-nav-icon', $list_item).on('click', function (e) {
                    e.preventDefault();
                    var $that = $(this);
                    if( $that.hasClass('active') ){
                        $that.removeClass('active lastudioicon-up-arrow').addClass('lastudioicon-down-arrow');
                        $('>ul', $list_item).stop().slideUp();
                    }
                    else{
                        $that.removeClass('lastudioicon-down-arrow').addClass('lastudioicon-up-arrow active');
                        $('>ul', $list_item).stop().slideDown(350, function () {
                            if($.fn.getNiceScroll){
                                $('.lahb-vertical .lahb-content-wrap').getNiceScroll().resize();
                            }
                        });
                    }
                });

            });


            $document.on('keyup', function (e) {
                if(e.keyCode == 27){
                    $body.removeClass('is-open open-search-form open-cart-aside');
                    $('.hamburger-menu-wrap').removeClass('hm-open');
                    $('.lahb-hamburger-menu.hamburger-type-toggle').removeClass('is-open');
                    $('.lahb-hamburger-menu.hamburger-type-full .hamburger-op-icon').removeClass('open-button').addClass('close-button');
                    $('.la-hamburger-wrap').removeClass('open-menu');
                }
            });

        },

        reloadAllEvents: function () {
            LaStudio.component.HeaderBuilder.init();
            LaStudio.core.HeaderSticky();
            LaStudio.core.MegaMenu();
            $window.trigger('scroll');
            console.log('ok -- reloadAllEvents!');
        }
    }

    LaStudio.component.HeaderBuilder = HeaderBuilder;

    $(function () {
        LaStudio.component.HeaderBuilder.init();
    });

})(jQuery);