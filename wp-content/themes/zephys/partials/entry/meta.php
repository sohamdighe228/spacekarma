<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
echo '<div class="post-meta">';
echo sprintf(
    '<span class="post__date post-meta__item"%3$s><i class="lastudioicon-clock"></i><time datetime="%1$s" title="%1$s">%2$s</time></span>',
    esc_attr( get_the_date( 'c' ) ),
    esc_html( get_the_date() ),
    zephys_get_schema_markup('publish_date')

);
echo sprintf(
    '<span class="posted-by post-meta__item"%4$s><i class="lastudioicon-circle-10"></i>%1$s<a href="%2$s" class="posted-by__author" rel="author"%5$s>%3$s</a></span>',
    esc_html__( 'by ', 'zephys' ),
    esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
    esc_html( get_the_author() ),
    zephys_get_schema_markup('author_name'),
    zephys_get_schema_markup('author_link')
);
echo '</div>';