<?php
/**
 * Default post entry layout
 *
 * @package Zephys WordPress theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


$preset = zephys_get_option('blog_design', '');
$blog_excerpt_length = zephys_get_option('blog_excerpt_length', 30);

$title_tag = 'h2';

$classes = array('lastudio-posts__item', 'loop__item', 'grid-item');

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
    <div class="lastudio-posts__inner-box"><?php

        zephys_single_post_thumbnail();

        echo '<div class="lastudio-posts__inner-content">';

        if($preset != 'grid-2'){
            zephys_entry_meta_item_category_list('<div class="post-terms post-meta__item">', '</div>', ', ');
        }

        if($preset == 'grid-2'){
            get_template_part('partials/entry/meta');
        }

        echo sprintf(
            '<header class="entry-header"><%1$s class="entry-title"><a href="%2$s" title="%3$s" rel="bookmark">%3$s</a></%1$s></header>',
            esc_attr($title_tag),
            esc_url(get_the_permalink()),
            esc_html(get_the_title())
        );

        if($preset != 'grid-1' && $preset != 'grid-2'){
            get_template_part('partials/entry/meta');
        }

        if($blog_excerpt_length > 0){
            echo sprintf(
                '<div class="entry-excerpt"%2$s>%1$s</div>',
                zephys_excerpt($blog_excerpt_length),
                zephys_get_schema_markup('entry_content')
            );
        }

        echo sprintf(
            '<div class="lastudio-more-wrap"><a href="%2$s" class="elementor-button lastudio-more" title="%3$s" rel="bookmark"><span class="btn__text">%1$s</span></a></div>',
            esc_html__('Read More', 'zephys'),
            esc_url(get_the_permalink()),
            esc_html(get_the_title())
        );

        echo '</div>';

        ?></div>
</article><!-- #post-## -->