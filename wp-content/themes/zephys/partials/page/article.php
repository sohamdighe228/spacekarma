<?php
/**
 * Outputs page article
 *
 * @package Zephys WordPress theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<div class="entry"<?php zephys_schema_markup( 'entry_content' ); ?>>

    <?php do_action( 'zephys/action/before_page_entry' ); ?>

	<?php the_content();

	wp_link_pages( array(
		'before' => '<div class="clearfix"></div><div class="page-links">' . __( 'Pages:', 'zephys' ),
		'after'  => '</div>',
	) );
	?>

    <?php

    // Display comments
    if ( comments_open() || get_comments_number() ) {
        comments_template();
    }

    ?>

    <?php do_action( 'zephys/action/after_page_entry' ); ?>

</div>