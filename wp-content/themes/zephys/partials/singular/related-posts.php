<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$enable_related = zephys_get_option('blog_related_posts', 'off');
$related_style  = zephys_get_option('blog_related_design', 1);
$max_related    = zephys_get_option('blog_related_max_post', 1);
$related_by     = zephys_get_option('blog_related_by', 'random');

if(!zephys_string_to_bool($enable_related)){
    return;
}

wp_reset_postdata();


$related_args = array(
    'posts_per_page' => $max_related,
    'post__not_in' => array(get_the_ID())
);
if ($related_by == 'random') {
    $related_args['orderby'] = 'rand';
}
if ($related_by == 'category') {
    $cats = wp_get_post_terms(get_the_ID(), 'category');
    if (is_array($cats) && isset($cats[0]) && is_object($cats[0])) {
        $related_args['category__in'] = array($cats[0]->term_id);
    }
}
if ($related_by == 'tag') {
    $tags = wp_get_post_terms(get_the_ID(), 'tag');
    if (is_array($tags) && isset($tags[0]) && is_object($tags[0])) {
        $related_args['tag__in'] = array($tags[0]->term_id);
    }
}
if ($related_by == 'both') {
    $cats = wp_get_post_terms(get_the_ID(), 'category');
    if (is_array($cats) && isset($cats[0]) && is_object($cats[0])) {
        $related_args['category__in'] = array($cats[0]->term_id);
    }
    $tags = wp_get_post_terms(get_the_ID(), 'tag');
    if (is_array($tags) && isset($tags[0]) && is_object($tags[0])) {
        $related_args['tag__in'] = array($tags[0]->term_id);
    }
}


$related_query = new WP_Query($related_args);

$loop_classes = array('la-loop', 'lastudio-posts');
if($related_style == 1){
    $loop_classes = 'lastudio-posts layout-type-grid preset-grid-7 lastudio-posts--grid';
    $slidesToShow = array(
        'desktop'           => 3,
        'laptop'            => 3,
        'tablet'            => 2,
        'tabletportrait'    => 2,
        'mobile'            => 1,
        'mobileportrait'    => 1
    );
    $loop_list_classes = 'active-object-fit lastudio-posts__list grid-items block-grid-3 laptop-block-grid-3 tablet-block-grid-2 mobile-block-grid-1 xmobile-block-grid-1 js-el la-slick-slider lastudio-carousel';
}
else{
    $loop_classes = 'lastudio-posts layout-type-list preset-list-5 lastudio-posts--list';
    $slidesToShow = array(
        'desktop'           => 1,
        'laptop'            => 1,
        'tablet'            => 1,
        'tabletportrait'    => 1,
        'mobile'            => 1,
        'mobileportrait'    => 1
    );
    $loop_list_classes = 'lastudio-posts__list grid-items block-grid-1 js-el la-slick-slider lastudio-carousel';
}

$slide_config = array(
    'slidesToShow'   => $slidesToShow,
    'adaptiveHeight' => true,
    'prevArrow'=> '<button type="button" class="slick-prev"><i class="lastudioicon-left-arrow"></i></button>',
    'nextArrow'=> '<button type="button" class="slick-next"><i class="lastudioicon-right-arrow"></i></button>',
    'rtl' => is_rtl()
);

$title_tag = 'h4';

if($related_query->have_posts()){
    ?>
    <section class="section-related-posts related-posts-design-<?php echo esc_attr($related_style); ?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php if($related_style == 1): ?>
                    <h3 class="related-posts-heading theme-heading"><?php esc_html_e('Related Post', 'zephys'); ?></h3>
                    <?php endif; ?>
                    <div class="<?php echo esc_attr($loop_classes); ?>">
                        <div class="lastudio-posts__list_wrapper">
                            <div class="<?php echo esc_attr($loop_list_classes) ?>" data-slider_config="<?php echo esc_attr(json_encode($slide_config)); ?>" data-la_component="AutoCarousel">
                                <?php
                                while ($related_query->have_posts()){
                                    $related_query->the_post();
                                    ?>

                                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'lastudio-posts__item loop__item grid-item' ); ?>>
                                        <div class="lastudio-posts__inner-box">
                                            <?php

                                            if(has_post_thumbnail()){
                                                ?>
                                                <div class="post-thumbnail">
                                                    <a href="<?php the_permalink() ?>">
                                                        <?php if( $related_style == 1 ) { ?>
                                                        <figure class="blog_item--thumbnail figure__object_fit">
                                                            <?php the_post_thumbnail(); ?>
                                                        </figure>

                                                    <?php
                                                        }
                                                        else{ ?>
                                                            <div class="blog_item--thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>')"></div>
                                                        <?php }
                                                        ?>
                                                        <span class="pf-icon pf-icon-standard"></span>
                                                    </a>
                                                </div>
                                                <?php
                                            }

                                            echo '<div class="lastudio-posts__inner-content">';

                                            if($related_style == 1){
                                                zephys_entry_meta_item_category_list('<div class="post-terms post-meta__item">', '</div>', ', ');
                                            }
                                            else{
                                                echo sprintf('<div class="related-posts-heading">%s</div>', esc_html__('You May Also Like', 'zephys'));
                                            }


                                            echo sprintf(
                                                '<header class="entry-header"><%1$s class="entry-title"><a href="%2$s" title="%3$s" rel="bookmark">%3$s</a></%1$s></header>',
                                                esc_attr($title_tag),
                                                esc_url(get_the_permalink()),
                                                esc_html(get_the_title())
                                            );

                                            get_template_part('partials/entry/meta');

                                            echo sprintf(
                                                '<div class="lastudio-more-wrap"><a href="%2$s" class="elementor-button lastudio-more" title="%3$s" rel="bookmark"><span class="btn__text">%1$s</span></a></div>',
                                                esc_html__('Read More', 'zephys'),
                                                esc_url(get_the_permalink()),
                                                esc_html(get_the_title())
                                            );

                                            echo '</div>';

                                            ?></div>
                                    </article><!-- #post-## -->

                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}

wp_reset_postdata();