<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$show_page_title = apply_filters('zephys/filter/show_page_title', true);

$blog_post_title = zephys_get_option('blog_post_title', 'below');
$blog_pn_nav = zephys_get_option('blog_pn_nav', 'off');
$featured_images_single = zephys_get_option('featured_images_single', 'off');
$blog_social_sharing_box = zephys_get_option('blog_social_sharing_box', 'off');
$blog_comments = zephys_get_option('blog_comments', 'off');
$single_post_thumbnail_size = zephys_get_option('single_post_thumbnail_size', 'full');

$page_header_layout = zephys_get_page_header_layout();

$title_tag = 'h1';
if($show_page_title){
    $title_tag = 'h2';
}
if($page_header_layout == 'hide'){
    $title_tag = 'h1';
}

?>
<article class="single-post-article">

    <?php
    if($blog_post_title == 'above'){
        zephys_entry_meta_item_category_list('<div class="post-terms post-meta__item">', '</div>', ', ');
        the_title( '<header class="entry-header"><'.$title_tag.' class="entry-title entry-title-single" itemprop="headline">', '</'.$title_tag.'></header>' );
        get_template_part('partials/entry/meta');
    }

    if($featured_images_single){
        zephys_single_post_thumbnail($single_post_thumbnail_size);
    }

    if($blog_post_title == 'below'){
        zephys_entry_meta_item_category_list('<div class="post-terms post-meta__item">', '</div>', ',');
        the_title( '<header class="entry-header"><'.$title_tag.' class="entry-title entry-title-single" itemprop="headline">', '</'.$title_tag.'></header>' );
        get_template_part('partials/entry/meta');
    }

    ?>

    <div class="entry clearfix"<?php zephys_schema_markup( 'entry_content' ); ?>>

        <?php do_action( 'zephys/action/before_single_entry' ); ?>
        <?php the_content();
        wp_link_pages( array(
            'before' => '<div class="page-links">' . __( 'Pages:', 'zephys' ),
            'after'  => '</div>',
        ) ); ?>
        <?php do_action( 'zephys/action/after_single_entry' ); ?>
    </div>

    <footer class="entry-footer">
        <?php
        the_tags('<div class="tags-list"><span>' . esc_html_x( 'Tags', 'front-view', 'zephys' ) . ' </span><span class="tags-list-item">' ,'','</span></div>');

        if(zephys_string_to_bool($blog_social_sharing_box)){
            echo '<div class="la-sharing-single-posts">';
            echo sprintf('<span class="title-share">%s</span>', esc_html_x('Share with', 'front-view', 'zephys') );
            zephys_social_sharing(get_the_permalink(), get_the_title(), (has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'full') : ''));
            echo '</div>';
        }

        edit_post_link( null, '<span class="edit-link hidden">', '</span>' );
        ?>
    </footer>

    <?php
    if(zephys_string_to_bool($blog_pn_nav)){
        the_post_navigation( array(
            'next_text' => '<span class="blog_pn_nav-text">'. esc_html__('Next Post', 'zephys') .'</span><span class="blog_pn_nav blog_pn_nav-left">%image</span><span class="blog_pn_nav blog_pn_nav-right"><span class="blog_pn_nav-title">%title</span><span class="blog_pn_nav-meta">%date - %author</span></span>',
            'prev_text' => '<span class="blog_pn_nav-text">'. esc_html__('Prev Post', 'zephys') .'</span><span class="blog_pn_nav blog_pn_nav-left">%image</span><span class="blog_pn_nav blog_pn_nav-right"><span class="blog_pn_nav-title">%title</span><span class="blog_pn_nav-meta">%date - %author</span></span>'
        ) );
    }
    ?>

    <?php

    // Display comments
    if ( (comments_open() || get_comments_number()) && zephys_string_to_bool($blog_comments)) {
        comments_template();
    }

    ?>

</article>