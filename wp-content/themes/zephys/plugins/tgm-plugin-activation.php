<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action( 'tgmpa_register', 'zephys_register_required_plugins' );

if(!function_exists('zephys_register_required_plugins')){

	function zephys_register_required_plugins() {

		$plugins = array();

		$plugins[] = array(
			'name'					=> esc_html_x('LA-Studio Core', 'admin-view', 'zephys'),
			'slug'					=> 'lastudio',
			'source'				=> get_template_directory() . '/plugins/lastudio.zip',
			'required'				=> true,
			'version'				=> '2.0.0'
		);

		$plugins[] = array(
			'name'					=> esc_html_x('LA-Studio Header Builder', 'admin-view', 'zephys'),
			'slug'					=> 'lastudio-header-builders',
			'source'				=> get_template_directory() . '/plugins/lastudio-header-builders.zip',
			'required'				=> true,
			'version'				=> '1.1'
		);


        $plugins[] = array(
            'name' 					=> esc_html_x('Elementor', 'admin-view', 'zephys'),
            'slug' 					=> 'elementor',
            'required' 				=> true,
            'version'				=> '2.5.16'
        );

		$plugins[] = array(
			'name'					=> esc_html_x('LaStudio Elements For Elementor', 'admin-view', 'zephys'),
			'slug'					=> 'lastudio-elements',
			'source'				=> get_template_directory() . '/plugins/lastudio-elements.zip',
			'required'				=> true,
			'version'				=> '1.0.2.4'
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('WooCommerce', 'admin-view', 'zephys'),
			'slug'     				=> 'woocommerce',
			'version'				=> '3.6.4',
			'required' 				=> false
		);
        
        $plugins[] = array(
			'name'     				=> esc_html_x('Zephys Package Demo Data', 'admin-view', 'zephys'),
			'slug'					=> 'zephys-demo-data',
			'source'   				=> 'https://github.com/la-studioweb/resource/raw/master/zephys/zephys-demo-data.zip',
			'required' 				=> false,
			'version' 				=> '1.0.1'
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('Envato Market', 'admin-view', 'zephys'),
			'slug'     				=> 'envato-market',
			'source'   				=> 'https://envato.github.io/wp-envato-market/dist/envato-market.zip',
			'required' 				=> false,
			'version' 				=> '2.0.0'
		);

		$plugins[] = array(
			'name' 					=> esc_html_x('Contact Form 7', 'admin-view', 'zephys'),
			'slug' 					=> 'contact-form-7',
			'required' 				=> false
		);

		$plugins[] = array(
			'name'					=> esc_html_x('Slider Revolution', 'admin-view', 'zephys'),
			'slug'					=> 'revslider',
			'source'				=> get_template_directory() . '/plugins/revslider.zip',
			'required'				=> false,
			'version'				=> '5.4.8.3'
		);

        $plugins[] = array(
            'name' 					=> esc_html_x('WP Job Manager', 'admin-view', 'zephys'),
            'slug' 					=> 'wp-job-manager',
            'required' 				=> false
        );

        $plugins[] = array(
            'name' 					=> esc_html_x('Contact Listing for WP Job Manager', 'admin-view', 'zephys'),
            'slug' 					=> 'wp-job-manager-contact-listing',
            'required' 				=> false
        );

        $plugins[] = array(
            'name'					=> esc_html_x('LaStudio Job Advanced', 'admin-view', 'zephys'),
            'slug'					=> 'lastudio-wpjm',
            'source'				=> get_template_directory() . '/plugins/lastudio-wpjm.zip',
            'required'				=> false,
            'version'				=> '1.0.0'
        );

		$config = array(
			'id'           				=> 'zephys',
			'default_path' 				=> '',
			'menu'         				=> 'tgmpa-install-plugins',
			'has_notices'  				=> true,
			'dismissable'  				=> true,
			'dismiss_msg'  				=> '',
			'is_automatic' 				=> false,
			'message'      				=> ''
		);

		tgmpa( $plugins, $config );

	}

}
