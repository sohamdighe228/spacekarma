<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if(!function_exists('zephys_add_icon_to_header_builder')){
    function zephys_add_icon_to_header_builder( $icons ) {
        $from_theme = array("lastudioicon-ic_mail_outline_24px","lastudioicon-ic_compare_arrows_24px","lastudioicon-ic_compare_24px","lastudioicon-ic_share_24px","lastudioicon-bath-tub-1","lastudioicon-shopping-cart-1","lastudioicon-contrast","lastudioicon-heart-1","lastudioicon-sort-tool","lastudioicon-list-bullet-1","lastudioicon-menu-8-1","lastudioicon-menu-4-1","lastudioicon-menu-3-1","lastudioicon-menu-1","lastudioicon-down-arrow","lastudioicon-left-arrow","lastudioicon-right-arrow","lastudioicon-up-arrow","lastudioicon-phone-1","lastudioicon-pin-3-1","lastudioicon-search-content","lastudioicon-single-01-1","lastudioicon-i-delete","lastudioicon-zoom-1","lastudioicon-b-meeting","lastudioicon-bag-20","lastudioicon-bath-tub-2","lastudioicon-web-link","lastudioicon-shopping-cart-2","lastudioicon-cart-return","lastudioicon-check","lastudioicon-g-check","lastudioicon-d-check","lastudioicon-circle-10","lastudioicon-circle-simple-left","lastudioicon-circle-simple-right","lastudioicon-compare","lastudioicon-letter","lastudioicon-mail","lastudioicon-email","lastudioicon-eye","lastudioicon-heart-2","lastudioicon-shopping-cart-3","lastudioicon-list-bullet-2","lastudioicon-marker-3","lastudioicon-measure-17","lastudioicon-menu-8-2","lastudioicon-menu-7","lastudioicon-menu-4-2","lastudioicon-menu-3-2","lastudioicon-menu-2","lastudioicon-microsoft","lastudioicon-phone-2","lastudioicon-phone-call-1","lastudioicon-pin-3-2","lastudioicon-pin-check","lastudioicon-e-remove","lastudioicon-single-01-2","lastudioicon-i-add","lastudioicon-small-triangle-down","lastudioicon-small-triangle-left","lastudioicon-small-triangle-right","lastudioicon-tag-check","lastudioicon-tag","lastudioicon-clock","lastudioicon-time-clock","lastudioicon-triangle-left","lastudioicon-triangle-right","lastudioicon-business-agent","lastudioicon-zoom-2","lastudioicon-zoom-88","lastudioicon-search-zoom-in","lastudioicon-search-zoom-out","lastudioicon-small-triangle-up","lastudioicon-phone-call-2","lastudioicon-full-screen","lastudioicon-car-parking","lastudioicon-transparent","lastudioicon-bedroom-1","lastudioicon-bedroom-2","lastudioicon-search-property","lastudioicon-menu-5","lastudioicon-circle-simple-right-2","lastudioicon-detached-property","lastudioicon-armchair","lastudioicon-measure-big","lastudioicon-b-meeting-2","lastudioicon-bulb-63","lastudioicon-new-construction","lastudioicon-quite-happy","lastudioicon-shape-star-1","lastudioicon-shape-star-2","lastudioicon-star-rate-1","lastudioicon-star-rate-2","lastudioicon-home-2","lastudioicon-home-3","lastudioicon-home","lastudioicon-home-2-2","lastudioicon-home-3-2","lastudioicon-home-4","lastudioicon-home-search","lastudioicon-e-add","lastudioicon-e-delete","lastudioicon-i-delete-2","lastudioicon-i-add-2");
        return array_merge($from_theme, $icons);
    }
    add_filter('lastudio/header-builder/all-icons', 'zephys_add_icon_to_header_builder');
}

if(!function_exists('zephys_render_socials_for_header_builder')){
    function zephys_render_socials_for_header_builder(){
        $social_links = zephys_get_option('social_links');
        if(!empty($social_links)){
            echo '<div class="social-media-link style-default">';
            foreach($social_links as $item){
                if(!empty($item['link']) && !empty($item['icon'])){
                    $title = isset($item['title']) ? $item['title'] : '';
                    printf(
                        '<a href="%1$s" class="%2$s" title="%3$s" target="_blank" rel="nofollow"><i class="%4$s"></i></a>',
                        esc_url($item['link']),
                        esc_attr(sanitize_title($title)),
                        esc_attr($title),
                        esc_attr($item['icon'])
                    );
                }
            }
            echo '</div>';
        }
    }
    add_action('lastudio/header-builder/render-social', 'zephys_render_socials_for_header_builder');
}