<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_filter('lastudio-elements/assets/css/default-theme-enabled', '__return_false');

if(!function_exists('zephys_add_icon_library_to_elementor')){
    function zephys_add_icon_library_to_elementor( $icons ){
        $from_theme = array("lastudioicon-ic_mail_outline_24px", "lastudioicon-ic_compare_arrows_24px", "lastudioicon-ic_compare_24px", "lastudioicon-ic_share_24px", "lastudioicon-bath-tub-1", "lastudioicon-shopping-cart-1", "lastudioicon-contrast", "lastudioicon-heart-1", "lastudioicon-sort-tool", "lastudioicon-list-bullet-1", "lastudioicon-menu-8-1", "lastudioicon-menu-4-1", "lastudioicon-menu-3-1", "lastudioicon-menu-1", "lastudioicon-down-arrow", "lastudioicon-left-arrow", "lastudioicon-right-arrow", "lastudioicon-up-arrow", "lastudioicon-phone-1", "lastudioicon-pin-3-1", "lastudioicon-search-content", "lastudioicon-single-01-1", "lastudioicon-i-delete", "lastudioicon-zoom-1", "lastudioicon-b-meeting", "lastudioicon-bag-20", "lastudioicon-bath-tub-2", "lastudioicon-web-link", "lastudioicon-shopping-cart-2", "lastudioicon-cart-return", "lastudioicon-check", "lastudioicon-g-check", "lastudioicon-d-check", "lastudioicon-circle-10", "lastudioicon-circle-simple-left", "lastudioicon-circle-simple-right", "lastudioicon-compare", "lastudioicon-letter", "lastudioicon-mail", "lastudioicon-email", "lastudioicon-eye", "lastudioicon-heart-2", "lastudioicon-shopping-cart-3", "lastudioicon-list-bullet-2", "lastudioicon-marker-3", "lastudioicon-measure-17", "lastudioicon-menu-8-2", "lastudioicon-menu-7", "lastudioicon-menu-4-2", "lastudioicon-menu-3-2", "lastudioicon-menu-2", "lastudioicon-microsoft", "lastudioicon-phone-2", "lastudioicon-phone-call-1", "lastudioicon-pin-3-2", "lastudioicon-pin-check", "lastudioicon-e-remove", "lastudioicon-single-01-2", "lastudioicon-i-add", "lastudioicon-small-triangle-down", "lastudioicon-small-triangle-left", "lastudioicon-small-triangle-right", "lastudioicon-tag-check", "lastudioicon-tag", "lastudioicon-clock", "lastudioicon-time-clock", "lastudioicon-triangle-left", "lastudioicon-triangle-right", "lastudioicon-business-agent", "lastudioicon-zoom-2", "lastudioicon-zoom-88", "lastudioicon-search-zoom-in", "lastudioicon-search-zoom-out", "lastudioicon-small-triangle-up", "lastudioicon-phone-call-2", "lastudioicon-full-screen", "lastudioicon-car-parking", "lastudioicon-transparent", "lastudioicon-bedroom-1", "lastudioicon-bedroom-2", "lastudioicon-search-property", "lastudioicon-menu-5", "lastudioicon-circle-simple-right-2", "lastudioicon-detached-property", "lastudioicon-armchair", "lastudioicon-measure-big", "lastudioicon-b-meeting-2", "lastudioicon-bulb-63", "lastudioicon-new-construction", "lastudioicon-quite-happy");
        $keys = array_map(function( $el ){
            return str_replace(array('lastudioicon-', '_'), array('', '-'), $el);
        }, $from_theme);
        $new_icon = array_combine($from_theme, $keys);
        return $icons + $new_icon;
    }
    add_filter('lastudio-elements/controls/icon_lists', 'zephys_add_icon_library_to_elementor');
}

if(!function_exists('zephys_add_portfolio_preset')){
    function zephys_add_portfolio_preset( ){
        return array(
            'type-1' => esc_html__( 'Type 1', 'zephys' ),
            'type-2' => esc_html__( 'Type 2', 'zephys' ),
            'type-3' => esc_html__( 'Type 3', 'zephys' ),
            'type-4' => esc_html__( 'Type 4', 'zephys' ),
            'type-5' => esc_html__( 'Type 5', 'zephys' ),
            'type-6' => esc_html__( 'Type 6', 'zephys' ),
            'type-7' => esc_html__( 'Type 7', 'zephys' ),
        );
    }
    add_filter('lastudio-elements/portfolio/control/preset', 'zephys_add_portfolio_preset');
}

if(!function_exists('zephys_add_portfolio_list_preset')){
    function zephys_add_portfolio_list_preset( ){
        return array(
            'list-type-1' => esc_html__( 'Type 1', 'zephys' ),
            'list-type-2' => esc_html__( 'Type 2', 'zephys' ),
            'list-type-3' => esc_html__( 'Type 3', 'zephys' ),
            'list-type-4' => esc_html__( 'Type 4', 'zephys' )
        );
    }
    add_filter('lastudio-elements/portfolio/control/preset_list', 'zephys_add_portfolio_list_preset');
}

if(!function_exists('zephys_add_team_member_preset')){
    function zephys_add_team_member_preset( ){
        return array(
            'type-1' => esc_html__( 'Type 1', 'zephys' ),
            'type-2' => esc_html__( 'Type 2', 'zephys' ),
            'type-3' => esc_html__( 'Type 3', 'zephys' ),
            'type-4' => esc_html__( 'Type 4', 'zephys' ),
            'type-5' => esc_html__( 'Type 5', 'zephys' ),
            'type-6' => esc_html__( 'Type 6', 'zephys' ),
            'type-7' => esc_html__( 'Type 7', 'zephys' ),
            'type-8' => esc_html__( 'Type 8', 'zephys' )
        );
    }
    add_filter('lastudio-elements/team-member/control/preset', 'zephys_add_team_member_preset');
}

if(!function_exists('zephys_add_posts_preset')){
    function zephys_add_posts_preset( ){
        return array(
            'grid-1' => esc_html__( 'Grid 1', 'zephys' ),
            'grid-2' => esc_html__( 'Grid 2', 'zephys' ),
            'grid-3' => esc_html__( 'Grid 3', 'zephys' ),
            'grid-4' => esc_html__( 'Grid 4', 'zephys' ),
            'grid-5' => esc_html__( 'Grid 5', 'zephys' ),
            'grid-6' => esc_html__( 'Grid 6', 'zephys' ),
            'grid-7' => esc_html__( 'Grid 7', 'zephys' ),
            'list-1' => esc_html__( 'List 1', 'zephys' ),
            'list-2' => esc_html__( 'List 2', 'zephys' ),
            'list-3' => esc_html__( 'List 3', 'zephys' ),
            'list-4' => esc_html__( 'List 4', 'zephys' ),
            'list-5' => esc_html__( 'List 5', 'zephys' )
        );
    }
    add_filter('lastudio-elements/posts/control/preset', 'zephys_add_posts_preset');
}

if(!function_exists('zephys_add_google_maps_api')){
    function zephys_add_google_maps_api( $key ){
        return zephys_get_option('google_key', $key);
    }
    add_filter('lastudio-elements/advanced-map/api', 'zephys_add_google_maps_api');
}

if(!function_exists('zephys_add_instagram_access_token_api')){
    function zephys_add_instagram_access_token_api( $key ){
        return zephys_get_option('instagram_token', $key);
    }
    add_filter('lastudio-elements/instagram-gallery/api', 'zephys_add_instagram_access_token_api');
}

if(!function_exists('zephys_add_mailchimp_access_token_api')){
    function zephys_add_mailchimp_access_token_api( $key ){
        return zephys_get_option('mailchimp_api_key', $key);
    }
    add_filter('lastudio-elements/mailchimp/api', 'zephys_add_mailchimp_access_token_api');
}

if(!function_exists('zephys_add_mailchimp_list_id')){
    function zephys_add_mailchimp_list_id( $key ){
        return zephys_get_option('mailchimp_list_id', $key);
    }
    add_filter('lastudio-elements/mailchimp/list_id', 'zephys_add_mailchimp_list_id');
}

if(!function_exists('zephys_add_mailchimp_double_opt_in')){
    function zephys_add_mailchimp_double_opt_in( $key ){
        return zephys_get_option('mailchimp_double_opt_in', $key);
    }
    add_filter('lastudio-elements/mailchimp/double_opt_in', 'zephys_add_mailchimp_double_opt_in');
}

if(!function_exists('zephys_render_breadcrumbs_in_widget')){
    function zephys_render_breadcrumbs_in_widget( $args ) {

        $html_tag = 'nav';
        if(!empty($args['container'])){
            $html_tag = esc_attr($args['container']);
        }

        if ( function_exists( 'yoast_breadcrumb' ) ) {
            $classes = 'site-breadcrumbs';
            return yoast_breadcrumb( '<'.$html_tag.' class="'. esc_attr($classes) .'">', '</'.$html_tag.'>' );
        }

        $breadcrumb = apply_filters( 'breadcrumb_trail_object', null, $args );

        if ( !is_object( $breadcrumb ) ){
            $breadcrumb = new Zephys_Breadcrumb_Trail( $args );
        }

        return $breadcrumb->trail();

    }
    add_action('lastudio-elements/render_breadcrumbs_output', 'zephys_render_breadcrumbs_in_widget');
}

if(!function_exists('zephys_turnoff_default_style_of_gallery')){
    function zephys_turnoff_default_style_of_gallery( $base ){
        if( 'image-gallery' === $base->get_name() ) {
            add_filter('use_default_gallery_style', '__return_false');
        }
    }
    add_action('elementor/widget/before_render_content', 'zephys_turnoff_default_style_of_gallery');
}