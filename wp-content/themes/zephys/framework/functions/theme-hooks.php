<?php
/**
 * This file includes helper functions used throughout the theme.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_filter( 'body_class', 'zephys_body_classes' );

/**
 * Head
 */
add_action('wp_head', 'zephys_add_meta_into_head_tag', 100 );
add_action('zephys/action/head', 'zephys_add_extra_data_into_head');

add_action('zephys/action/before_outer_wrap', 'zephys_add_pageloader_icon', 1);

/**
 * Header
 */
add_action( 'zephys/action/header', 'zephys_render_header', 10 );


/**
 * Page Header
 */
add_action( 'zephys/action/page_header', 'zephys_render_page_header', 10 );


/**
 * Sidebar
 */


$site_layout = zephys_get_site_layout();

if($site_layout == 'col-2cr' || $site_layout == 'col-2cr-l'){
    add_action( 'zephys/action/after_primary', 'zephys_render_sidebar', 10 );
}
else{
    add_action( 'zephys/action/before_primary', 'zephys_render_sidebar', 10 );
}


/**
 * Footer
 */
add_action( 'zephys/action/footer', 'zephys_render_footer', 10 );

add_action( 'zephys/action/after_outer_wrap', 'zephys_render_footer_searchform_overlay', 10 );
add_action( 'zephys/action/after_outer_wrap', 'zephys_render_footer_cartwidget_overlay', 15 );
add_action( 'zephys/action/after_outer_wrap', 'zephys_render_footer_newsletter_popup', 20 );


/**
 * FILTERS
 */

add_filter('zephys/filter/get_theme_option_by_context', 'zephys_override_page_title_bar_from_context', 10, 2);
add_filter('previous_post_link', 'zephys_override_post_navigation_template', 10, 5);
add_filter('next_post_link', 'zephys_override_post_navigation_template', 10, 5);


/**
 * Single Posts
 */

add_action( 'zephys/action/after_content_wrap', 'zephys_add_related_posts_to_blog_posts', 10 );