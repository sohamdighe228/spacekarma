<?php
/**
 * WooCommerce helper functions
 * This functions only load if WooCommerce is enabled because
 * they should be used within Woo loops only.
 *
 * @package Zephys WordPress theme
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_filter('woocommerce_product_description_heading', '__return_empty_string');
add_filter('woocommerce_product_additional_information_heading', '__return_empty_string');

if(!function_exists('zephys_woo_get_product_per_page_array')){
    function zephys_woo_get_product_per_page_array(){
        $per_page_array = apply_filters('zephys/filter/get_product_per_page_array', zephys_get_option('product_per_page_allow', ''));
        if(!empty($per_page_array)){
            $per_page_array = explode(',', $per_page_array);
            $per_page_array = array_map('trim', $per_page_array);
            $per_page_array = array_map('absint', $per_page_array);
            asort($per_page_array);
            return $per_page_array;
        }
        else{
            return array();
        }
    }
}

if(!function_exists('zephys_woo_get_product_per_page')){
    function zephys_woo_get_product_per_page(){
        return apply_filters('zephys/filter/get_product_per_page', zephys_get_option('product_per_page_default', 9));
    }
}

if(!function_exists('zephys_get_base_shop_url')){
    function zephys_get_base_shop_url( $with_post_type_archive = true ){

        if(function_exists('la_get_base_shop_url')){
            return la_get_base_shop_url($with_post_type_archive);
        }

        $link = '';
        if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
            $link = home_url();
        }
        elseif( function_exists('is_product_taxonomy') && is_product_taxonomy() ) {
            if( is_product_tag() && $with_post_type_archive ){
                $link = get_post_type_archive_link( 'product' );
            }
            else{
                if( is_product_category() ) {
                    $link = get_term_link( get_query_var( 'product_cat' ), 'product_cat' );
                }
                elseif ( is_product_tag() ) {
                    $link = get_term_link( get_query_var( 'product_tag' ), 'product_tag' );
                }
                else{
                    $queried_object = get_queried_object();
                    $link = get_term_link( $queried_object->slug, $queried_object->taxonomy );
                }
            }
        }
        else{
            if($with_post_type_archive){
                $link = get_post_type_archive_link( 'product' );
            }
            else{
                if(function_exists('dokan')){
                    $current_url = add_query_arg(null, null);
                    $current_url = remove_query_arg(array('page', 'paged', 'mode_view', 'la_doing_ajax'), $current_url);
                    $link = preg_replace('/\/page\/\d+/', '', $current_url);
                    $tmp = explode('?', $link);
                    if(isset($tmp[0])){
                        $link = $tmp[0];
                    }
                }
            }
        }
        return $link;
    }
}

if(!function_exists('zephys_get_wc_attribute_for_compare')){
    function zephys_get_wc_attribute_for_compare(){
        return array(
            'image'         => esc_html__( 'Image', 'zephys' ),
            'title'         => esc_html__( 'Title', 'zephys' ),
            'add-to-cart'   => esc_html__( 'Add to cart', 'zephys' ),
            'price'         => esc_html__( 'Price', 'zephys' ),
            'sku'           => esc_html__( 'Sku', 'zephys' ),
            'description'   => esc_html__( 'Description', 'zephys' ),
            'stock'         => esc_html__( 'Availability', 'zephys' ),
            'weight'        => esc_html__( 'Weight', 'zephys' ),
            'dimensions'    => esc_html__( 'Dimensions', 'zephys' )
        );
    }
}

if(!function_exists('zephys_get_wc_attribute_taxonomies')){
    function zephys_get_wc_attribute_taxonomies( ){
        $attributes = array();
        if( function_exists( 'wc_get_attribute_taxonomies' ) && function_exists( 'wc_attribute_taxonomy_name' ) ) {
            $attribute_taxonomies = wc_get_attribute_taxonomies();
            if(!empty($attribute_taxonomies)){
                foreach( $attribute_taxonomies as $attribute ) {
                    $tax = wc_attribute_taxonomy_name( $attribute->attribute_name );
                    $attributes[$tax] = ucfirst( $attribute->attribute_name );
                }
            }
        }

        return $attributes;
    }
}

if(!function_exists('zephys_get_wishlist_url')){
    function zephys_get_wishlist_url(){
        $wishlist_page_id = zephys_get_option('wishlist_page', 0);
        return (!empty($wishlist_page_id) ? get_the_permalink($wishlist_page_id) : esc_url(home_url('/')));
    }
}

if(!function_exists('zephys_get_compare_url')){
    function zephys_get_compare_url(){
        $compare_page_id = zephys_get_option('compare_page', 0);
        return (!empty($compare_page_id) ? get_the_permalink($compare_page_id) : esc_url(home_url('/')));
    }
}

/**
 * This function allow get property of `woocommerce_loop` inside the loop
 * @since 1.0.0
 * @param string $prop Prop to get.
 * @param string $default Default if the prop does not exist.
 * @return mixed
 */

if(!function_exists('zephys_get_wc_loop_prop')){
    function zephys_get_wc_loop_prop( $prop, $default = ''){
        return isset( $GLOBALS['woocommerce_loop'], $GLOBALS['woocommerce_loop'][ $prop ] ) ? $GLOBALS['woocommerce_loop'][ $prop ] : $default;
    }
}

/**
 * This function allow set property of `woocommerce_loop`
 * @since 1.0.0
 * @param string $prop Prop to set.
 * @param string $value Value to set.
 */

if(!function_exists('zephys_set_wc_loop_prop')){
    function zephys_set_wc_loop_prop( $prop, $value = ''){
        if(isset($GLOBALS['woocommerce_loop'])){
            $GLOBALS['woocommerce_loop'][ $prop ] = $value;
        }
    }
}
/**
 * Override template product title
 */
if ( ! function_exists( 'woocommerce_template_loop_product_title' ) ) {
    function woocommerce_template_loop_product_title() {
        the_title( sprintf( '<h2 class="product_item--title"><a href="%s">', esc_url( get_the_permalink() ) ), '</a></h3>' );
    }
}


if(!function_exists('zephys_wc_filter_show_page_title')){
    function zephys_wc_filter_show_page_title( $show ){
        if( is_singular('product') && zephys_string_to_bool( zephys_get_option('product_single_hide_page_title', 'no') ) ){
            return false;
        }
        return $show;
    }
    add_filter('zephys/filter/show_page_title', 'zephys_wc_filter_show_page_title', 10, 1 );
}

if(!function_exists('zephys_wc_filter_show_breadcrumbs')){
    function zephys_wc_filter_show_breadcrumbs( $show ){
        if( is_singular('product') && zephys_string_to_bool( zephys_get_option('product_single_hide_breadcrumb', 'no') ) ){
            return false;
        }
        return $show;
    }
    add_filter('zephys/filter/show_breadcrumbs', 'zephys_wc_filter_show_breadcrumbs', 10, 1 );
}


if(!function_exists('zephys_wc_allow_translate_text_in_swatches')){

    function zephys_wc_allow_translate_text_in_swatches( $text ){
        return esc_html_x( 'Choose an option', 'front-view', 'zephys' );
    }

    add_filter('LaStudio/swatches/args/show_option_none', 'zephys_wc_allow_translate_text_in_swatches', 10, 1);
}

/**
 * Override page title bar from global settings
 * What we need to do now is
 * 1. checking in single content types
 *  1.1) post
 *  1.2) product
 *  1.3) portfolio
 * 2. checking in archives
 *  2.1) shop
 *  2.2) portfolio
 *
 * TIPS: List functions will be use to check
 * `is_product`, `is_single_la_portfolio`, `is_shop`, `is_woocommerce`, `is_product_taxonomy`, `is_archive_la_portfolio`, `is_tax_la_portfolio`
 */

if(!function_exists('zephys_wc_override_page_title_bar_from_context')){
    function zephys_wc_override_page_title_bar_from_context( $value, $key ){

        $array_key_allow = array(
            'page_title_bar_style',
            'page_title_bar_layout',
            'page_title_bar_background',
            'page_title_bar_space',
            'page_title_bar_heading_font_family',
            'page_title_bar_heading_font_size',
            'page_title_bar_breadcrumb_font_family',
            'page_title_bar_breadcrumb_font_size',
            'page_title_bar_text_color',
            'page_title_bar_link_color',
            'page_title_bar_link_hover_color'
        );

        $array_key_alternative = array(
            'page_title_font_size',
            'page_title_bar_layout',
            'page_title_bar_background',
            'page_title_bar_space',
            'page_title_bar_heading_font_family',
            'page_title_bar_heading_font_size',
            'page_title_bar_breadcrumb_font_family',
            'page_title_bar_breadcrumb_font_size',
            'page_title_bar_text_color',
            'page_title_bar_link_color',
            'page_title_bar_link_hover_color'
        );

        /**
         * Firstly, we need to check the `$key` input
         */
        if( !in_array($key, $array_key_allow) ){
            return $value;
        }

        /**
         * Secondary, we need to check the `$context` input
         */

        if( !is_woocommerce() ){
            return $value;
        }


        $func_name = 'zephys_get_post_meta';
        $queried_object_id = get_queried_object_id();

        if( is_product_taxonomy() ){
            $func_name = 'zephys_get_term_meta';
        }

        if( is_shop() ){
            $queried_object_id = wc_get_page_id( 'shop' );
        }

        if ( 'page_title_bar_layout' == $key ) {
            $page_title_bar_layout = call_user_func($func_name, $queried_object_id, $key);
            if($page_title_bar_layout && $page_title_bar_layout != 'inherit'){
                return $page_title_bar_layout;
            }
        }

        if( 'yes' == call_user_func($func_name ,$queried_object_id, 'page_title_bar_style') && in_array($key, $array_key_alternative) ){
            return $value;
        }

        $key_override = $new_key = false;

        if( is_product() ){
            $key_override = 'single_product_override_page_title_bar';
            $new_key = 'single_product_' . $key;
        }
        elseif ( is_shop() || is_product_taxonomy() ) {
            $key_override = 'woo_override_page_title_bar';
            $new_key = 'woo_' . $key;
        }

        if(false != $key_override){
            if( 'on' == zephys_get_option($key_override, 'off') ){
                return zephys_get_option($new_key, $value);
            }
        }

        return $value;
    }

    add_filter('zephys/filter/get_theme_option_by_context', 'zephys_wc_override_page_title_bar_from_context', 20, 2);
}

if(!function_exists('zephys_override_woothumbnail_size_name')){
    function zephys_override_woothumbnail_size_name( ) {
        return 'shop_thumbnail';
    }
    add_filter('woocommerce_gallery_thumbnail_size', 'zephys_override_woothumbnail_size_name', 0);
}


if(!function_exists('zephys_override_woothumbnail_size')){
    function zephys_override_woothumbnail_size( $size ) {
        if(!function_exists('wc_get_theme_support')){
            return $size;
        }
        $size['width'] = absint( wc_get_theme_support( 'gallery_thumbnail_image_width', 180 ) );
        $cropping      = get_option( 'woocommerce_thumbnail_cropping', '1:1' );

        if ( 'uncropped' === $cropping ) {
            $size['height'] = '';
            $size['crop']   = 0;
        }
        elseif ( 'custom' === $cropping ) {
            $width          = max( 1, get_option( 'woocommerce_thumbnail_cropping_custom_width', '4' ) );
            $height         = max( 1, get_option( 'woocommerce_thumbnail_cropping_custom_height', '3' ) );
            $size['height'] = absint( round( ( $size['width'] / $width ) * $height ) );
            $size['crop']   = 1;
        }
        else {
            $cropping_split = explode( ':', $cropping );
            $width          = max( 1, current( $cropping_split ) );
            $height         = max( 1, end( $cropping_split ) );
            $size['height'] = absint( round( ( $size['width'] / $width ) * $height ) );
            $size['crop']   = 1;
        }

        return $size;
    }
    add_filter('woocommerce_get_image_size_gallery_thumbnail', 'zephys_override_woothumbnail_size');
}

if(!function_exists('zephys_override_woothumbnail_single')){
    function zephys_override_woothumbnail_single( $size ) {
        if(!function_exists('wc_get_theme_support')){
            return $size;
        }
        $size['width'] = absint( wc_get_theme_support( 'single_image_width', get_option( 'woocommerce_single_image_width', 600 ) ) );
        $cropping      = get_option( 'woocommerce_thumbnail_cropping', '1:1' );

        if ( 'uncropped' === $cropping ) {
            $size['height'] = '';
            $size['crop']   = 0;
        }
        elseif ( 'custom' === $cropping ) {
            $width          = max( 1, get_option( 'woocommerce_thumbnail_cropping_custom_width', '4' ) );
            $height         = max( 1, get_option( 'woocommerce_thumbnail_cropping_custom_height', '3' ) );
            $size['height'] = absint( round( ( $size['width'] / $width ) * $height ) );
            $size['crop']   = 1;
        }
        else {
            $cropping_split = explode( ':', $cropping );
            $width          = max( 1, current( $cropping_split ) );
            $height         = max( 1, end( $cropping_split ) );
            $size['height'] = absint( round( ( $size['width'] / $width ) * $height ) );
            $size['crop']   = 1;
        }

        return $size;
    }
    add_filter('woocommerce_get_image_size_single', 'zephys_override_woothumbnail_single', 0);
}
