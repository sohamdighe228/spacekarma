<?php
/**
 * This file includes dynamic css
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$css_primary_color = zephys_get_option('primary_color', '#4241FF');
$css_secondary_color = zephys_get_option('secondary_color', '#212121');

$all_styles = array(
    'mobile' => array(),
    'mobile_landscape' => array(),
    'tablet' => array(),
    'laptop' => array(),
    'desktop' => array()
);
/**
 * Body Background
 */

$body_background = zephys_get_option('body_background');
if(!empty(zephys_array_filter_recursive($body_background))){
    echo zephys_render_background_style_from_setting($body_background, 'body.zephys-body');
}

/**
 * Main_Space
 */
$main_space = zephys_get_theme_option_by_context('main_space');
if(!empty($main_space)){
    foreach ($main_space as $screen => $value ){
        $_css = '';
        $unit = !empty($value['unit'])? $value['unit']: 'px';
        $value_atts = shortcode_atts(array(
            'top' => '',
            'right' => '',
            'bottom' => '',
            'left' => '',
        ), $value);
        foreach ($value_atts as $k => $v){
            if($v !== ''){
                $_css .= 'padding-' . $k . ':' . $v . $unit . ';';
            }
        }
        if(!empty($_css)) {
            $all_styles[$screen][] = '#main #content-wrap{'. $_css .'}';
        }
    }
}

/**
 * Page Title Bar
 */
$page_title_bar_func = 'zephys_get_option';
if( zephys_string_to_bool(zephys_get_theme_option_by_context('page_title_bar_style', 'no')) ){
    $page_title_bar_func = 'zephys_get_theme_option_by_context';
}
if ( function_exists('is_product') && is_product() ){
    if( zephys_string_to_bool( zephys_get_option('single_product_override_page_title_bar', 'off') ) ) {
        $page_title_bar_func = 'zephys_get_theme_option_by_context';
    }
}
elseif ( is_singular('la_portfolio') ){
    if( zephys_string_to_bool( zephys_get_option('single_portfolio_override_page_title_bar', 'off') ) ) {
        $page_title_bar_func = 'zephys_get_theme_option_by_context';
    }
}
elseif ( is_singular('post') ){
    if( zephys_string_to_bool( zephys_get_option('single_post_override_page_title_bar', 'off') ) ) {
        $page_title_bar_func = 'zephys_get_theme_option_by_context';
    }
}
elseif ( function_exists('is_woocommerce') && is_woocommerce() ) {
    if( zephys_string_to_bool( zephys_get_option('woo_override_page_title_bar', 'off') ) ) {
        $page_title_bar_func = 'zephys_get_theme_option_by_context';
    }
}
elseif ( is_post_type_archive('la_portfolio') || ( is_tax() && is_tax( get_object_taxonomies( 'la_portfolio' ) ) ) ) {
    if( zephys_string_to_bool( zephys_get_option('archive_portfolio_override_page_title_bar', 'off') ) ) {
        $page_title_bar_func = 'zephys_get_theme_option_by_context';
    }
}

$page_title_bar_space = call_user_func($page_title_bar_func, 'page_title_bar_space');

if(!empty($page_title_bar_space)){
    foreach ($page_title_bar_space as $screen => $value ){
        $_css = '';
        $unit = !empty($value['unit'])? $value['unit']: 'px';
        $value_atts = shortcode_atts(array(
            'top' => '',
            'right' => '',
            'bottom' => '',
            'left' => '',
        ), $value);
        foreach ($value_atts as $k => $v){
            if($v !== ''){
                $_css .= 'padding-' . $k . ':' . $v . $unit . ';';
            }
        }
        if(!empty($_css)) {
            $all_styles[$screen][] = '.section-page-header .page-header-inner{'. $_css .'}';
        }
    }
}

$page_title_bar_background = call_user_func($page_title_bar_func, 'page_title_bar_background');
$page_title_bar_text_color = call_user_func($page_title_bar_func, 'page_title_bar_text_color', '#212121');
$page_title_bar_link_color = call_user_func($page_title_bar_func, 'page_title_bar_link_color', '#212121');
$page_title_bar_link_hover_color = call_user_func($page_title_bar_func, 'page_title_bar_link_hover_color', $css_primary_color);


$page_title_bar_heading_font_family = call_user_func($page_title_bar_func, 'page_title_bar_heading_font_family');
echo zephys_render_typography_style_from_setting( $page_title_bar_heading_font_family, '.section-page-header .page-title' );

$page_title_bar_breadcrumb_font_family = call_user_func($page_title_bar_func, 'page_title_bar_breadcrumb_font_family');
echo zephys_render_typography_style_from_setting( $page_title_bar_breadcrumb_font_family, '.section-page-header .site-breadcrumbs' );

if(!empty(zephys_array_filter_recursive($page_title_bar_background))){
    echo zephys_render_background_style_from_setting($page_title_bar_background, '.section-page-header');
}

$page_title_bar_heading_font_size = call_user_func($page_title_bar_func, 'page_title_bar_heading_font_size');
$page_title_bar_breadcrumb_font_size = call_user_func($page_title_bar_func, 'page_title_bar_breadcrumb_font_size');

if(!empty($page_title_bar_heading_font_size)){
    foreach ($page_title_bar_heading_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.section-page-header .page-title' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

if(!empty($page_title_bar_breadcrumb_font_size)){
    foreach ($page_title_bar_breadcrumb_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.section-page-header .site-breadcrumbs' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}
if(!empty($page_title_bar_text_color)){
    echo '.section-page-header { color: '.esc_attr($page_title_bar_text_color).' }';
}
if(!empty($page_title_bar_link_color)){
    echo '.section-page-header a { color: '.esc_attr($page_title_bar_link_color).' }';
}
if(!empty($page_title_bar_link_hover_color)){
    echo '.section-page-header a:hover { color: '.esc_attr($page_title_bar_link_hover_color).' }';
}

/**
 * Popup Style
 */
$popup_background = zephys_get_option('popup_background');
if(!empty(zephys_array_filter_recursive($popup_background))){
    echo zephys_render_background_style_from_setting($popup_background, '.open-newsletter-popup .lightcase-inlineWrap');
}


/**
 * Shop Item Space
 */
$shop_item_space = zephys_get_option('shop_item_space');
if(!empty($shop_item_space)){
    foreach ($shop_item_space as $screen => $value ){
        $_css = '';
        $_css2 = '';
        $unit = !empty($value['unit'])? $value['unit']: 'px';

        $value_atts = shortcode_atts(array(
            'top' => '',
            'right' => '',
            'bottom' => '',
            'left' => ''
        ), $value);


        foreach ($value_atts as $k => $v){
            if($v !== ''){
                $_css .= 'padding-' . $k . ':' . $v . $unit . ';';
                if($k == 'left' || $k == 'right'){
                    $_css2 .= 'margin-' . $k . ':-' . $v . $unit . ';';
                }
            }
        }

        if(!empty($_css)) {
            $all_styles[$screen][] = '.ul_products.products{'. $_css2 .'}';
            $all_styles[$screen][] = '.ul_products.products li.product_item{'. $_css .'}';
        }
    }
}
/**
 * Blog Item Image Height
 */
$blog_item_space = zephys_get_option('blog_item_space');
if(!empty($blog_item_space)){
    foreach ($blog_item_space as $screen => $value ){
        $_css = '';
        $_css2 = '';
        $unit = !empty($value['unit'])? $value['unit']: 'px';

        $value_atts = shortcode_atts(array(
            'top' => '',
            'right' => '',
            'bottom' => '',
            'left' => '',
        ), $value);
        foreach ($value_atts as $k => $v){
            if($v !== ''){
                $_css .= 'padding-' . $k . ':' . $v . $unit . ';';
                if($k == 'left' || $k == 'right'){
                    $_css2 .= 'margin-' . $k . ':-' . $v . $unit . ';';
                }
            }
        }
        if(!empty($_css)) {
            $all_styles[$screen][] = '.lastudio-posts.blog__entries{'. $_css2 .'}';
            $all_styles[$screen][] = '.lastudio-posts.blog__entries .loop__item{'. $_css .'}';
        }
    }
}

$blog_thumbnail_height_mode = zephys_get_option('blog_thumbnail_height_mode', 'original');
$blog_thumbnail_height_custom = zephys_get_option('blog_thumbnail_height_custom', '70%');
$blog_thumbnail_height = '70%';

switch ($blog_thumbnail_height_mode){
    case '1-1':
        $blog_thumbnail_height = '100%';
        break;
    case '4-3':
        $blog_thumbnail_height = '75%';
        break;
    case '3-4':
        $blog_thumbnail_height = '133.34%';
        break;
    case '16-9':
        $blog_thumbnail_height = '56.25%';
        break;
    case '9-16':
        $blog_thumbnail_height = '177.78%';
        break;
    case 'custom':
        $blog_thumbnail_height = $blog_thumbnail_height_custom;
        break;
}
if($blog_thumbnail_height_mode != 'original'){
    $all_styles['mobile'][] = '.lastudio-posts.blog__entries .post-thumbnail .blog_item--thumbnail, .lastudio-posts.blog__entries .post-thumbnail .blog_item--thumbnail .slick-slide .sinmer{ padding-bottom: '.$blog_thumbnail_height.'}';
}

/**
 * Build Typography
 */
/**
 * Build Typography - Body
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('body_font_family'), 'body' );
$body_font_size = zephys_get_option('body_font_size');
if(!empty($body_font_size)){
    foreach ($body_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'body' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

/**
 * Build Typography - All Headings
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('headings_font_family'), 'h1,h2,h3,h4,h5,h6' );
$headings_font_size = zephys_get_option('headings_font_size');
if(!empty($headings_font_size)){
    foreach ($headings_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'h1,h2,h3,h4,h5,h6,.theme-heading, .widget-title, .comments-title, .comment-reply-title, .entry-title' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}
/**
 * Build Typography - Heading H1
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('heading1_font_family'), 'h1' );
$heading1_font_size = zephys_get_option('heading1_font_size');
if(!empty($heading1_font_size)){
    foreach ($heading1_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'h1' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}
/**
 * Build Typography - Heading H2
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('heading2_font_family'), 'h2' );
$heading2_font_size = zephys_get_option('heading2_font_size');
if(!empty($heading2_font_size)){
    foreach ($heading2_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'h2' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}
/**
 * Build Typography - Heading H3
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('heading3_font_family'), 'h3' );
$heading3_font_size = zephys_get_option('heading3_font_size');
if(!empty($heading3_font_size)){
    foreach ($heading3_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'h3' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}
/**
 * Build Typography - Heading H4
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('heading4_font_family'), 'h4' );
$heading4_font_size = zephys_get_option('heading4_font_size');
if(!empty($heading4_font_size)){
    foreach ($heading4_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'h4' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}
/**
 * Build Typography - Heading H5
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('heading5_font_family'), 'h5' );
$heading5_font_size = zephys_get_option('heading5_font_size');
if(!empty($heading5_font_size)){
    foreach ($heading5_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'h5' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

/**
 * Build Typography - Blog Entry Title
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('blog_entry_title_font_family'), '.lastudio-posts.blog__entries .entry-title' );
$blog_entry_title_font_size = zephys_get_option('blog_entry_title_font_size');
if(!empty($blog_entry_title_font_size)){
    foreach ($blog_entry_title_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.lastudio-posts.blog__entries .entry-title' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

/**
 * Build Typography - Blog Entry Meta
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('blog_entry_meta_font_family'), '.lastudio-posts.blog__entries .post-meta' );
$blog_entry_meta_font_size = zephys_get_option('blog_entry_meta_font_size');
if(!empty($blog_entry_meta_font_size)){
    foreach ($blog_entry_meta_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.lastudio-posts.blog__entries .post-meta' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

/**
 * Build Typography - Blog Entry Content
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('blog_entry_content_font_family'), '.lastudio-posts.blog__entries .entry-excerpt' );
$blog_entry_content_font_size = zephys_get_option('blog_entry_content_font_size');
if(!empty($blog_entry_content_font_size)){
    foreach ($blog_entry_content_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.lastudio-posts.blog__entries .entry-excerpt' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}


/**
 * Build Typography - Blog Post Title
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('blog_post_title_font_family'), '.single-post-article > .entry-header .entry-title' );
$blog_post_title_font_size = zephys_get_option('blog_post_title_font_size');
if(!empty($blog_post_title_font_size)){
    foreach ($blog_post_title_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.single-post-article > .entry-header .entry-title' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}


/**
 * Build Typography - Blog Post Meta
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('blog_post_meta_font_family'), '.single-post-article > .post-meta__item, .single-post-article > .post-meta .post-meta__item' );
$blog_post_meta_font_size = zephys_get_option('blog_post_meta_font_size');
if(!empty($blog_post_meta_font_size)){
    foreach ($blog_post_meta_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, '.single-post-article > .post-meta__item, .single-post-article > .post-meta .post-meta__item' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

/**
 * Build Typography - Blog Post Content
 */
echo zephys_render_typography_style_from_setting( zephys_get_option('blog_post_content_font_family'), 'body:not(.page-use-builder) .single-post-article > .entry' );
$blog_post_content_font_size = zephys_get_option('blog_post_content_font_size');
if(!empty($blog_post_content_font_size)){
    foreach ($blog_post_content_font_size as $screen => $value ){
        $_css = zephys_render_typography_style_from_setting( $value, 'body:not(.page-use-builder) .single-post-article > .entry' );
        if(!empty($_css)) {
            $all_styles[$screen][] = $_css;
        }
    }
}

/**
 * Print the styles
 */
/**
 * MOBILE FIRST
 */
if(!empty($all_styles['mobile'])){
    echo esc_html(join('', $all_styles['mobile']));
}

/**
 * MOBILE LANDSCAPE AND TABLET PORTRAIT
 */
if(!empty($all_styles['mobile_landscape'])){
    echo '@media (min-width: 600px) {';
    echo esc_html(join('', $all_styles['mobile_landscape']));
    echo '}';
}

/**
 * TABLET LANDSCAPE
 */
if(!empty($all_styles['tablet'])){
    echo '@media (min-width: 800px) {';
    echo esc_html(join('', $all_styles['tablet']));
    echo '}';
}

/**
 * LAPTOP LANDSCAPE
 */
if(!empty($all_styles['laptop'])){
    echo '@media (min-width: 1300px) {';
    echo esc_html(join('', $all_styles['laptop']));
    echo '}';
}

/**
 * DESKTOP LANDSCAPE
 */
if(!empty($all_styles['desktop'])){
    echo '@media (min-width: 1600px) {';
    echo esc_html(join('', $all_styles['desktop']));
    echo '}';
}
?>
.la-isotope-loading span{
box-shadow: 2px 2px 1px <?php echo esc_attr($css_primary_color); ?>;
}
a:hover,
a.light:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
blockquote{
border-color: <?php echo esc_attr($css_primary_color); ?>;
}
input[type="button"]:hover,
input[type="reset"]:hover,
input[type="submit"]:hover,
button[type="submit"]:hover,
input[type="button"]:focus,
input[type="reset"]:focus,
input[type="submit"]:focus,
button[type="submit"]:focus,
.button:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}

.lahb-wrap .lahb-nav-wrap .menu li.current ul li a:hover,
.lahb-wrap .lahb-nav-wrap .menu ul.sub-menu li.current > a,
.lahb-wrap .lahb-nav-wrap .menu ul li.menu-item:hover > a {
color: <?php echo esc_attr($css_primary_color); ?>;
}

.lahb-nav-wrap .menu > li.current > a {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.lahb-modal-login #user-logged .author-avatar img {
border-color: <?php echo esc_attr($css_primary_color); ?>;
}
.single-post-article > .post-terms {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.single-post-article > .entry-footer .tags-list a:hover {
border-color: <?php echo esc_attr($css_primary_color); ?>;
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.single-post-article > .entry-footer .tags-list a:hover {
border-color: <?php echo esc_attr($css_primary_color); ?>;
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.section-related-posts.related-posts-design-2 .entry-title a:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.widget-title:after {
border-bottom-color: <?php echo esc_attr($css_primary_color); ?>;
}
.la_product_tag_cloud .active a,
.la_product_tag_cloud a:hover,
.widget_tag_cloud .active a,
.widget_tag_cloud a:hover {
border-color: <?php echo esc_attr($css_primary_color); ?>;
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.la_product_tag_cloud .active a,
.la_product_tag_cloud a:hover,
.widget_tag_cloud .active a,
.widget_tag_cloud a:hover {
border-color: <?php echo esc_attr($css_primary_color); ?>;
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.widget_layered_nav ul li.chosen a:after, .widget_layered_nav ul li.active a:after {
background: <?php echo esc_attr($css_primary_color); ?>;
}
.search-form .search-button:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts .lastudio-more-wrap .lastudio-more:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
border-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts .lastudio-more-wrap .lastudio-more:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
border-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts .lastudio-more-wrap .lastudio-more:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
border-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts.preset-grid-2 .lastudio-more-wrap .lastudio-more:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts.preset-grid-4 .post-terms {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts.preset-grid-5 .loop__item.has-post-thumbnail .lastudio-posts__inner-content:after {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts.preset-grid-6 .loop__item .lastudio-posts__inner-box:after {
background-image: linear-gradient(180deg, rgba(19, 19, 19, 0.66) 0%, <?php echo esc_attr($css_primary_color); ?> 100%);
}
.lastudio-posts.preset-grid-7 .post-terms {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts--list .post-terms {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-posts .post-thumbnail.single_post_quote_wrap .blog_item--thumbnail {
background: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-slick-dots li.slick-active span, .lastudio-slick-dots li:hover span {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-team-member__item .lastudio-images-layout__link:after {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.lastudio-team-member__socials .item--social a:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.preset-type-3.lastudio-team-member .lastudio-team-member__position {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.preset-type-7.lastudio-team-member .item--social a:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.preset-type-8.lastudio-team-member .item--social a:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.playout-grid.preset-type-4 .lastudio-portfolio__button:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.playout-grid.preset-type-6 .lastudio-portfolio__item:hover .lastudio-portfolio__button {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.playout-grid.preset-type-7 .lastudio-portfolio__item:hover .lastudio-portfolio__button {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.has-default-404 .default-404-content h1 {
color: <?php echo esc_attr($css_primary_color); ?>;
}
blockquote.wp-block-quote.quote-style-one {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.listing__content-price {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.job_listings .job_filters .lastudio-search-submit .search-submit {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.woocommerce-message .button:hover,
.woocommerce-error .button:hover,
.woocommerce-info .button:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.open-advanced-shop-filter .wc-toolbar-container .btn-advanced-shop-filter {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.la-compare-table .add_to_cart_wrap a:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.products-list .product_item .product_item--thumbnail .quickview:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.products-list .product_item .product_item--info .la-addcart:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.products-list .product_item .product_item--info .add_compare.added, .products-list .product_item .product_item--info .add_compare:hover,
.products-list .product_item .product_item--info .add_wishlist.added,
.products-list .product_item .product_item--info .add_wishlist:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.products-grid .button.added {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.products-grid .button:hover {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.entry-summary p.stock.in-stock {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.entry-summary .add_compare.added,
.entry-summary .add_wishlist.added {
color: <?php echo esc_attr($css_primary_color); ?>;
}
.woocommerce-MyAccount-navigation li:hover a,
.woocommerce-MyAccount-navigation li.is-active a {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
.registration-form .button {
background-color: <?php echo esc_attr($css_primary_color); ?>;
}
p.lost_password {
color: <?php echo esc_attr($css_primary_color); ?>;
}
@media (min-width: 992px) {
.woocommerce-cart .woocommerce td.actions .button:hover {
color: <?php echo esc_attr($css_primary_color); ?>;
}
}