<?php
/**
 * The Header for our theme.
 *
 * @package Zephys WordPress theme
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?><!DOCTYPE html>
<html <?php language_attributes(); ?><?php zephys_schema_markup( 'html' ); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action('zephys/action/before_outer_wrap'); ?>

<div id="outer-wrap" class="site">

    <?php do_action('zephys/action/before_wrap'); ?>

    <div id="wrap">
        <?php

            do_action('zephys/action/before_header');

            do_action('zephys/action/header');

            do_action('zephys/action/after_header');

        ?>

        <?php do_action('zephys/action/before_main'); ?>

        <main id="main" class="site-main"<?php zephys_schema_markup('main') ?>>
            <?php

                do_action('zephys/action/before_page_header');

                do_action('zephys/action/page_header');

                do_action('zephys/action/after_page_header');